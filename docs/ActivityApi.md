# ActivityApi

All URIs are relative to *https://gitee.com/api/v5*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteUserStarredOwnerRepo**](ActivityApi.md#deleteUserStarredOwnerRepo) | **DELETE** /user/starred/{owner}/{repo} | 取消 star 一个仓库
[**deleteUserSubscriptionsOwnerRepo**](ActivityApi.md#deleteUserSubscriptionsOwnerRepo) | **DELETE** /user/subscriptions/{owner}/{repo} | 取消 watch 一个仓库
[**getNetworksOwnerRepoEvents**](ActivityApi.md#getNetworksOwnerRepoEvents) | **GET** /networks/{owner}/{repo}/events | 列出仓库的所有公开动态
[**getNotificationsCount**](ActivityApi.md#getNotificationsCount) | **GET** /notifications/count | 获取授权用户的通知数
[**getNotificationsMessages**](ActivityApi.md#getNotificationsMessages) | **GET** /notifications/messages | 列出授权用户的所有私信
[**getNotificationsMessagesId**](ActivityApi.md#getNotificationsMessagesId) | **GET** /notifications/messages/{id} | 获取一条私信
[**getNotificationsThreads**](ActivityApi.md#getNotificationsThreads) | **GET** /notifications/threads | 列出授权用户的所有通知
[**getNotificationsThreadsId**](ActivityApi.md#getNotificationsThreadsId) | **GET** /notifications/threads/{id} | 获取一条通知
[**getOrgsOrgEvents**](ActivityApi.md#getOrgsOrgEvents) | **GET** /orgs/{org}/events | 列出组织的公开动态
[**getReposOwnerRepoEvents**](ActivityApi.md#getReposOwnerRepoEvents) | **GET** /repos/{owner}/{repo}/events | 列出仓库的所有动态
[**getReposOwnerRepoNotifications**](ActivityApi.md#getReposOwnerRepoNotifications) | **GET** /repos/{owner}/{repo}/notifications | 列出一个仓库里的通知
[**getReposOwnerRepoStargazers**](ActivityApi.md#getReposOwnerRepoStargazers) | **GET** /repos/{owner}/{repo}/stargazers | 列出 star 了仓库的用户
[**getReposOwnerRepoSubscribers**](ActivityApi.md#getReposOwnerRepoSubscribers) | **GET** /repos/{owner}/{repo}/subscribers | 列出 watch 了仓库的用户
[**getUserStarred**](ActivityApi.md#getUserStarred) | **GET** /user/starred | 列出授权用户 star 了的仓库
[**getUserStarredOwnerRepo**](ActivityApi.md#getUserStarredOwnerRepo) | **GET** /user/starred/{owner}/{repo} | 检查授权用户是否 star 了一个仓库
[**getUserSubscriptions**](ActivityApi.md#getUserSubscriptions) | **GET** /user/subscriptions | 列出授权用户 watch 了的仓库
[**getUserSubscriptionsOwnerRepo**](ActivityApi.md#getUserSubscriptionsOwnerRepo) | **GET** /user/subscriptions/{owner}/{repo} | 检查授权用户是否 watch 了一个仓库
[**getUsersUsernameEvents**](ActivityApi.md#getUsersUsernameEvents) | **GET** /users/{username}/events | 列出用户的动态
[**getUsersUsernameEventsOrgsOrg**](ActivityApi.md#getUsersUsernameEventsOrgsOrg) | **GET** /users/{username}/events/orgs/{org} | 列出用户所属组织的动态
[**getUsersUsernameEventsPublic**](ActivityApi.md#getUsersUsernameEventsPublic) | **GET** /users/{username}/events/public | 列出用户的公开动态
[**getUsersUsernameReceivedEvents**](ActivityApi.md#getUsersUsernameReceivedEvents) | **GET** /users/{username}/received_events | 列出一个用户收到的动态
[**getUsersUsernameReceivedEventsPublic**](ActivityApi.md#getUsersUsernameReceivedEventsPublic) | **GET** /users/{username}/received_events/public | 列出一个用户收到的公开动态
[**getUsersUsernameStarred**](ActivityApi.md#getUsersUsernameStarred) | **GET** /users/{username}/starred | 列出用户 star 了的仓库
[**getUsersUsernameSubscriptions**](ActivityApi.md#getUsersUsernameSubscriptions) | **GET** /users/{username}/subscriptions | 列出用户 watch 了的仓库
[**patchNotificationsMessagesId**](ActivityApi.md#patchNotificationsMessagesId) | **PATCH** /notifications/messages/{id} | 标记一条私信为已读
[**patchNotificationsThreadsId**](ActivityApi.md#patchNotificationsThreadsId) | **PATCH** /notifications/threads/{id} | 标记一条通知为已读
[**postNotificationsMessages**](ActivityApi.md#postNotificationsMessages) | **POST** /notifications/messages | 发送私信给指定用户
[**putNotificationsMessages**](ActivityApi.md#putNotificationsMessages) | **PUT** /notifications/messages | 标记所有私信为已读
[**putNotificationsThreads**](ActivityApi.md#putNotificationsThreads) | **PUT** /notifications/threads | 标记所有通知为已读
[**putReposOwnerRepoNotifications**](ActivityApi.md#putReposOwnerRepoNotifications) | **PUT** /repos/{owner}/{repo}/notifications | 标记一个仓库里的通知为已读
[**putUserStarredOwnerRepo**](ActivityApi.md#putUserStarredOwnerRepo) | **PUT** /user/starred/{owner}/{repo} | star 一个仓库
[**putUserSubscriptionsOwnerRepo**](ActivityApi.md#putUserSubscriptionsOwnerRepo) | **PUT** /user/subscriptions/{owner}/{repo} | watch 一个仓库

<a name="deleteUserStarredOwnerRepo"></a>
# **deleteUserStarredOwnerRepo**
> deleteUserStarredOwnerRepo(owner, repo)

取消 star 一个仓库

取消 star 一个仓库

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
try {
    apiInstance.deleteUserStarredOwnerRepo(owner, repo);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#deleteUserStarredOwnerRepo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteUserSubscriptionsOwnerRepo"></a>
# **deleteUserSubscriptionsOwnerRepo**
> deleteUserSubscriptionsOwnerRepo(owner, repo)

取消 watch 一个仓库

取消 watch 一个仓库

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
try {
    apiInstance.deleteUserSubscriptionsOwnerRepo(owner, repo);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#deleteUserSubscriptionsOwnerRepo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getNetworksOwnerRepoEvents"></a>
# **getNetworksOwnerRepoEvents**
> List&lt;Event&gt; getNetworksOwnerRepoEvents(owner, repo, prevId, limit)

列出仓库的所有公开动态

列出仓库的所有公开动态

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer prevId = 56; // Integer | 滚动列表的最后一条记录的id
Integer limit = 20; // Integer | 滚动列表每页的数量，最大为 100
try {
    List<Event> result = apiInstance.getNetworksOwnerRepoEvents(owner, repo, prevId, limit);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getNetworksOwnerRepoEvents");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **prevId** | **Integer**| 滚动列表的最后一条记录的id | [optional]
 **limit** | **Integer**| 滚动列表每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Event&gt;**](Event.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getNotificationsCount"></a>
# **getNotificationsCount**
> UserNotificationCount getNotificationsCount(unread)

获取授权用户的通知数

获取授权用户的通知数

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
Boolean unread = true; // Boolean | 是否只获取未读消息，默认：否
try {
    UserNotificationCount result = apiInstance.getNotificationsCount(unread);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getNotificationsCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **unread** | **Boolean**| 是否只获取未读消息，默认：否 | [optional]

### Return type

[**UserNotificationCount**](UserNotificationCount.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getNotificationsMessages"></a>
# **getNotificationsMessages**
> UserMessageList getNotificationsMessages(unread, since, before, ids, page, perPage)

列出授权用户的所有私信

列出授权用户的所有私信

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
Boolean unread = true; // Boolean | 是否只显示未读私信，默认：否
String since = "since_example"; // String | 只获取在给定时间后更新的私信，要求时间格式为 ISO 8601
String before = "before_example"; // String | 只获取在给定时间前更新的私信，要求时间格式为 ISO 8601
String ids = "ids_example"; // String | 指定一组私信 ID，以 , 分隔
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    UserMessageList result = apiInstance.getNotificationsMessages(unread, since, before, ids, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getNotificationsMessages");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **unread** | **Boolean**| 是否只显示未读私信，默认：否 | [optional]
 **since** | **String**| 只获取在给定时间后更新的私信，要求时间格式为 ISO 8601 | [optional]
 **before** | **String**| 只获取在给定时间前更新的私信，要求时间格式为 ISO 8601 | [optional]
 **ids** | **String**| 指定一组私信 ID，以 , 分隔 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**UserMessageList**](UserMessageList.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getNotificationsMessagesId"></a>
# **getNotificationsMessagesId**
> UserMessage getNotificationsMessagesId(id)

获取一条私信

获取一条私信

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
Integer id = 56; // Integer | 私信的 ID
try {
    UserMessage result = apiInstance.getNotificationsMessagesId(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getNotificationsMessagesId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| 私信的 ID |

### Return type

[**UserMessage**](UserMessage.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getNotificationsThreads"></a>
# **getNotificationsThreads**
> UserNotificationList getNotificationsThreads(unread, participating, type, since, before, ids, page, perPage)

列出授权用户的所有通知

列出授权用户的所有通知

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
Boolean unread = true; // Boolean | 是否只获取未读消息，默认：否
Boolean participating = true; // Boolean | 是否只获取自己直接参与的消息，默认：否
String type = "all"; // String | 筛选指定类型的通知，all：所有，event：事件通知，referer：@ 通知
String since = "since_example"; // String | 只获取在给定时间后更新的消息，要求时间格式为 ISO 8601
String before = "before_example"; // String | 只获取在给定时间前更新的消息，要求时间格式为 ISO 8601
String ids = "ids_example"; // String | 指定一组通知 ID，以 , 分隔
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    UserNotificationList result = apiInstance.getNotificationsThreads(unread, participating, type, since, before, ids, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getNotificationsThreads");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **unread** | **Boolean**| 是否只获取未读消息，默认：否 | [optional]
 **participating** | **Boolean**| 是否只获取自己直接参与的消息，默认：否 | [optional]
 **type** | **String**| 筛选指定类型的通知，all：所有，event：事件通知，referer：@ 通知 | [optional] [default to all] [enum: all, event, referer]
 **since** | **String**| 只获取在给定时间后更新的消息，要求时间格式为 ISO 8601 | [optional]
 **before** | **String**| 只获取在给定时间前更新的消息，要求时间格式为 ISO 8601 | [optional]
 **ids** | **String**| 指定一组通知 ID，以 , 分隔 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**UserNotificationList**](UserNotificationList.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getNotificationsThreadsId"></a>
# **getNotificationsThreadsId**
> UserNotification getNotificationsThreadsId(id)

获取一条通知

获取一条通知

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
Integer id = 56; // Integer | 通知的 ID
try {
    UserNotification result = apiInstance.getNotificationsThreadsId(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getNotificationsThreadsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| 通知的 ID |

### Return type

[**UserNotification**](UserNotification.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getOrgsOrgEvents"></a>
# **getOrgsOrgEvents**
> List&lt;Event&gt; getOrgsOrgEvents(org, prevId, limit)

列出组织的公开动态

列出组织的公开动态

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
String org = "org_example"; // String | 组织的路径(path/login)
Integer prevId = 56; // Integer | 滚动列表的最后一条记录的id
Integer limit = 20; // Integer | 滚动列表每页的数量，最大为 100
try {
    List<Event> result = apiInstance.getOrgsOrgEvents(org, prevId, limit);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getOrgsOrgEvents");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |
 **prevId** | **Integer**| 滚动列表的最后一条记录的id | [optional]
 **limit** | **Integer**| 滚动列表每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Event&gt;**](Event.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoEvents"></a>
# **getReposOwnerRepoEvents**
> List&lt;Event&gt; getReposOwnerRepoEvents(owner, repo, prevId, limit)

列出仓库的所有动态

列出仓库的所有动态

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer prevId = 56; // Integer | 滚动列表的最后一条记录的id
Integer limit = 20; // Integer | 滚动列表每页的数量，最大为 100
try {
    List<Event> result = apiInstance.getReposOwnerRepoEvents(owner, repo, prevId, limit);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getReposOwnerRepoEvents");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **prevId** | **Integer**| 滚动列表的最后一条记录的id | [optional]
 **limit** | **Integer**| 滚动列表每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Event&gt;**](Event.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoNotifications"></a>
# **getReposOwnerRepoNotifications**
> UserNotificationList getReposOwnerRepoNotifications(owner, repo, unread, participating, type, since, before, ids, page, perPage)

列出一个仓库里的通知

列出一个仓库里的通知

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Boolean unread = true; // Boolean | 是否只获取未读消息，默认：否
Boolean participating = true; // Boolean | 是否只获取自己直接参与的消息，默认：否
String type = "all"; // String | 筛选指定类型的通知，all：所有，event：事件通知，referer：@ 通知
String since = "since_example"; // String | 只获取在给定时间后更新的消息，要求时间格式为 ISO 8601
String before = "before_example"; // String | 只获取在给定时间前更新的消息，要求时间格式为 ISO 8601
String ids = "ids_example"; // String | 指定一组通知 ID，以 , 分隔
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    UserNotificationList result = apiInstance.getReposOwnerRepoNotifications(owner, repo, unread, participating, type, since, before, ids, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getReposOwnerRepoNotifications");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **unread** | **Boolean**| 是否只获取未读消息，默认：否 | [optional]
 **participating** | **Boolean**| 是否只获取自己直接参与的消息，默认：否 | [optional]
 **type** | **String**| 筛选指定类型的通知，all：所有，event：事件通知，referer：@ 通知 | [optional] [default to all] [enum: all, event, referer]
 **since** | **String**| 只获取在给定时间后更新的消息，要求时间格式为 ISO 8601 | [optional]
 **before** | **String**| 只获取在给定时间前更新的消息，要求时间格式为 ISO 8601 | [optional]
 **ids** | **String**| 指定一组通知 ID，以 , 分隔 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**UserNotificationList**](UserNotificationList.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoStargazers"></a>
# **getReposOwnerRepoStargazers**
> List&lt;ProjectStargazers&gt; getReposOwnerRepoStargazers(owner, repo, page, perPage)

列出 star 了仓库的用户

列出 star 了仓库的用户

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<ProjectStargazers> result = apiInstance.getReposOwnerRepoStargazers(owner, repo, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getReposOwnerRepoStargazers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;ProjectStargazers&gt;**](ProjectStargazers.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoSubscribers"></a>
# **getReposOwnerRepoSubscribers**
> List&lt;ProjectWatchers&gt; getReposOwnerRepoSubscribers(owner, repo, page, perPage)

列出 watch 了仓库的用户

列出 watch 了仓库的用户

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<ProjectWatchers> result = apiInstance.getReposOwnerRepoSubscribers(owner, repo, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getReposOwnerRepoSubscribers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;ProjectWatchers&gt;**](ProjectWatchers.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUserStarred"></a>
# **getUserStarred**
> List&lt;Project&gt; getUserStarred(sort, direction, page, perPage)

列出授权用户 star 了的仓库

列出授权用户 star 了的仓库

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
String sort = "created"; // String | 根据仓库创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间
String direction = "desc"; // String | 按递增(asc)或递减(desc)排序，默认：递减
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Project> result = apiInstance.getUserStarred(sort, direction, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getUserStarred");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sort** | **String**| 根据仓库创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间 | [optional] [default to created] [enum: created, last_push]
 **direction** | **String**| 按递增(asc)或递减(desc)排序，默认：递减 | [optional] [default to desc] [enum: asc, desc]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;Project&gt;**](Project.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUserStarredOwnerRepo"></a>
# **getUserStarredOwnerRepo**
> getUserStarredOwnerRepo(owner, repo)

检查授权用户是否 star 了一个仓库

检查授权用户是否 star 了一个仓库

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
try {
    apiInstance.getUserStarredOwnerRepo(owner, repo);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getUserStarredOwnerRepo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getUserSubscriptions"></a>
# **getUserSubscriptions**
> List&lt;Project&gt; getUserSubscriptions(sort, direction, page, perPage)

列出授权用户 watch 了的仓库

列出授权用户 watch 了的仓库

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
String sort = "created"; // String | 根据仓库创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间
String direction = "desc"; // String | 按递增(asc)或递减(desc)排序，默认：递减
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Project> result = apiInstance.getUserSubscriptions(sort, direction, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getUserSubscriptions");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sort** | **String**| 根据仓库创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间 | [optional] [default to created] [enum: created, last_push]
 **direction** | **String**| 按递增(asc)或递减(desc)排序，默认：递减 | [optional] [default to desc] [enum: asc, desc]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;Project&gt;**](Project.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUserSubscriptionsOwnerRepo"></a>
# **getUserSubscriptionsOwnerRepo**
> getUserSubscriptionsOwnerRepo(owner, repo)

检查授权用户是否 watch 了一个仓库

检查授权用户是否 watch 了一个仓库

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
try {
    apiInstance.getUserSubscriptionsOwnerRepo(owner, repo);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getUserSubscriptionsOwnerRepo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getUsersUsernameEvents"></a>
# **getUsersUsernameEvents**
> List&lt;Event&gt; getUsersUsernameEvents(username, prevId, limit)

列出用户的动态

列出用户的动态

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
String username = "username_example"; // String | 用户名(username/login)
Integer prevId = 56; // Integer | 滚动列表的最后一条记录的id
Integer limit = 20; // Integer | 滚动列表每页的数量，最大为 100
try {
    List<Event> result = apiInstance.getUsersUsernameEvents(username, prevId, limit);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getUsersUsernameEvents");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **prevId** | **Integer**| 滚动列表的最后一条记录的id | [optional]
 **limit** | **Integer**| 滚动列表每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Event&gt;**](Event.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUsersUsernameEventsOrgsOrg"></a>
# **getUsersUsernameEventsOrgsOrg**
> List&lt;Event&gt; getUsersUsernameEventsOrgsOrg(username, org, prevId, limit)

列出用户所属组织的动态

列出用户所属组织的动态

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
String username = "username_example"; // String | 用户名(username/login)
String org = "org_example"; // String | 组织的路径(path/login)
Integer prevId = 56; // Integer | 滚动列表的最后一条记录的id
Integer limit = 20; // Integer | 滚动列表每页的数量，最大为 100
try {
    List<Event> result = apiInstance.getUsersUsernameEventsOrgsOrg(username, org, prevId, limit);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getUsersUsernameEventsOrgsOrg");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **org** | **String**| 组织的路径(path/login) |
 **prevId** | **Integer**| 滚动列表的最后一条记录的id | [optional]
 **limit** | **Integer**| 滚动列表每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Event&gt;**](Event.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUsersUsernameEventsPublic"></a>
# **getUsersUsernameEventsPublic**
> List&lt;Event&gt; getUsersUsernameEventsPublic(username, prevId, limit)

列出用户的公开动态

列出用户的公开动态

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
String username = "username_example"; // String | 用户名(username/login)
Integer prevId = 56; // Integer | 滚动列表的最后一条记录的id
Integer limit = 20; // Integer | 滚动列表每页的数量，最大为 100
try {
    List<Event> result = apiInstance.getUsersUsernameEventsPublic(username, prevId, limit);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getUsersUsernameEventsPublic");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **prevId** | **Integer**| 滚动列表的最后一条记录的id | [optional]
 **limit** | **Integer**| 滚动列表每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Event&gt;**](Event.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUsersUsernameReceivedEvents"></a>
# **getUsersUsernameReceivedEvents**
> List&lt;Event&gt; getUsersUsernameReceivedEvents(username, prevId, limit)

列出一个用户收到的动态

列出一个用户收到的动态

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
String username = "username_example"; // String | 用户名(username/login)
Integer prevId = 56; // Integer | 滚动列表的最后一条记录的id
Integer limit = 20; // Integer | 滚动列表每页的数量，最大为 100
try {
    List<Event> result = apiInstance.getUsersUsernameReceivedEvents(username, prevId, limit);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getUsersUsernameReceivedEvents");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **prevId** | **Integer**| 滚动列表的最后一条记录的id | [optional]
 **limit** | **Integer**| 滚动列表每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Event&gt;**](Event.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUsersUsernameReceivedEventsPublic"></a>
# **getUsersUsernameReceivedEventsPublic**
> List&lt;Event&gt; getUsersUsernameReceivedEventsPublic(username, prevId, limit)

列出一个用户收到的公开动态

列出一个用户收到的公开动态

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
String username = "username_example"; // String | 用户名(username/login)
Integer prevId = 56; // Integer | 滚动列表的最后一条记录的id
Integer limit = 20; // Integer | 滚动列表每页的数量，最大为 100
try {
    List<Event> result = apiInstance.getUsersUsernameReceivedEventsPublic(username, prevId, limit);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getUsersUsernameReceivedEventsPublic");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **prevId** | **Integer**| 滚动列表的最后一条记录的id | [optional]
 **limit** | **Integer**| 滚动列表每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Event&gt;**](Event.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUsersUsernameStarred"></a>
# **getUsersUsernameStarred**
> List&lt;Project&gt; getUsersUsernameStarred(username, prevId, limit, sort, direction)

列出用户 star 了的仓库

列出用户 star 了的仓库

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
String username = "username_example"; // String | 用户名(username/login)
Integer prevId = 56; // Integer | 滚动列表的最后一条记录的id
Integer limit = 20; // Integer | 滚动列表每页的数量，最大为 100
String sort = "created"; // String | 根据仓库创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间
String direction = "desc"; // String | 按递增(asc)或递减(desc)排序，默认：递减
try {
    List<Project> result = apiInstance.getUsersUsernameStarred(username, prevId, limit, sort, direction);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getUsersUsernameStarred");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **prevId** | **Integer**| 滚动列表的最后一条记录的id | [optional]
 **limit** | **Integer**| 滚动列表每页的数量，最大为 100 | [optional] [default to 20]
 **sort** | **String**| 根据仓库创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间 | [optional] [default to created] [enum: created, last_push]
 **direction** | **String**| 按递增(asc)或递减(desc)排序，默认：递减 | [optional] [default to desc] [enum: asc, desc]

### Return type

[**List&lt;Project&gt;**](Project.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUsersUsernameSubscriptions"></a>
# **getUsersUsernameSubscriptions**
> List&lt;Project&gt; getUsersUsernameSubscriptions(username, prevId, limit, sort, direction)

列出用户 watch 了的仓库

列出用户 watch 了的仓库

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
String username = "username_example"; // String | 用户名(username/login)
Integer prevId = 56; // Integer | 滚动列表的最后一条记录的id
Integer limit = 20; // Integer | 滚动列表每页的数量，最大为 100
String sort = "created"; // String | 根据仓库创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间
String direction = "desc"; // String | 按递增(asc)或递减(desc)排序，默认：递减
try {
    List<Project> result = apiInstance.getUsersUsernameSubscriptions(username, prevId, limit, sort, direction);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getUsersUsernameSubscriptions");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **prevId** | **Integer**| 滚动列表的最后一条记录的id | [optional]
 **limit** | **Integer**| 滚动列表每页的数量，最大为 100 | [optional] [default to 20]
 **sort** | **String**| 根据仓库创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间 | [optional] [default to created] [enum: created, last_push]
 **direction** | **String**| 按递增(asc)或递减(desc)排序，默认：递减 | [optional] [default to desc] [enum: asc, desc]

### Return type

[**List&lt;Project&gt;**](Project.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="patchNotificationsMessagesId"></a>
# **patchNotificationsMessagesId**
> patchNotificationsMessagesId(id)

标记一条私信为已读

标记一条私信为已读

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
Integer id = 56; // Integer | 私信的 ID
try {
    apiInstance.patchNotificationsMessagesId(id);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#patchNotificationsMessagesId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| 私信的 ID |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="patchNotificationsThreadsId"></a>
# **patchNotificationsThreadsId**
> patchNotificationsThreadsId(id)

标记一条通知为已读

标记一条通知为已读

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
Integer id = 56; // Integer | 通知的 ID
try {
    apiInstance.patchNotificationsThreadsId(id);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#patchNotificationsThreadsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| 通知的 ID |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="postNotificationsMessages"></a>
# **postNotificationsMessages**
> UserMessage postNotificationsMessages(body)

发送私信给指定用户

发送私信给指定用户

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
NotificationsMessagesBody1 body = new NotificationsMessagesBody1(); // NotificationsMessagesBody1 | 
try {
    UserMessage result = apiInstance.postNotificationsMessages(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#postNotificationsMessages");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**NotificationsMessagesBody1**](NotificationsMessagesBody1.md)|  | [optional]

### Return type

[**UserMessage**](UserMessage.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putNotificationsMessages"></a>
# **putNotificationsMessages**
> putNotificationsMessages(body)

标记所有私信为已读

标记所有私信为已读

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
NotificationsMessagesBody body = new NotificationsMessagesBody(); // NotificationsMessagesBody | 
try {
    apiInstance.putNotificationsMessages(body);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#putNotificationsMessages");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**NotificationsMessagesBody**](NotificationsMessagesBody.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="putNotificationsThreads"></a>
# **putNotificationsThreads**
> putNotificationsThreads(body)

标记所有通知为已读

标记所有通知为已读

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
NotificationsThreadsBody body = new NotificationsThreadsBody(); // NotificationsThreadsBody | 
try {
    apiInstance.putNotificationsThreads(body);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#putNotificationsThreads");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**NotificationsThreadsBody**](NotificationsThreadsBody.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="putReposOwnerRepoNotifications"></a>
# **putReposOwnerRepoNotifications**
> putReposOwnerRepoNotifications(owner, repo, body)

标记一个仓库里的通知为已读

标记一个仓库里的通知为已读

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
RepoNotificationsBody body = new RepoNotificationsBody(); // RepoNotificationsBody | 
try {
    apiInstance.putReposOwnerRepoNotifications(owner, repo, body);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#putReposOwnerRepoNotifications");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **body** | [**RepoNotificationsBody**](RepoNotificationsBody.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="putUserStarredOwnerRepo"></a>
# **putUserStarredOwnerRepo**
> putUserStarredOwnerRepo(owner, repo)

star 一个仓库

star 一个仓库

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
try {
    apiInstance.putUserStarredOwnerRepo(owner, repo);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#putUserStarredOwnerRepo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="putUserSubscriptionsOwnerRepo"></a>
# **putUserSubscriptionsOwnerRepo**
> putUserSubscriptionsOwnerRepo(owner, repo, body)

watch 一个仓库

watch 一个仓库

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.ActivityApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

ActivityApi apiInstance = new ActivityApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
OwnerRepoBody1 body = new OwnerRepoBody1(); // OwnerRepoBody1 | 
try {
    apiInstance.putUserSubscriptionsOwnerRepo(owner, repo, body);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#putUserSubscriptionsOwnerRepo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **body** | [**OwnerRepoBody1**](OwnerRepoBody1.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

