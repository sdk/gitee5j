# AttachFile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**browserDownloadUrl** | **String** |  |  [optional]
**id** | **Integer** |  |  [optional]
**name** | **String** |  |  [optional]
**size** | **Integer** |  |  [optional]
**uploader** | [**UserMini**](UserMini.md) |  |  [optional]
