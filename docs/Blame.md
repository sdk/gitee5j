# Blame

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**commit** | [**Commit**](Commit.md) |  |  [optional]
**lines** | **List&lt;String&gt;** | 代码行 |  [optional]
