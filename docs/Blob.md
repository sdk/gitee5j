# Blob

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content** | **String** |  |  [optional]
**encoding** | **String** |  |  [optional]
**sha** | **String** |  |  [optional]
**size** | **Integer** |  |  [optional]
**url** | **String** |  |  [optional]
