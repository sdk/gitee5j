# Branch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**commit** | [**BranchCommit**](BranchCommit.md) |  |  [optional]
**name** | **String** |  |  [optional]
**_protected** | **Boolean** |  |  [optional]
**protectionUrl** | **String** |  |  [optional]
