# BranchCommit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**commit** | [**GitCommit**](GitCommit.md) |  |  [optional]
**sha** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
