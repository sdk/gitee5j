# Code

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**comments** | **Integer** |  |  [optional]
**commentsUrl** | **String** |  |  [optional]
**commitsUrl** | **String** |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**description** | **String** |  |  [optional]
**files** | [**Map&lt;String, CodeFile&gt;**](CodeFile.md) |  |  [optional]
**forksUrl** | **String** |  |  [optional]
**gitPullUrl** | **String** |  |  [optional]
**gitPushUrl** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**id** | **String** |  |  [optional]
**owner** | [**UserBasic**](UserBasic.md) |  |  [optional]
**_public** | **Boolean** |  |  [optional]
**truncated** | **Boolean** |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**url** | **String** |  |  [optional]
**user** | [**UserBasic**](UserBasic.md) |  |  [optional]
