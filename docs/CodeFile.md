# CodeFile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content** | **String** |  |  [optional]
**rawUrl** | **String** |  |  [optional]
**size** | **Integer** |  |  [optional]
**truncated** | **Boolean** |  |  [optional]
**type** | **String** |  |  [optional]
