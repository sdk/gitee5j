# Commit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**author** | [**GitUser**](GitUser.md) |  |  [optional]
**committer** | [**GitUser**](GitUser.md) |  |  [optional]
**message** | **String** |  |  [optional]
**parents** | [**List&lt;GitSha&gt;**](GitSha.md) |  |  [optional]
**sha** | **String** |  |  [optional]
**tree** | [**GitSha**](GitSha.md) |  |  [optional]
