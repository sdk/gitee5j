# CommitContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**commit** | [**Commit**](Commit.md) |  |  [optional]
**content** | [**ContentBasic**](ContentBasic.md) |  |  [optional]
