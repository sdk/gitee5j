# CommitParentsBasic

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sha** | **String** | 第一个父级commit的sha值（即将废弃） |  [optional]
**shas** | **List&lt;String&gt;** | 全部父级 commit 的 sha 值 |  [optional]
**url** | **String** |  |  [optional]
