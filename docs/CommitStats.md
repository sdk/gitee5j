# CommitStats

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**additions** | **Integer** |  |  [optional]
**deletions** | **Integer** |  |  [optional]
**id** | **String** |  |  [optional]
