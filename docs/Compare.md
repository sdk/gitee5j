# Compare

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**baseCommit** | [**RepoCommit**](RepoCommit.md) |  |  [optional]
**commits** | [**List&lt;RepoCommit&gt;**](RepoCommit.md) | commits 数量限制在 100 以内 |  [optional]
**files** | [**List&lt;DiffFile&gt;**](DiffFile.md) | 文件列表 |  [optional]
**mergeBaseCommit** | [**RepoCommit**](RepoCommit.md) |  |  [optional]
**truncated** | **Boolean** | 文件列表是否被截断 |  [optional]
