# CompleteBranch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_links** | [**Link**](Link.md) |  |  [optional]
**commit** | [**Commit**](Commit.md) |  |  [optional]
**name** | **String** |  |  [optional]
**_protected** | **Boolean** |  |  [optional]
**protectionUrl** | **String** |  |  [optional]
