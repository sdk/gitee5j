# ContentBasic

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_links** | [**Link**](Link.md) |  |  [optional]
**downloadUrl** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**path** | **String** |  |  [optional]
**sha** | **String** |  |  [optional]
**size** | **Integer** |  |  [optional]
**type** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
