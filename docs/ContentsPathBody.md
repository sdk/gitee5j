# ContentsPathBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**author** | [**GitUserBasic**](GitUserBasic.md) |  |  [optional]
**branch** | **String** | 分支名称。默认为仓库对默认分支 |  [optional]
**committer** | [**GitUserBasic**](GitUserBasic.md) |  |  [optional]
**content** | **String** | 文件内容, 要用 base64 编码 | 
**message** | **String** | 提交信息 | 
**sha** | **String** | 文件的 Blob SHA，可通过 [获取仓库具体路径下的内容] API 获取 | 
