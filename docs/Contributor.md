# Contributor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contributions** | **Integer** |  |  [optional]
**email** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
