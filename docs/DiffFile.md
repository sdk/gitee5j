# DiffFile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**additions** | **Integer** | 新增行数 |  [optional]
**blobUrl** | **String** | blob 链接 |  [optional]
**changes** | **Integer** | 变更行数 |  [optional]
**contentUrl** | **String** | content 链接 |  [optional]
**deletions** | **Integer** | 删除行数 |  [optional]
**filename** | **String** | 文件路径 |  [optional]
**patch** | **String** | patch |  [optional]
**rawUrl** | **String** | raw 链接 |  [optional]
**sha** | **String** |  |  [optional]
**status** | **String** | 文件状态 |  [optional]
**truncated** | **Boolean** | patch 内容是否被截断 |  [optional]
