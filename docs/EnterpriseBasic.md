# EnterpriseBasic

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**avatarUrl** | **String** | 企业头像地址 |  [optional]
**id** | **Integer** | 企业ID |  [optional]
**name** | **String** | 企业名称 |  [optional]
**path** | **String** | 企业命名空间 |  [optional]
**url** | **String** | 企业地址 |  [optional]
