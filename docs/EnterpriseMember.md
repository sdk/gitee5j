# EnterpriseMember

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **Boolean** |  |  [optional]
**enterprise** | [**EnterpriseBasic**](EnterpriseBasic.md) |  |  [optional]
**outsourced** | **Boolean** |  |  [optional]
**remark** | **String** |  |  [optional]
**role** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**user** | [**UserMini**](UserMini.md) |  |  [optional]
