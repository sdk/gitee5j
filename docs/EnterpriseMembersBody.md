# EnterpriseMembersBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** | 要添加邮箱地址，若该邮箱未注册则自动创建帐号。username,email至少填写一个 |  [optional]
**name** | **String** | 企业成员真实姓名（备注） |  [optional]
**role** | [**RoleEnum**](#RoleEnum) | 企业角色：member &#x3D;&gt; 普通成员, outsourced &#x3D;&gt; 外包成员, admin &#x3D;&gt; 管理员 |  [optional]
**username** | **String** | 需要邀请的用户名(username/login)，username,email至少填写一个 |  [optional]

<a name="RoleEnum"></a>
## Enum: RoleEnum
Name | Value
---- | -----
ADMIN | &quot;admin&quot;
MEMBER | &quot;member&quot;
OUTSOURCED | &quot;outsourced&quot;
