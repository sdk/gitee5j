# EnterpriseWeekReportBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content** | **String** | 周报内容 | 
**date** | **String** | 周报日期(格式：2019-03-25) |  [optional]
**weekIndex** | **Integer** | 周报所属周 | 
**year** | **Integer** | 周报所属年 | 
