# EnterprisesApi

All URIs are relative to *https://gitee.com/api/v5*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteEnterprisesEnterpriseMembersUsername**](EnterprisesApi.md#deleteEnterprisesEnterpriseMembersUsername) | **DELETE** /enterprises/{enterprise}/members/{username} | 移除企业成员
[**deleteEnterprisesEnterpriseWeekReportsReportIdCommentsId**](EnterprisesApi.md#deleteEnterprisesEnterpriseWeekReportsReportIdCommentsId) | **DELETE** /enterprises/{enterprise}/week_reports/{report_id}/comments/{id} | 删除周报某个评论
[**getEnterpriseEnterprisePullRequests**](EnterprisesApi.md#getEnterpriseEnterprisePullRequests) | **GET** /enterprise/{enterprise}/pull_requests | 企业 Pull Request 列表
[**getEnterprisesEnterprise**](EnterprisesApi.md#getEnterprisesEnterprise) | **GET** /enterprises/{enterprise} | 获取一个企业
[**getEnterprisesEnterpriseMembers**](EnterprisesApi.md#getEnterprisesEnterpriseMembers) | **GET** /enterprises/{enterprise}/members | 列出企业的所有成员
[**getEnterprisesEnterpriseMembersSearch**](EnterprisesApi.md#getEnterprisesEnterpriseMembersSearch) | **GET** /enterprises/{enterprise}/members/search | 获取企业成员信息(通过用户名/邮箱)
[**getEnterprisesEnterpriseMembersUsername**](EnterprisesApi.md#getEnterprisesEnterpriseMembersUsername) | **GET** /enterprises/{enterprise}/members/{username} | 获取企业的一个成员
[**getEnterprisesEnterpriseUsersUsernameWeekReports**](EnterprisesApi.md#getEnterprisesEnterpriseUsersUsernameWeekReports) | **GET** /enterprises/{enterprise}/users/{username}/week_reports | 个人周报列表
[**getEnterprisesEnterpriseWeekReports**](EnterprisesApi.md#getEnterprisesEnterpriseWeekReports) | **GET** /enterprises/{enterprise}/week_reports | 企业成员周报列表
[**getEnterprisesEnterpriseWeekReportsId**](EnterprisesApi.md#getEnterprisesEnterpriseWeekReportsId) | **GET** /enterprises/{enterprise}/week_reports/{id} | 周报详情
[**getEnterprisesEnterpriseWeekReportsIdComments**](EnterprisesApi.md#getEnterprisesEnterpriseWeekReportsIdComments) | **GET** /enterprises/{enterprise}/week_reports/{id}/comments | 某个周报评论列表
[**getUserEnterprises**](EnterprisesApi.md#getUserEnterprises) | **GET** /user/enterprises | 列出授权用户所属的企业
[**patchEnterprisesEnterpriseWeekReportId**](EnterprisesApi.md#patchEnterprisesEnterpriseWeekReportId) | **PATCH** /enterprises/{enterprise}/week_report/{id} | 编辑周报
[**postEnterprisesEnterpriseMembers**](EnterprisesApi.md#postEnterprisesEnterpriseMembers) | **POST** /enterprises/{enterprise}/members | 添加或邀请企业成员
[**postEnterprisesEnterpriseWeekReport**](EnterprisesApi.md#postEnterprisesEnterpriseWeekReport) | **POST** /enterprises/{enterprise}/week_report | 新建周报
[**postEnterprisesEnterpriseWeekReportsIdComment**](EnterprisesApi.md#postEnterprisesEnterpriseWeekReportsIdComment) | **POST** /enterprises/{enterprise}/week_reports/{id}/comment | 评论周报
[**putEnterprisesEnterpriseMembersUsername**](EnterprisesApi.md#putEnterprisesEnterpriseMembersUsername) | **PUT** /enterprises/{enterprise}/members/{username} | 修改企业成员权限或备注

<a name="deleteEnterprisesEnterpriseMembersUsername"></a>
# **deleteEnterprisesEnterpriseMembersUsername**
> deleteEnterprisesEnterpriseMembersUsername(enterprise, username)

移除企业成员

移除企业成员

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.EnterprisesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String username = "username_example"; // String | 用户名(username/login)
try {
    apiInstance.deleteEnterprisesEnterpriseMembersUsername(enterprise, username);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#deleteEnterprisesEnterpriseMembersUsername");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **username** | **String**| 用户名(username/login) |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteEnterprisesEnterpriseWeekReportsReportIdCommentsId"></a>
# **deleteEnterprisesEnterpriseWeekReportsReportIdCommentsId**
> deleteEnterprisesEnterpriseWeekReportsReportIdCommentsId(enterprise, reportId, id)

删除周报某个评论

删除周报某个评论

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.EnterprisesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
Integer reportId = 56; // Integer | 周报ID
Integer id = 56; // Integer | 评论ID
try {
    apiInstance.deleteEnterprisesEnterpriseWeekReportsReportIdCommentsId(enterprise, reportId, id);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#deleteEnterprisesEnterpriseWeekReportsReportIdCommentsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **reportId** | **Integer**| 周报ID |
 **id** | **Integer**| 评论ID |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getEnterpriseEnterprisePullRequests"></a>
# **getEnterpriseEnterprisePullRequests**
> List&lt;PullRequest&gt; getEnterpriseEnterprisePullRequests(enterprise, issueNumber, repo, programId, state, head, base, sort, since, direction, milestoneNumber, labels, page, perPage)

企业 Pull Request 列表

企业 Pull Request 列表

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.EnterprisesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String issueNumber = "issueNumber_example"; // String | 可选。Issue 编号(区分大小写，无需添加 # 号)
String repo = "repo_example"; // String | 可选。仓库路径(path)
Integer programId = 56; // Integer | 可选。项目ID
String state = "open"; // String | 可选。Pull Request 状态
String head = "head_example"; // String | 可选。Pull Request 提交的源分支。格式：branch 或者：username:branch
String base = "base_example"; // String | 可选。Pull Request 提交目标分支的名称。
String sort = "created"; // String | 可选。排序字段，默认按创建时间
String since = "since_example"; // String | 可选。起始的更新时间，要求时间格式为 ISO 8601
String direction = "desc"; // String | 可选。升序/降序
Integer milestoneNumber = 56; // Integer | 可选。里程碑序号(id)
String labels = "labels_example"; // String | 用逗号分开的标签。如: bug,performance
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<PullRequest> result = apiInstance.getEnterpriseEnterprisePullRequests(enterprise, issueNumber, repo, programId, state, head, base, sort, since, direction, milestoneNumber, labels, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#getEnterpriseEnterprisePullRequests");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **issueNumber** | **String**| 可选。Issue 编号(区分大小写，无需添加 # 号) | [optional]
 **repo** | **String**| 可选。仓库路径(path) | [optional]
 **programId** | **Integer**| 可选。项目ID | [optional]
 **state** | **String**| 可选。Pull Request 状态 | [optional] [default to open] [enum: open, closed, merged, all]
 **head** | **String**| 可选。Pull Request 提交的源分支。格式：branch 或者：username:branch | [optional]
 **base** | **String**| 可选。Pull Request 提交目标分支的名称。 | [optional]
 **sort** | **String**| 可选。排序字段，默认按创建时间 | [optional] [default to created] [enum: created, updated, popularity, long-running]
 **since** | **String**| 可选。起始的更新时间，要求时间格式为 ISO 8601 | [optional]
 **direction** | **String**| 可选。升序/降序 | [optional] [default to desc] [enum: asc, desc]
 **milestoneNumber** | **Integer**| 可选。里程碑序号(id) | [optional]
 **labels** | **String**| 用逗号分开的标签。如: bug,performance | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;PullRequest&gt;**](PullRequest.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getEnterprisesEnterprise"></a>
# **getEnterprisesEnterprise**
> EnterpriseBasic getEnterprisesEnterprise(enterprise)

获取一个企业

获取一个企业

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.EnterprisesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
try {
    EnterpriseBasic result = apiInstance.getEnterprisesEnterprise(enterprise);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#getEnterprisesEnterprise");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |

### Return type

[**EnterpriseBasic**](EnterpriseBasic.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getEnterprisesEnterpriseMembers"></a>
# **getEnterprisesEnterpriseMembers**
> List&lt;EnterpriseMember&gt; getEnterprisesEnterpriseMembers(enterprise, role, page, perPage)

列出企业的所有成员

列出企业的所有成员

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.EnterprisesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String role = "all"; // String | 根据角色筛选成员
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<EnterpriseMember> result = apiInstance.getEnterprisesEnterpriseMembers(enterprise, role, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#getEnterprisesEnterpriseMembers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **role** | **String**| 根据角色筛选成员 | [optional] [default to all] [enum: all, admin, member]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;EnterpriseMember&gt;**](EnterpriseMember.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getEnterprisesEnterpriseMembersSearch"></a>
# **getEnterprisesEnterpriseMembersSearch**
> EnterpriseMember getEnterprisesEnterpriseMembersSearch(enterprise, queryType, queryValue)

获取企业成员信息(通过用户名/邮箱)

获取企业成员信息(通过用户名/邮箱)

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.EnterprisesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String queryType = "queryType_example"; // String | 查询类型：username/email
String queryValue = "queryValue_example"; // String | 查询值
try {
    EnterpriseMember result = apiInstance.getEnterprisesEnterpriseMembersSearch(enterprise, queryType, queryValue);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#getEnterprisesEnterpriseMembersSearch");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **queryType** | **String**| 查询类型：username/email |
 **queryValue** | **String**| 查询值 |

### Return type

[**EnterpriseMember**](EnterpriseMember.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getEnterprisesEnterpriseMembersUsername"></a>
# **getEnterprisesEnterpriseMembersUsername**
> EnterpriseMember getEnterprisesEnterpriseMembersUsername(enterprise, username)

获取企业的一个成员

获取企业的一个成员

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.EnterprisesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String username = "username_example"; // String | 用户名(username/login)
try {
    EnterpriseMember result = apiInstance.getEnterprisesEnterpriseMembersUsername(enterprise, username);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#getEnterprisesEnterpriseMembersUsername");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **username** | **String**| 用户名(username/login) |

### Return type

[**EnterpriseMember**](EnterpriseMember.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getEnterprisesEnterpriseUsersUsernameWeekReports"></a>
# **getEnterprisesEnterpriseUsersUsernameWeekReports**
> List&lt;WeekReport&gt; getEnterprisesEnterpriseUsersUsernameWeekReports(enterprise, username, page, perPage)

个人周报列表

个人周报列表

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.EnterprisesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String username = "username_example"; // String | 用户名(username/login)
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<WeekReport> result = apiInstance.getEnterprisesEnterpriseUsersUsernameWeekReports(enterprise, username, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#getEnterprisesEnterpriseUsersUsernameWeekReports");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **username** | **String**| 用户名(username/login) |
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;WeekReport&gt;**](WeekReport.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getEnterprisesEnterpriseWeekReports"></a>
# **getEnterprisesEnterpriseWeekReports**
> List&lt;WeekReport&gt; getEnterprisesEnterpriseWeekReports(enterprise, page, perPage, username, year, weekIndex, date)

企业成员周报列表

企业成员周报列表

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.EnterprisesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
String username = "username_example"; // String | 用户名(username/login)
Integer year = 56; // Integer | 周报所属年
Integer weekIndex = 56; // Integer | 周报所属周
String date = "date_example"; // String | 周报日期(格式：2019-03-25)
try {
    List<WeekReport> result = apiInstance.getEnterprisesEnterpriseWeekReports(enterprise, page, perPage, username, year, weekIndex, date);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#getEnterprisesEnterpriseWeekReports");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]
 **username** | **String**| 用户名(username/login) | [optional]
 **year** | **Integer**| 周报所属年 | [optional]
 **weekIndex** | **Integer**| 周报所属周 | [optional]
 **date** | **String**| 周报日期(格式：2019-03-25) | [optional]

### Return type

[**List&lt;WeekReport&gt;**](WeekReport.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getEnterprisesEnterpriseWeekReportsId"></a>
# **getEnterprisesEnterpriseWeekReportsId**
> WeekReport getEnterprisesEnterpriseWeekReportsId(enterprise, id)

周报详情

周报详情

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.EnterprisesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
Integer id = 56; // Integer | 周报ID
try {
    WeekReport result = apiInstance.getEnterprisesEnterpriseWeekReportsId(enterprise, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#getEnterprisesEnterpriseWeekReportsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **id** | **Integer**| 周报ID |

### Return type

[**WeekReport**](WeekReport.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getEnterprisesEnterpriseWeekReportsIdComments"></a>
# **getEnterprisesEnterpriseWeekReportsIdComments**
> List&lt;Note&gt; getEnterprisesEnterpriseWeekReportsIdComments(enterprise, id, page, perPage)

某个周报评论列表

某个周报评论列表

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.EnterprisesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
Integer id = 56; // Integer | 周报ID
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Note> result = apiInstance.getEnterprisesEnterpriseWeekReportsIdComments(enterprise, id, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#getEnterprisesEnterpriseWeekReportsIdComments");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **id** | **Integer**| 周报ID |
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;Note&gt;**](Note.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUserEnterprises"></a>
# **getUserEnterprises**
> List&lt;EnterpriseBasic&gt; getUserEnterprises(page, perPage, admin)

列出授权用户所属的企业

列出授权用户所属的企业

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.EnterprisesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

EnterprisesApi apiInstance = new EnterprisesApi();
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
Boolean admin = true; // Boolean | 只列出授权用户管理的企业
try {
    List<EnterpriseBasic> result = apiInstance.getUserEnterprises(page, perPage, admin);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#getUserEnterprises");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]
 **admin** | **Boolean**| 只列出授权用户管理的企业 | [optional] [default to true]

### Return type

[**List&lt;EnterpriseBasic&gt;**](EnterpriseBasic.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="patchEnterprisesEnterpriseWeekReportId"></a>
# **patchEnterprisesEnterpriseWeekReportId**
> WeekReport patchEnterprisesEnterpriseWeekReportId(enterprise, id, body)

编辑周报

编辑周报

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.EnterprisesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
Integer id = 56; // Integer | 周报ID
WeekReportIdBody body = new WeekReportIdBody(); // WeekReportIdBody | 
try {
    WeekReport result = apiInstance.patchEnterprisesEnterpriseWeekReportId(enterprise, id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#patchEnterprisesEnterpriseWeekReportId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **id** | **Integer**| 周报ID |
 **body** | [**WeekReportIdBody**](WeekReportIdBody.md)|  | [optional]

### Return type

[**WeekReport**](WeekReport.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/json

<a name="postEnterprisesEnterpriseMembers"></a>
# **postEnterprisesEnterpriseMembers**
> postEnterprisesEnterpriseMembers(enterprise, body)

添加或邀请企业成员

添加或邀请企业成员

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.EnterprisesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
EnterpriseMembersBody body = new EnterpriseMembersBody(); // EnterpriseMembersBody | 
try {
    apiInstance.postEnterprisesEnterpriseMembers(enterprise, body);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#postEnterprisesEnterpriseMembers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **body** | [**EnterpriseMembersBody**](EnterpriseMembersBody.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="postEnterprisesEnterpriseWeekReport"></a>
# **postEnterprisesEnterpriseWeekReport**
> WeekReport postEnterprisesEnterpriseWeekReport(enterprise, body)

新建周报

新建周报

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.EnterprisesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
EnterpriseWeekReportBody body = new EnterpriseWeekReportBody(); // EnterpriseWeekReportBody | 
try {
    WeekReport result = apiInstance.postEnterprisesEnterpriseWeekReport(enterprise, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#postEnterprisesEnterpriseWeekReport");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **body** | [**EnterpriseWeekReportBody**](EnterpriseWeekReportBody.md)|  | [optional]

### Return type

[**WeekReport**](WeekReport.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postEnterprisesEnterpriseWeekReportsIdComment"></a>
# **postEnterprisesEnterpriseWeekReportsIdComment**
> Note postEnterprisesEnterpriseWeekReportsIdComment(enterprise, id, body)

评论周报

评论周报

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.EnterprisesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
Integer id = 56; // Integer | 周报ID
IdCommentBody body = new IdCommentBody(); // IdCommentBody | 
try {
    Note result = apiInstance.postEnterprisesEnterpriseWeekReportsIdComment(enterprise, id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#postEnterprisesEnterpriseWeekReportsIdComment");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **id** | **Integer**| 周报ID |
 **body** | [**IdCommentBody**](IdCommentBody.md)|  | [optional]

### Return type

[**Note**](Note.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putEnterprisesEnterpriseMembersUsername"></a>
# **putEnterprisesEnterpriseMembersUsername**
> EnterpriseMember putEnterprisesEnterpriseMembersUsername(enterprise, username, body)

修改企业成员权限或备注

修改企业成员权限或备注

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.EnterprisesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String username = "username_example"; // String | 用户名(username/login)
MembersUsernameBody body = new MembersUsernameBody(); // MembersUsernameBody | 
try {
    EnterpriseMember result = apiInstance.putEnterprisesEnterpriseMembersUsername(enterprise, username, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#putEnterprisesEnterpriseMembersUsername");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **username** | **String**| 用户名(username/login) |
 **body** | [**MembersUsernameBody**](MembersUsernameBody.md)|  | [optional]

### Return type

[**EnterpriseMember**](EnterpriseMember.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

