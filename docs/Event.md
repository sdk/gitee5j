# Event

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**actor** | [**UserMini**](UserMini.md) |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**id** | **Integer** |  |  [optional]
**org** | [**GroupBasic**](GroupBasic.md) |  |  [optional]
**payload** | **Object** | 不同类型动态的内容 |  [optional]
**_public** | **Boolean** |  |  [optional]
**repo** | [**ProjectMini**](ProjectMini.md) |  |  [optional]
**type** | **String** |  |  [optional]
