# GistsApi

All URIs are relative to *https://gitee.com/api/v5*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteGistsGistIdCommentsId**](GistsApi.md#deleteGistsGistIdCommentsId) | **DELETE** /gists/{gist_id}/comments/{id} | 删除代码片段的评论
[**deleteGistsId**](GistsApi.md#deleteGistsId) | **DELETE** /gists/{id} | 删除指定代码片段
[**deleteGistsIdStar**](GistsApi.md#deleteGistsIdStar) | **DELETE** /gists/{id}/star | 取消Star代码片段
[**getGists**](GistsApi.md#getGists) | **GET** /gists | 获取代码片段
[**getGistsGistIdComments**](GistsApi.md#getGistsGistIdComments) | **GET** /gists/{gist_id}/comments | 获取代码片段的评论
[**getGistsGistIdCommentsId**](GistsApi.md#getGistsGistIdCommentsId) | **GET** /gists/{gist_id}/comments/{id} | 获取单条代码片段的评论
[**getGistsId**](GistsApi.md#getGistsId) | **GET** /gists/{id} | 获取单条代码片段
[**getGistsIdCommits**](GistsApi.md#getGistsIdCommits) | **GET** /gists/{id}/commits | 获取代码片段的commit
[**getGistsIdForks**](GistsApi.md#getGistsIdForks) | **GET** /gists/{id}/forks | 获取 Fork 了指定代码片段的列表
[**getGistsIdStar**](GistsApi.md#getGistsIdStar) | **GET** /gists/{id}/star | 判断代码片段是否已Star
[**getGistsStarred**](GistsApi.md#getGistsStarred) | **GET** /gists/starred | 获取用户Star的代码片段
[**patchGistsGistIdCommentsId**](GistsApi.md#patchGistsGistIdCommentsId) | **PATCH** /gists/{gist_id}/comments/{id} | 修改代码片段的评论
[**patchGistsId**](GistsApi.md#patchGistsId) | **PATCH** /gists/{id} | 修改代码片段
[**postGists**](GistsApi.md#postGists) | **POST** /gists | 创建代码片段
[**postGistsGistIdComments**](GistsApi.md#postGistsGistIdComments) | **POST** /gists/{gist_id}/comments | 增加代码片段的评论
[**postGistsIdForks**](GistsApi.md#postGistsIdForks) | **POST** /gists/{id}/forks | Fork代码片段
[**putGistsIdStar**](GistsApi.md#putGistsIdStar) | **PUT** /gists/{id}/star | Star代码片段

<a name="deleteGistsGistIdCommentsId"></a>
# **deleteGistsGistIdCommentsId**
> deleteGistsGistIdCommentsId(gistId, id)

删除代码片段的评论

删除代码片段的评论

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.GistsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

GistsApi apiInstance = new GistsApi();
String gistId = "gistId_example"; // String | 代码片段的ID
Integer id = 56; // Integer | 评论的ID
try {
    apiInstance.deleteGistsGistIdCommentsId(gistId, id);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#deleteGistsGistIdCommentsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **gistId** | **String**| 代码片段的ID |
 **id** | **Integer**| 评论的ID |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteGistsId"></a>
# **deleteGistsId**
> deleteGistsId(id)

删除指定代码片段

删除指定代码片段

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.GistsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

GistsApi apiInstance = new GistsApi();
String id = "id_example"; // String | 代码片段的ID
try {
    apiInstance.deleteGistsId(id);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#deleteGistsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| 代码片段的ID |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteGistsIdStar"></a>
# **deleteGistsIdStar**
> deleteGistsIdStar(id)

取消Star代码片段

取消Star代码片段

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.GistsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

GistsApi apiInstance = new GistsApi();
String id = "id_example"; // String | 代码片段的ID
try {
    apiInstance.deleteGistsIdStar(id);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#deleteGistsIdStar");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| 代码片段的ID |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getGists"></a>
# **getGists**
> List&lt;Code&gt; getGists(since, page, perPage)

获取代码片段

获取代码片段

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.GistsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

GistsApi apiInstance = new GistsApi();
String since = "since_example"; // String | 起始的更新时间，要求时间格式为 ISO 8601
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Code> result = apiInstance.getGists(since, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#getGists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **since** | **String**| 起始的更新时间，要求时间格式为 ISO 8601 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;Code&gt;**](Code.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getGistsGistIdComments"></a>
# **getGistsGistIdComments**
> List&lt;CodeComment&gt; getGistsGistIdComments(gistId, page, perPage)

获取代码片段的评论

获取代码片段的评论

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.GistsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

GistsApi apiInstance = new GistsApi();
String gistId = "gistId_example"; // String | 代码片段的ID
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<CodeComment> result = apiInstance.getGistsGistIdComments(gistId, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#getGistsGistIdComments");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **gistId** | **String**| 代码片段的ID |
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;CodeComment&gt;**](CodeComment.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getGistsGistIdCommentsId"></a>
# **getGistsGistIdCommentsId**
> CodeComment getGistsGistIdCommentsId(gistId, id)

获取单条代码片段的评论

获取单条代码片段的评论

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.GistsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

GistsApi apiInstance = new GistsApi();
String gistId = "gistId_example"; // String | 代码片段的ID
Integer id = 56; // Integer | 评论的ID
try {
    CodeComment result = apiInstance.getGistsGistIdCommentsId(gistId, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#getGistsGistIdCommentsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **gistId** | **String**| 代码片段的ID |
 **id** | **Integer**| 评论的ID |

### Return type

[**CodeComment**](CodeComment.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getGistsId"></a>
# **getGistsId**
> CodeForksHistory getGistsId(id)

获取单条代码片段

获取单条代码片段

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.GistsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

GistsApi apiInstance = new GistsApi();
String id = "id_example"; // String | 代码片段的ID
try {
    CodeForksHistory result = apiInstance.getGistsId(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#getGistsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| 代码片段的ID |

### Return type

[**CodeForksHistory**](CodeForksHistory.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getGistsIdCommits"></a>
# **getGistsIdCommits**
> CodeForksHistory getGistsIdCommits(id)

获取代码片段的commit

获取代码片段的commit

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.GistsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

GistsApi apiInstance = new GistsApi();
String id = "id_example"; // String | 代码片段的ID
try {
    CodeForksHistory result = apiInstance.getGistsIdCommits(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#getGistsIdCommits");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| 代码片段的ID |

### Return type

[**CodeForksHistory**](CodeForksHistory.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getGistsIdForks"></a>
# **getGistsIdForks**
> CodeForks getGistsIdForks(id, page, perPage)

获取 Fork 了指定代码片段的列表

获取 Fork 了指定代码片段的列表

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.GistsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

GistsApi apiInstance = new GistsApi();
String id = "id_example"; // String | 代码片段的ID
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    CodeForks result = apiInstance.getGistsIdForks(id, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#getGistsIdForks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| 代码片段的ID |
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**CodeForks**](CodeForks.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getGistsIdStar"></a>
# **getGistsIdStar**
> getGistsIdStar(id)

判断代码片段是否已Star

判断代码片段是否已Star

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.GistsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

GistsApi apiInstance = new GistsApi();
String id = "id_example"; // String | 代码片段的ID
try {
    apiInstance.getGistsIdStar(id);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#getGistsIdStar");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| 代码片段的ID |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getGistsStarred"></a>
# **getGistsStarred**
> List&lt;Code&gt; getGistsStarred(since, page, perPage)

获取用户Star的代码片段

获取用户Star的代码片段

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.GistsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

GistsApi apiInstance = new GistsApi();
String since = "since_example"; // String | 起始的更新时间，要求时间格式为 ISO 8601
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Code> result = apiInstance.getGistsStarred(since, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#getGistsStarred");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **since** | **String**| 起始的更新时间，要求时间格式为 ISO 8601 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;Code&gt;**](Code.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="patchGistsGistIdCommentsId"></a>
# **patchGistsGistIdCommentsId**
> CodeComment patchGistsGistIdCommentsId(gistId, id, body)

修改代码片段的评论

修改代码片段的评论

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.GistsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

GistsApi apiInstance = new GistsApi();
String gistId = "gistId_example"; // String | 代码片段的ID
Integer id = 56; // Integer | 评论的ID
CommentBody body = new CommentBody(); // CommentBody | 
try {
    CodeComment result = apiInstance.patchGistsGistIdCommentsId(gistId, id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#patchGistsGistIdCommentsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **gistId** | **String**| 代码片段的ID |
 **id** | **Integer**| 评论的ID |
 **body** | [**CommentBody**](CommentBody.md)|  | [optional]

### Return type

[**CodeComment**](CodeComment.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/json

<a name="patchGistsId"></a>
# **patchGistsId**
> CodeForksHistory patchGistsId(id, body)

修改代码片段

修改代码片段

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.GistsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

GistsApi apiInstance = new GistsApi();
String id = "id_example"; // String | 代码片段的ID
GistsIdBody body = new GistsIdBody(); // GistsIdBody | 
try {
    CodeForksHistory result = apiInstance.patchGistsId(id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#patchGistsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| 代码片段的ID |
 **body** | [**GistsIdBody**](GistsIdBody.md)|  | [optional]

### Return type

[**CodeForksHistory**](CodeForksHistory.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/json

<a name="postGists"></a>
# **postGists**
> List&lt;CodeForksHistory&gt; postGists(body)

创建代码片段

创建代码片段

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.GistsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

GistsApi apiInstance = new GistsApi();
GistsBody body = new GistsBody(); // GistsBody | 
try {
    List<CodeForksHistory> result = apiInstance.postGists(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#postGists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**GistsBody**](GistsBody.md)|  | [optional]

### Return type

[**List&lt;CodeForksHistory&gt;**](CodeForksHistory.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postGistsGistIdComments"></a>
# **postGistsGistIdComments**
> CodeComment postGistsGistIdComments(gistId, body)

增加代码片段的评论

增加代码片段的评论

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.GistsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

GistsApi apiInstance = new GistsApi();
String gistId = "gistId_example"; // String | 代码片段的ID
GistIdCommentsBody body = new GistIdCommentsBody(); // GistIdCommentsBody | 
try {
    CodeComment result = apiInstance.postGistsGistIdComments(gistId, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#postGistsGistIdComments");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **gistId** | **String**| 代码片段的ID |
 **body** | [**GistIdCommentsBody**](GistIdCommentsBody.md)|  | [optional]

### Return type

[**CodeComment**](CodeComment.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postGistsIdForks"></a>
# **postGistsIdForks**
> postGistsIdForks(id)

Fork代码片段

Fork代码片段

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.GistsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

GistsApi apiInstance = new GistsApi();
String id = "id_example"; // String | 代码片段的ID
try {
    apiInstance.postGistsIdForks(id);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#postGistsIdForks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| 代码片段的ID |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="putGistsIdStar"></a>
# **putGistsIdStar**
> putGistsIdStar(id)

Star代码片段

Star代码片段

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.GistsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

GistsApi apiInstance = new GistsApi();
String id = "id_example"; // String | 代码片段的ID
try {
    apiInstance.putGistsIdStar(id);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#putGistsIdStar");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| 代码片段的ID |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

