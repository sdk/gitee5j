# GistsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **String** | 代码片段描述，1~30个字符 | 
**files** | **Object** | Hash形式的代码片段文件名以及文件内容。如: { \&quot;file1.txt\&quot;: { \&quot;content\&quot;: \&quot;String file contents\&quot; } } | 
**_public** | **Boolean** | 公开/私有，默认: 私有 |  [optional]
