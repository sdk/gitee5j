# GitAction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**action** | [**ActionEnum**](#ActionEnum) | 文件操作：&#x60;create&#x60;, &#x60;delete&#x60;, &#x60;move&#x60;, &#x60;update&#x60;, &#x60;chmod&#x60; | 
**content** | **String** | 文件内容 |  [optional]
**encoding** | [**EncodingEnum**](#EncodingEnum) | 文件内容编码 &#x60;text&#x60; 或者 &#x60;base64&#x60; |  [optional]
**executeFilemode** | **Boolean** |  |  [optional]
**lastCommitId** | **String** | 文件最近一次提交的 SHA |  [optional]
**path** | **String** | 文件路径 | 
**previousPath** | **String** | 原文件路径，文件重命名 &#x60;move&#x60; 时使用 |  [optional]

<a name="ActionEnum"></a>
## Enum: ActionEnum
Name | Value
---- | -----
CREATE | &quot;create&quot;
UPDATE | &quot;update&quot;
MOVE | &quot;move&quot;
DELETE | &quot;delete&quot;
CHMOD | &quot;chmod&quot;

<a name="EncodingEnum"></a>
## Enum: EncodingEnum
Name | Value
---- | -----
TEXT | &quot;text&quot;
BASE64 | &quot;base64&quot;
