# GitCommit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**author** | [**GitUser**](GitUser.md) |  |  [optional]
**committer** | [**GitUser**](GitUser.md) |  |  [optional]
**message** | **String** |  |  [optional]
