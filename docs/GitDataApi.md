# GitDataApi

All URIs are relative to *https://gitee.com/api/v5*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getReposOwnerRepoGitBlobsSha**](GitDataApi.md#getReposOwnerRepoGitBlobsSha) | **GET** /repos/{owner}/{repo}/git/blobs/{sha} | 获取文件Blob
[**getReposOwnerRepoGitGiteeMetrics**](GitDataApi.md#getReposOwnerRepoGitGiteeMetrics) | **GET** /repos/{owner}/{repo}/git/gitee_metrics | 获取 Gitee 指数
[**getReposOwnerRepoGitTreesSha**](GitDataApi.md#getReposOwnerRepoGitTreesSha) | **GET** /repos/{owner}/{repo}/git/trees/{sha} | 获取目录Tree

<a name="getReposOwnerRepoGitBlobsSha"></a>
# **getReposOwnerRepoGitBlobsSha**
> Blob getReposOwnerRepoGitBlobsSha(owner, repo, sha)

获取文件Blob

获取文件Blob

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.GitDataApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

GitDataApi apiInstance = new GitDataApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String sha = "sha_example"; // String | 文件的 Blob SHA，可通过 [获取仓库具体路径下的内容] API 获取
try {
    Blob result = apiInstance.getReposOwnerRepoGitBlobsSha(owner, repo, sha);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GitDataApi#getReposOwnerRepoGitBlobsSha");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **sha** | **String**| 文件的 Blob SHA，可通过 [获取仓库具体路径下的内容] API 获取 |

### Return type

[**Blob**](Blob.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoGitGiteeMetrics"></a>
# **getReposOwnerRepoGitGiteeMetrics**
> GiteeMetrics getReposOwnerRepoGitGiteeMetrics(owner, repo)

获取 Gitee 指数

获取 Gitee 指数

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.GitDataApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

GitDataApi apiInstance = new GitDataApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
try {
    GiteeMetrics result = apiInstance.getReposOwnerRepoGitGiteeMetrics(owner, repo);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GitDataApi#getReposOwnerRepoGitGiteeMetrics");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |

### Return type

[**GiteeMetrics**](GiteeMetrics.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoGitTreesSha"></a>
# **getReposOwnerRepoGitTreesSha**
> Tree getReposOwnerRepoGitTreesSha(owner, repo, sha, recursive)

获取目录Tree

获取目录Tree

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.GitDataApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

GitDataApi apiInstance = new GitDataApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String sha = "sha_example"; // String | 可以是分支名(如master)、Commit或者目录Tree的SHA值
Integer recursive = 56; // Integer | 赋值为1递归获取目录
try {
    Tree result = apiInstance.getReposOwnerRepoGitTreesSha(owner, repo, sha, recursive);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GitDataApi#getReposOwnerRepoGitTreesSha");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **sha** | **String**| 可以是分支名(如master)、Commit或者目录Tree的SHA值 |
 **recursive** | **Integer**| 赋值为1递归获取目录 | [optional]

### Return type

[**Tree**](Tree.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

