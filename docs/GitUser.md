# GitUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**email** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
