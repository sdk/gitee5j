# GiteeMetricData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**community** | **Integer** |  |  [optional]
**communityPercent** | **Integer** |  |  [optional]
**health** | **Integer** |  |  [optional]
**healthPercent** | **Integer** |  |  [optional]
**influence** | **Integer** |  |  [optional]
**influencePercent** | **Integer** |  |  [optional]
**totalScore** | **Integer** |  |  [optional]
**trend** | **Integer** |  |  [optional]
**trendPercent** | **Integer** |  |  [optional]
**vitality** | **Integer** |  |  [optional]
**vitalityPercent** | **Integer** |  |  [optional]
