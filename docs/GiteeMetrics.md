# GiteeMetrics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**data** | [**GiteeMetricData**](GiteeMetricData.md) |  |  [optional]
**repo** | [**ProjectBasic**](ProjectBasic.md) |  |  [optional]
**totalScore** | **Integer** |  |  [optional]
