# Group

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**avatarUrl** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**eventsUrl** | **String** |  |  [optional]
**followCount** | **Integer** |  |  [optional]
**id** | **Integer** |  |  [optional]
**login** | **String** |  |  [optional]
**membersUrl** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**reposUrl** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
