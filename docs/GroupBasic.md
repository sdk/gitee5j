# GroupBasic

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**avatarUrl** | **String** |  |  [optional]
**id** | **Integer** |  |  [optional]
**login** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
