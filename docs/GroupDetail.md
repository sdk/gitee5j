# GroupDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**avatarUrl** | **String** |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**description** | **String** |  |  [optional]
**email** | **String** |  |  [optional]
**enterprise** | **String** |  |  [optional]
**eventsUrl** | **String** |  |  [optional]
**followCount** | **Integer** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**id** | **Integer** |  |  [optional]
**location** | **String** |  |  [optional]
**login** | **String** |  |  [optional]
**members** | **Integer** |  |  [optional]
**membersUrl** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**owner** | [**UserMini**](UserMini.md) |  |  [optional]
**privateRepos** | **Integer** |  |  [optional]
**_public** | **Boolean** |  |  [optional]
**publicRepos** | **Integer** |  |  [optional]
**reposUrl** | **String** |  |  [optional]
**type** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
