# GroupFollowers

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**followedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**self** | [**UserBasic**](UserBasic.md) |  |  [optional]
