# GroupMember

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **Boolean** |  |  [optional]
**organization** | [**Group**](Group.md) |  |  [optional]
**organizationUrl** | **String** |  |  [optional]
**remark** | **String** |  |  [optional]
**role** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**user** | [**UserMini**](UserMini.md) |  |  [optional]
