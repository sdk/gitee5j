# Hook

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**id** | **Integer** |  |  [optional]
**issuesEvents** | **Boolean** |  |  [optional]
**mergeRequestsEvents** | **Boolean** |  |  [optional]
**noteEvents** | **Boolean** |  |  [optional]
**password** | **String** |  |  [optional]
**projectId** | **Integer** |  |  [optional]
**pushEvents** | **Boolean** |  |  [optional]
**result** | **String** |  |  [optional]
**resultCode** | **Integer** |  |  [optional]
**tagPushEvents** | **Boolean** |  |  [optional]
**url** | **String** |  |  [optional]
