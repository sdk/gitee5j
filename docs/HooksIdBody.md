# HooksIdBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**encryptionType** | [**EncryptionTypeEnum**](#EncryptionTypeEnum) | 加密类型: 0: 密码, 1: 签名密钥 |  [optional]
**issuesEvents** | **Boolean** | 创建/关闭Issue |  [optional]
**mergeRequestsEvents** | **Boolean** | 合并请求和合并后 |  [optional]
**noteEvents** | **Boolean** | 评论了Issue/代码等等 |  [optional]
**password** | **String** | 请求URL时会带上该密码，防止URL被恶意请求 |  [optional]
**pushEvents** | **Boolean** | Push代码到仓库 |  [optional]
**tagPushEvents** | **Boolean** | 提交Tag到仓库 |  [optional]
**url** | **String** | 远程HTTP URL | 

<a name="EncryptionTypeEnum"></a>
## Enum: EncryptionTypeEnum
Name | Value
---- | -----
NUMBER_0 | 0
NUMBER_1 | 1
