# IssueState

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**color** | **String** | 任务状态的颜色 |  [optional]
**command** | **String** | 任务状态的 指令 |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) | 任务状态创建时间 |  [optional]
**icon** | **String** | 任务状态的 Icon |  [optional]
**id** | **Integer** | 任务状态 ID |  [optional]
**serial** | **Integer** | 任务状态的 排序 |  [optional]
**title** | **String** | 任务状态的名称 |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) | 任务状态更新时间 |  [optional]
