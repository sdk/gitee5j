# IssueType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**color** | **String** | 颜色 |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) | 任务类型创建时间 |  [optional]
**id** | **Integer** | 任务类型 ID |  [optional]
**ident** | **String** | 唯一标识符 |  [optional]
**isSystem** | **Boolean** | 是否系统默认类型 |  [optional]
**template** | **String** | 任务类型模板 |  [optional]
**title** | **String** | 任务类型的名称 |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) | 任务类型更新时间 |  [optional]
