# IssuesApi

All URIs are relative to *https://gitee.com/api/v5*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteReposOwnerRepoIssuesCommentsId**](IssuesApi.md#deleteReposOwnerRepoIssuesCommentsId) | **DELETE** /repos/{owner}/{repo}/issues/comments/{id} | 删除Issue某条评论
[**getEnterprisesEnterpriseIssues**](IssuesApi.md#getEnterprisesEnterpriseIssues) | **GET** /enterprises/{enterprise}/issues | 获取某个企业的所有Issues
[**getEnterprisesEnterpriseIssuesNumber**](IssuesApi.md#getEnterprisesEnterpriseIssuesNumber) | **GET** /enterprises/{enterprise}/issues/{number} | 获取企业的某个Issue
[**getEnterprisesEnterpriseIssuesNumberComments**](IssuesApi.md#getEnterprisesEnterpriseIssuesNumberComments) | **GET** /enterprises/{enterprise}/issues/{number}/comments | 获取企业某个Issue所有评论
[**getEnterprisesEnterpriseIssuesNumberLabels**](IssuesApi.md#getEnterprisesEnterpriseIssuesNumberLabels) | **GET** /enterprises/{enterprise}/issues/{number}/labels | 获取企业某个Issue所有标签
[**getEnterprisesEnterpriseIssuesNumberPullRequests**](IssuesApi.md#getEnterprisesEnterpriseIssuesNumberPullRequests) | **GET** /enterprises/{enterprise}/issues/{number}/pull_requests | 获取企业 issue 关联的 Pull Requests
[**getIssues**](IssuesApi.md#getIssues) | **GET** /issues | 获取当前授权用户的所有Issues
[**getOrgsOrgIssues**](IssuesApi.md#getOrgsOrgIssues) | **GET** /orgs/{org}/issues | 获取当前用户某个组织的Issues
[**getReposOwnerIssuesNumberOperateLogs**](IssuesApi.md#getReposOwnerIssuesNumberOperateLogs) | **GET** /repos/{owner}/issues/{number}/operate_logs | 获取某个Issue下的操作日志
[**getReposOwnerIssuesNumberPullRequests**](IssuesApi.md#getReposOwnerIssuesNumberPullRequests) | **GET** /repos/{owner}/issues/{number}/pull_requests | 获取 issue 关联的 Pull Requests
[**getReposOwnerRepoIssues**](IssuesApi.md#getReposOwnerRepoIssues) | **GET** /repos/{owner}/{repo}/issues | 仓库的所有Issues
[**getReposOwnerRepoIssuesComments**](IssuesApi.md#getReposOwnerRepoIssuesComments) | **GET** /repos/{owner}/{repo}/issues/comments | 获取仓库所有Issue的评论
[**getReposOwnerRepoIssuesCommentsId**](IssuesApi.md#getReposOwnerRepoIssuesCommentsId) | **GET** /repos/{owner}/{repo}/issues/comments/{id} | 获取仓库Issue某条评论
[**getReposOwnerRepoIssuesNumber**](IssuesApi.md#getReposOwnerRepoIssuesNumber) | **GET** /repos/{owner}/{repo}/issues/{number} | 仓库的某个Issue
[**getReposOwnerRepoIssuesNumberComments**](IssuesApi.md#getReposOwnerRepoIssuesNumberComments) | **GET** /repos/{owner}/{repo}/issues/{number}/comments | 获取仓库某个Issue所有的评论
[**getUserIssues**](IssuesApi.md#getUserIssues) | **GET** /user/issues | 获取授权用户的所有Issues
[**patchEnterprisesEnterpriseIssuesNumber**](IssuesApi.md#patchEnterprisesEnterpriseIssuesNumber) | **PATCH** /enterprises/{enterprise}/issues/{number} | 更新企业的某个Issue
[**patchReposOwnerIssuesNumber**](IssuesApi.md#patchReposOwnerIssuesNumber) | **PATCH** /repos/{owner}/issues/{number} | 更新Issue
[**patchReposOwnerRepoIssuesCommentsId**](IssuesApi.md#patchReposOwnerRepoIssuesCommentsId) | **PATCH** /repos/{owner}/{repo}/issues/comments/{id} | 更新Issue某条评论
[**postReposOwnerIssues**](IssuesApi.md#postReposOwnerIssues) | **POST** /repos/{owner}/issues | 创建Issue
[**postReposOwnerRepoIssuesNumberComments**](IssuesApi.md#postReposOwnerRepoIssuesNumberComments) | **POST** /repos/{owner}/{repo}/issues/{number}/comments | 创建某个Issue评论

<a name="deleteReposOwnerRepoIssuesCommentsId"></a>
# **deleteReposOwnerRepoIssuesCommentsId**
> deleteReposOwnerRepoIssuesCommentsId(owner, repo, id)

删除Issue某条评论

删除Issue某条评论

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.IssuesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

IssuesApi apiInstance = new IssuesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 评论的ID
try {
    apiInstance.deleteReposOwnerRepoIssuesCommentsId(owner, repo, id);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#deleteReposOwnerRepoIssuesCommentsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| 评论的ID |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getEnterprisesEnterpriseIssues"></a>
# **getEnterprisesEnterpriseIssues**
> List&lt;Issue&gt; getEnterprisesEnterpriseIssues(enterprise, state, labels, sort, direction, since, page, perPage, schedule, deadline, createdAt, finishedAt, milestone, assignee, creator, program)

获取某个企业的所有Issues

获取某个企业的所有Issues

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.IssuesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

IssuesApi apiInstance = new IssuesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String state = "open"; // String | Issue的状态: open（开启的）, progressing(进行中), closed（关闭的）, rejected（拒绝的）。 默认: open
String labels = "labels_example"; // String | 用逗号分开的标签。如: bug,performance
String sort = "created"; // String | 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at
String direction = "desc"; // String | 排序方式: 升序(asc)，降序(desc)。默认: desc
String since = "since_example"; // String | 起始的更新时间，要求时间格式为 ISO 8601
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
String schedule = "schedule_example"; // String | 计划开始日期，格式：20181006T173008+80-20181007T173008+80（区间），或者 -20181007T173008+80（小于20181007T173008+80），或者 20181006T173008+80-（大于20181006T173008+80），要求时间格式为20181006T173008+80
String deadline = "deadline_example"; // String | 计划截止日期，格式同上
String createdAt = "createdAt_example"; // String | 任务创建时间，格式同上
String finishedAt = "finishedAt_example"; // String | 任务完成时间，即任务最后一次转为已完成状态的时间点。格式同上
String milestone = "milestone_example"; // String | 根据里程碑标题。none为没里程碑的，*为所有带里程碑的
String assignee = "assignee_example"; // String | 用户的username。 none为没指派者, *为所有带有指派者的
String creator = "creator_example"; // String | 创建Issues的用户username
String program = "program_example"; // String | 所属项目名称。none为没所属有项目的，*为所有带所属项目的
try {
    List<Issue> result = apiInstance.getEnterprisesEnterpriseIssues(enterprise, state, labels, sort, direction, since, page, perPage, schedule, deadline, createdAt, finishedAt, milestone, assignee, creator, program);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#getEnterprisesEnterpriseIssues");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **state** | **String**| Issue的状态: open（开启的）, progressing(进行中), closed（关闭的）, rejected（拒绝的）。 默认: open | [optional] [default to open] [enum: open, progressing, closed, rejected, all]
 **labels** | **String**| 用逗号分开的标签。如: bug,performance | [optional]
 **sort** | **String**| 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at | [optional] [default to created] [enum: created, updated]
 **direction** | **String**| 排序方式: 升序(asc)，降序(desc)。默认: desc | [optional] [default to desc] [enum: asc, desc]
 **since** | **String**| 起始的更新时间，要求时间格式为 ISO 8601 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]
 **schedule** | **String**| 计划开始日期，格式：20181006T173008+80-20181007T173008+80（区间），或者 -20181007T173008+80（小于20181007T173008+80），或者 20181006T173008+80-（大于20181006T173008+80），要求时间格式为20181006T173008+80 | [optional]
 **deadline** | **String**| 计划截止日期，格式同上 | [optional]
 **createdAt** | **String**| 任务创建时间，格式同上 | [optional]
 **finishedAt** | **String**| 任务完成时间，即任务最后一次转为已完成状态的时间点。格式同上 | [optional]
 **milestone** | **String**| 根据里程碑标题。none为没里程碑的，*为所有带里程碑的 | [optional]
 **assignee** | **String**| 用户的username。 none为没指派者, *为所有带有指派者的 | [optional]
 **creator** | **String**| 创建Issues的用户username | [optional]
 **program** | **String**| 所属项目名称。none为没所属有项目的，*为所有带所属项目的 | [optional]

### Return type

[**List&lt;Issue&gt;**](Issue.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getEnterprisesEnterpriseIssuesNumber"></a>
# **getEnterprisesEnterpriseIssuesNumber**
> Issue getEnterprisesEnterpriseIssuesNumber(enterprise, number)

获取企业的某个Issue

获取企业的某个Issue

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.IssuesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

IssuesApi apiInstance = new IssuesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
try {
    Issue result = apiInstance.getEnterprisesEnterpriseIssuesNumber(enterprise, number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#getEnterprisesEnterpriseIssuesNumber");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |

### Return type

[**Issue**](Issue.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getEnterprisesEnterpriseIssuesNumberComments"></a>
# **getEnterprisesEnterpriseIssuesNumberComments**
> List&lt;Note&gt; getEnterprisesEnterpriseIssuesNumberComments(enterprise, number, page, perPage)

获取企业某个Issue所有评论

获取企业某个Issue所有评论

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.IssuesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

IssuesApi apiInstance = new IssuesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Note> result = apiInstance.getEnterprisesEnterpriseIssuesNumberComments(enterprise, number, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#getEnterprisesEnterpriseIssuesNumberComments");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;Note&gt;**](Note.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getEnterprisesEnterpriseIssuesNumberLabels"></a>
# **getEnterprisesEnterpriseIssuesNumberLabels**
> List&lt;Label&gt; getEnterprisesEnterpriseIssuesNumberLabels(enterprise, number, page, perPage)

获取企业某个Issue所有标签

获取企业某个Issue所有标签

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.IssuesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

IssuesApi apiInstance = new IssuesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Label> result = apiInstance.getEnterprisesEnterpriseIssuesNumberLabels(enterprise, number, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#getEnterprisesEnterpriseIssuesNumberLabels");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;Label&gt;**](Label.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getEnterprisesEnterpriseIssuesNumberPullRequests"></a>
# **getEnterprisesEnterpriseIssuesNumberPullRequests**
> List&lt;PullRequest&gt; getEnterprisesEnterpriseIssuesNumberPullRequests(enterprise, number)

获取企业 issue 关联的 Pull Requests

获取企业 issue 关联的 Pull Requests

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.IssuesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

IssuesApi apiInstance = new IssuesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
try {
    List<PullRequest> result = apiInstance.getEnterprisesEnterpriseIssuesNumberPullRequests(enterprise, number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#getEnterprisesEnterpriseIssuesNumberPullRequests");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |

### Return type

[**List&lt;PullRequest&gt;**](PullRequest.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getIssues"></a>
# **getIssues**
> List&lt;Issue&gt; getIssues(filter, state, labels, sort, direction, since, page, perPage, schedule, deadline, createdAt, finishedAt)

获取当前授权用户的所有Issues

获取当前授权用户的所有Issues

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.IssuesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

IssuesApi apiInstance = new IssuesApi();
String filter = "assigned"; // String | 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned
String state = "open"; // String | Issue的状态: open（开启的）, progressing(进行中), closed（关闭的）, rejected（拒绝的）。 默认: open
String labels = "labels_example"; // String | 用逗号分开的标签。如: bug,performance
String sort = "created"; // String | 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at
String direction = "desc"; // String | 排序方式: 升序(asc)，降序(desc)。默认: desc
String since = "since_example"; // String | 起始的更新时间，要求时间格式为 ISO 8601
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
String schedule = "schedule_example"; // String | 计划开始日期，格式：20181006T173008+80-20181007T173008+80（区间），或者 -20181007T173008+80（小于20181007T173008+80），或者 20181006T173008+80-（大于20181006T173008+80），要求时间格式为20181006T173008+80
String deadline = "deadline_example"; // String | 计划截止日期，格式同上
String createdAt = "createdAt_example"; // String | 任务创建时间，格式同上
String finishedAt = "finishedAt_example"; // String | 任务完成时间，即任务最后一次转为已完成状态的时间点。格式同上
try {
    List<Issue> result = apiInstance.getIssues(filter, state, labels, sort, direction, since, page, perPage, schedule, deadline, createdAt, finishedAt);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#getIssues");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned | [optional] [default to assigned] [enum: assigned, created, all]
 **state** | **String**| Issue的状态: open（开启的）, progressing(进行中), closed（关闭的）, rejected（拒绝的）。 默认: open | [optional] [default to open] [enum: open, progressing, closed, rejected, all]
 **labels** | **String**| 用逗号分开的标签。如: bug,performance | [optional]
 **sort** | **String**| 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at | [optional] [default to created] [enum: created, updated]
 **direction** | **String**| 排序方式: 升序(asc)，降序(desc)。默认: desc | [optional] [default to desc] [enum: asc, desc]
 **since** | **String**| 起始的更新时间，要求时间格式为 ISO 8601 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]
 **schedule** | **String**| 计划开始日期，格式：20181006T173008+80-20181007T173008+80（区间），或者 -20181007T173008+80（小于20181007T173008+80），或者 20181006T173008+80-（大于20181006T173008+80），要求时间格式为20181006T173008+80 | [optional]
 **deadline** | **String**| 计划截止日期，格式同上 | [optional]
 **createdAt** | **String**| 任务创建时间，格式同上 | [optional]
 **finishedAt** | **String**| 任务完成时间，即任务最后一次转为已完成状态的时间点。格式同上 | [optional]

### Return type

[**List&lt;Issue&gt;**](Issue.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getOrgsOrgIssues"></a>
# **getOrgsOrgIssues**
> List&lt;Issue&gt; getOrgsOrgIssues(org, filter, state, labels, sort, direction, since, page, perPage, schedule, deadline, createdAt, finishedAt)

获取当前用户某个组织的Issues

获取当前用户某个组织的Issues

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.IssuesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

IssuesApi apiInstance = new IssuesApi();
String org = "org_example"; // String | 组织的路径(path/login)
String filter = "assigned"; // String | 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned
String state = "open"; // String | Issue的状态: open（开启的）, progressing(进行中), closed（关闭的）, rejected（拒绝的）。 默认: open
String labels = "labels_example"; // String | 用逗号分开的标签。如: bug,performance
String sort = "created"; // String | 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at
String direction = "desc"; // String | 排序方式: 升序(asc)，降序(desc)。默认: desc
String since = "since_example"; // String | 起始的更新时间，要求时间格式为 ISO 8601
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
String schedule = "schedule_example"; // String | 计划开始日期，格式：20181006T173008+80-20181007T173008+80（区间），或者 -20181007T173008+80（小于20181007T173008+80），或者 20181006T173008+80-（大于20181006T173008+80），要求时间格式为20181006T173008+80
String deadline = "deadline_example"; // String | 计划截止日期，格式同上
String createdAt = "createdAt_example"; // String | 任务创建时间，格式同上
String finishedAt = "finishedAt_example"; // String | 任务完成时间，即任务最后一次转为已完成状态的时间点。格式同上
try {
    List<Issue> result = apiInstance.getOrgsOrgIssues(org, filter, state, labels, sort, direction, since, page, perPage, schedule, deadline, createdAt, finishedAt);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#getOrgsOrgIssues");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |
 **filter** | **String**| 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned | [optional] [default to assigned] [enum: assigned, created, all]
 **state** | **String**| Issue的状态: open（开启的）, progressing(进行中), closed（关闭的）, rejected（拒绝的）。 默认: open | [optional] [default to open] [enum: open, progressing, closed, rejected, all]
 **labels** | **String**| 用逗号分开的标签。如: bug,performance | [optional]
 **sort** | **String**| 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at | [optional] [default to created] [enum: created, updated]
 **direction** | **String**| 排序方式: 升序(asc)，降序(desc)。默认: desc | [optional] [default to desc] [enum: asc, desc]
 **since** | **String**| 起始的更新时间，要求时间格式为 ISO 8601 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]
 **schedule** | **String**| 计划开始日期，格式：20181006T173008+80-20181007T173008+80（区间），或者 -20181007T173008+80（小于20181007T173008+80），或者 20181006T173008+80-（大于20181006T173008+80），要求时间格式为20181006T173008+80 | [optional]
 **deadline** | **String**| 计划截止日期，格式同上 | [optional]
 **createdAt** | **String**| 任务创建时间，格式同上 | [optional]
 **finishedAt** | **String**| 任务完成时间，即任务最后一次转为已完成状态的时间点。格式同上 | [optional]

### Return type

[**List&lt;Issue&gt;**](Issue.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerIssuesNumberOperateLogs"></a>
# **getReposOwnerIssuesNumberOperateLogs**
> List&lt;OperateLog&gt; getReposOwnerIssuesNumberOperateLogs(owner, number, repo, sort)

获取某个Issue下的操作日志

获取某个Issue下的操作日志

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.IssuesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

IssuesApi apiInstance = new IssuesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
String repo = "repo_example"; // String | 仓库路径(path)
String sort = "desc"; // String | 按递增(asc)或递减(desc)排序，默认：递减
try {
    List<OperateLog> result = apiInstance.getReposOwnerIssuesNumberOperateLogs(owner, number, repo, sort);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#getReposOwnerIssuesNumberOperateLogs");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |
 **repo** | **String**| 仓库路径(path) | [optional]
 **sort** | **String**| 按递增(asc)或递减(desc)排序，默认：递减 | [optional] [default to desc] [enum: desc, asc]

### Return type

[**List&lt;OperateLog&gt;**](OperateLog.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerIssuesNumberPullRequests"></a>
# **getReposOwnerIssuesNumberPullRequests**
> List&lt;PullRequest&gt; getReposOwnerIssuesNumberPullRequests(owner, number, repo)

获取 issue 关联的 Pull Requests

获取 issue 关联的 Pull Requests

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.IssuesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

IssuesApi apiInstance = new IssuesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
String repo = "repo_example"; // String | 仓库路径(path)
try {
    List<PullRequest> result = apiInstance.getReposOwnerIssuesNumberPullRequests(owner, number, repo);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#getReposOwnerIssuesNumberPullRequests");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |
 **repo** | **String**| 仓库路径(path) | [optional]

### Return type

[**List&lt;PullRequest&gt;**](PullRequest.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoIssues"></a>
# **getReposOwnerRepoIssues**
> List&lt;Issue&gt; getReposOwnerRepoIssues(owner, repo, state, labels, sort, direction, since, page, perPage, schedule, deadline, createdAt, finishedAt, milestone, assignee, creator, program)

仓库的所有Issues

仓库的所有Issues

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.IssuesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

IssuesApi apiInstance = new IssuesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String state = "open"; // String | Issue的状态: open（开启的）, progressing(进行中), closed（关闭的）, rejected（拒绝的）。 默认: open
String labels = "labels_example"; // String | 用逗号分开的标签。如: bug,performance
String sort = "created"; // String | 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at
String direction = "desc"; // String | 排序方式: 升序(asc)，降序(desc)。默认: desc
String since = "since_example"; // String | 起始的更新时间，要求时间格式为 ISO 8601
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
String schedule = "schedule_example"; // String | 计划开始日期，格式：20181006T173008+80-20181007T173008+80（区间），或者 -20181007T173008+80（小于20181007T173008+80），或者 20181006T173008+80-（大于20181006T173008+80），要求时间格式为20181006T173008+80
String deadline = "deadline_example"; // String | 计划截止日期，格式同上
String createdAt = "createdAt_example"; // String | 任务创建时间，格式同上
String finishedAt = "finishedAt_example"; // String | 任务完成时间，即任务最后一次转为已完成状态的时间点。格式同上
String milestone = "milestone_example"; // String | 根据里程碑标题。none为没里程碑的，*为所有带里程碑的
String assignee = "assignee_example"; // String | 用户的username。 none为没指派者, *为所有带有指派者的
String creator = "creator_example"; // String | 创建Issues的用户username
String program = "program_example"; // String | 所属项目名称。none为没有所属项目，*为所有带所属项目的
try {
    List<Issue> result = apiInstance.getReposOwnerRepoIssues(owner, repo, state, labels, sort, direction, since, page, perPage, schedule, deadline, createdAt, finishedAt, milestone, assignee, creator, program);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#getReposOwnerRepoIssues");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **state** | **String**| Issue的状态: open（开启的）, progressing(进行中), closed（关闭的）, rejected（拒绝的）。 默认: open | [optional] [default to open] [enum: open, progressing, closed, rejected, all]
 **labels** | **String**| 用逗号分开的标签。如: bug,performance | [optional]
 **sort** | **String**| 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at | [optional] [default to created] [enum: created, updated]
 **direction** | **String**| 排序方式: 升序(asc)，降序(desc)。默认: desc | [optional] [default to desc] [enum: asc, desc]
 **since** | **String**| 起始的更新时间，要求时间格式为 ISO 8601 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]
 **schedule** | **String**| 计划开始日期，格式：20181006T173008+80-20181007T173008+80（区间），或者 -20181007T173008+80（小于20181007T173008+80），或者 20181006T173008+80-（大于20181006T173008+80），要求时间格式为20181006T173008+80 | [optional]
 **deadline** | **String**| 计划截止日期，格式同上 | [optional]
 **createdAt** | **String**| 任务创建时间，格式同上 | [optional]
 **finishedAt** | **String**| 任务完成时间，即任务最后一次转为已完成状态的时间点。格式同上 | [optional]
 **milestone** | **String**| 根据里程碑标题。none为没里程碑的，*为所有带里程碑的 | [optional]
 **assignee** | **String**| 用户的username。 none为没指派者, *为所有带有指派者的 | [optional]
 **creator** | **String**| 创建Issues的用户username | [optional]
 **program** | **String**| 所属项目名称。none为没有所属项目，*为所有带所属项目的 | [optional]

### Return type

[**List&lt;Issue&gt;**](Issue.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoIssuesComments"></a>
# **getReposOwnerRepoIssuesComments**
> List&lt;Note&gt; getReposOwnerRepoIssuesComments(owner, repo, sort, direction, since, page, perPage)

获取仓库所有Issue的评论

获取仓库所有Issue的评论

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.IssuesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

IssuesApi apiInstance = new IssuesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String sort = "created"; // String | Either created or updated. Default: created
String direction = "asc"; // String | Either asc or desc. Ignored without the sort parameter.
String since = "since_example"; // String | Only comments updated at or after this time are returned.                                               This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Note> result = apiInstance.getReposOwnerRepoIssuesComments(owner, repo, sort, direction, since, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#getReposOwnerRepoIssuesComments");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **sort** | **String**| Either created or updated. Default: created | [optional] [default to created] [enum: created, updated]
 **direction** | **String**| Either asc or desc. Ignored without the sort parameter. | [optional] [default to asc] [enum: asc, desc]
 **since** | **String**| Only comments updated at or after this time are returned.                                               This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;Note&gt;**](Note.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoIssuesCommentsId"></a>
# **getReposOwnerRepoIssuesCommentsId**
> Note getReposOwnerRepoIssuesCommentsId(owner, repo, id)

获取仓库Issue某条评论

获取仓库Issue某条评论

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.IssuesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

IssuesApi apiInstance = new IssuesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 评论的ID
try {
    Note result = apiInstance.getReposOwnerRepoIssuesCommentsId(owner, repo, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#getReposOwnerRepoIssuesCommentsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| 评论的ID |

### Return type

[**Note**](Note.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoIssuesNumber"></a>
# **getReposOwnerRepoIssuesNumber**
> Issue getReposOwnerRepoIssuesNumber(owner, repo, number)

仓库的某个Issue

仓库的某个Issue

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.IssuesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

IssuesApi apiInstance = new IssuesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
try {
    Issue result = apiInstance.getReposOwnerRepoIssuesNumber(owner, repo, number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#getReposOwnerRepoIssuesNumber");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |

### Return type

[**Issue**](Issue.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoIssuesNumberComments"></a>
# **getReposOwnerRepoIssuesNumberComments**
> List&lt;Note&gt; getReposOwnerRepoIssuesNumberComments(owner, repo, number, since, page, perPage, order)

获取仓库某个Issue所有的评论

获取仓库某个Issue所有的评论

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.IssuesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

IssuesApi apiInstance = new IssuesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
String since = "since_example"; // String | Only comments updated at or after this time are returned.                                               This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
String order = "asc"; // String | 排序顺序: asc(default),desc
try {
    List<Note> result = apiInstance.getReposOwnerRepoIssuesNumberComments(owner, repo, number, since, page, perPage, order);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#getReposOwnerRepoIssuesNumberComments");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |
 **since** | **String**| Only comments updated at or after this time are returned.                                               This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]
 **order** | **String**| 排序顺序: asc(default),desc | [optional] [default to asc]

### Return type

[**List&lt;Note&gt;**](Note.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUserIssues"></a>
# **getUserIssues**
> List&lt;Issue&gt; getUserIssues(filter, state, labels, sort, direction, since, page, perPage, schedule, deadline, createdAt, finishedAt)

获取授权用户的所有Issues

获取授权用户的所有Issues

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.IssuesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

IssuesApi apiInstance = new IssuesApi();
String filter = "assigned"; // String | 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned
String state = "open"; // String | Issue的状态: open（开启的）, progressing(进行中), closed（关闭的）, rejected（拒绝的）。 默认: open
String labels = "labels_example"; // String | 用逗号分开的标签。如: bug,performance
String sort = "created"; // String | 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at
String direction = "desc"; // String | 排序方式: 升序(asc)，降序(desc)。默认: desc
String since = "since_example"; // String | 起始的更新时间，要求时间格式为 ISO 8601
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
String schedule = "schedule_example"; // String | 计划开始日期，格式：20181006T173008+80-20181007T173008+80（区间），或者 -20181007T173008+80（小于20181007T173008+80），或者 20181006T173008+80-（大于20181006T173008+80），要求时间格式为20181006T173008+80
String deadline = "deadline_example"; // String | 计划截止日期，格式同上
String createdAt = "createdAt_example"; // String | 任务创建时间，格式同上
String finishedAt = "finishedAt_example"; // String | 任务完成时间，即任务最后一次转为已完成状态的时间点。格式同上
try {
    List<Issue> result = apiInstance.getUserIssues(filter, state, labels, sort, direction, since, page, perPage, schedule, deadline, createdAt, finishedAt);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#getUserIssues");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned | [optional] [default to assigned] [enum: assigned, created, all]
 **state** | **String**| Issue的状态: open（开启的）, progressing(进行中), closed（关闭的）, rejected（拒绝的）。 默认: open | [optional] [default to open] [enum: open, progressing, closed, rejected, all]
 **labels** | **String**| 用逗号分开的标签。如: bug,performance | [optional]
 **sort** | **String**| 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at | [optional] [default to created] [enum: created, updated]
 **direction** | **String**| 排序方式: 升序(asc)，降序(desc)。默认: desc | [optional] [default to desc] [enum: asc, desc]
 **since** | **String**| 起始的更新时间，要求时间格式为 ISO 8601 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]
 **schedule** | **String**| 计划开始日期，格式：20181006T173008+80-20181007T173008+80（区间），或者 -20181007T173008+80（小于20181007T173008+80），或者 20181006T173008+80-（大于20181006T173008+80），要求时间格式为20181006T173008+80 | [optional]
 **deadline** | **String**| 计划截止日期，格式同上 | [optional]
 **createdAt** | **String**| 任务创建时间，格式同上 | [optional]
 **finishedAt** | **String**| 任务完成时间，即任务最后一次转为已完成状态的时间点。格式同上 | [optional]

### Return type

[**List&lt;Issue&gt;**](Issue.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="patchEnterprisesEnterpriseIssuesNumber"></a>
# **patchEnterprisesEnterpriseIssuesNumber**
> Issue patchEnterprisesEnterpriseIssuesNumber(enterprise, number, body)

更新企业的某个Issue

更新企业的某个Issue

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.IssuesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

IssuesApi apiInstance = new IssuesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
IssuesNumberBody body = new IssuesNumberBody(); // IssuesNumberBody | 
try {
    Issue result = apiInstance.patchEnterprisesEnterpriseIssuesNumber(enterprise, number, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#patchEnterprisesEnterpriseIssuesNumber");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |
 **body** | [**IssuesNumberBody**](IssuesNumberBody.md)|  | [optional]

### Return type

[**Issue**](Issue.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/json

<a name="patchReposOwnerIssuesNumber"></a>
# **patchReposOwnerIssuesNumber**
> Issue patchReposOwnerIssuesNumber(owner, number, body)

更新Issue

更新Issue

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.IssuesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

IssuesApi apiInstance = new IssuesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
IssuesNumberBody1 body = new IssuesNumberBody1(); // IssuesNumberBody1 | 
try {
    Issue result = apiInstance.patchReposOwnerIssuesNumber(owner, number, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#patchReposOwnerIssuesNumber");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |
 **body** | [**IssuesNumberBody1**](IssuesNumberBody1.md)|  | [optional]

### Return type

[**Issue**](Issue.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/json

<a name="patchReposOwnerRepoIssuesCommentsId"></a>
# **patchReposOwnerRepoIssuesCommentsId**
> Note patchReposOwnerRepoIssuesCommentsId(owner, repo, id, body)

更新Issue某条评论

更新Issue某条评论

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.IssuesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

IssuesApi apiInstance = new IssuesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 评论的ID
CommentBody body = new CommentBody(); // CommentBody | 
try {
    Note result = apiInstance.patchReposOwnerRepoIssuesCommentsId(owner, repo, id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#patchReposOwnerRepoIssuesCommentsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| 评论的ID |
 **body** | [**CommentBody**](CommentBody.md)|  | [optional]

### Return type

[**Note**](Note.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/json

<a name="postReposOwnerIssues"></a>
# **postReposOwnerIssues**
> Issue postReposOwnerIssues(owner, body)

创建Issue

创建Issue

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.IssuesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

IssuesApi apiInstance = new IssuesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
OwnerIssuesBody body = new OwnerIssuesBody(); // OwnerIssuesBody | 
try {
    Issue result = apiInstance.postReposOwnerIssues(owner, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#postReposOwnerIssues");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **body** | [**OwnerIssuesBody**](OwnerIssuesBody.md)|  | [optional]

### Return type

[**Issue**](Issue.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postReposOwnerRepoIssuesNumberComments"></a>
# **postReposOwnerRepoIssuesNumberComments**
> Note postReposOwnerRepoIssuesNumberComments(owner, repo, number, body)

创建某个Issue评论

创建某个Issue评论

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.IssuesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

IssuesApi apiInstance = new IssuesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
NumberCommentsBody body = new NumberCommentsBody(); // NumberCommentsBody | 
try {
    Note result = apiInstance.postReposOwnerRepoIssuesNumberComments(owner, repo, number, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#postReposOwnerRepoIssuesNumberComments");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |
 **body** | [**NumberCommentsBody**](NumberCommentsBody.md)|  | [optional]

### Return type

[**Note**](Note.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

