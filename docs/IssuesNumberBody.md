# IssuesNumberBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assignee** | **String** | Issue负责人的个人空间地址 |  [optional]
**body** | **String** | Issue描述 |  [optional]
**branch** | **String** | 分支名称，传空串表示取消关联分支 |  [optional]
**collaborators** | **String** | Issue协助者的个人空间地址, 以 , 分隔 |  [optional]
**labels** | **String** | 用逗号分开的标签，名称要求长度在 2-20 之间且非特殊字符。如: bug,performance |  [optional]
**milestone** | **Integer** | 里程碑序号 |  [optional]
**program** | **String** | 项目ID |  [optional]
**securityHole** | **Boolean** | 是否是私有issue(默认为false) |  [optional]
**state** | [**StateEnum**](#StateEnum) | Issue 状态，open（开启的）、progressing（进行中）、closed（关闭的） |  [optional]
**title** | **String** | Issue标题 |  [optional]

<a name="StateEnum"></a>
## Enum: StateEnum
Name | Value
---- | -----
OPEN | &quot;open&quot;
PROGRESSING | &quot;progressing&quot;
CLOSED | &quot;closed&quot;
