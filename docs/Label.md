# Label

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**color** | **String** |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**id** | **Integer** |  |  [optional]
**name** | **String** |  |  [optional]
**repositoryId** | **Integer** |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**url** | **String** |  |  [optional]
