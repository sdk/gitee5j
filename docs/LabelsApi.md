# LabelsApi

All URIs are relative to *https://gitee.com/api/v5*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteReposOwnerRepoIssuesNumberLabels**](LabelsApi.md#deleteReposOwnerRepoIssuesNumberLabels) | **DELETE** /repos/{owner}/{repo}/issues/{number}/labels | 删除Issue所有标签
[**deleteReposOwnerRepoIssuesNumberLabelsName**](LabelsApi.md#deleteReposOwnerRepoIssuesNumberLabelsName) | **DELETE** /repos/{owner}/{repo}/issues/{number}/labels/{name} | 删除Issue标签
[**deleteReposOwnerRepoLabelsName**](LabelsApi.md#deleteReposOwnerRepoLabelsName) | **DELETE** /repos/{owner}/{repo}/labels/{name} | 删除一个仓库任务标签
[**deleteReposOwnerRepoProjectLabels**](LabelsApi.md#deleteReposOwnerRepoProjectLabels) | **DELETE** /repos/{owner}/{repo}/project_labels | 删除仓库标签
[**getEnterprisesEnterpriseLabels**](LabelsApi.md#getEnterprisesEnterpriseLabels) | **GET** /enterprises/{enterprise}/labels | 获取企业所有标签
[**getEnterprisesEnterpriseLabelsName**](LabelsApi.md#getEnterprisesEnterpriseLabelsName) | **GET** /enterprises/{enterprise}/labels/{name} | 获取企业某个标签
[**getReposOwnerRepoIssuesNumberLabels**](LabelsApi.md#getReposOwnerRepoIssuesNumberLabels) | **GET** /repos/{owner}/{repo}/issues/{number}/labels | 获取仓库任务的所有标签
[**getReposOwnerRepoLabels**](LabelsApi.md#getReposOwnerRepoLabels) | **GET** /repos/{owner}/{repo}/labels | 获取仓库所有任务标签
[**getReposOwnerRepoLabelsName**](LabelsApi.md#getReposOwnerRepoLabelsName) | **GET** /repos/{owner}/{repo}/labels/{name} | 根据标签名称获取单个标签
[**getReposOwnerRepoProjectLabels**](LabelsApi.md#getReposOwnerRepoProjectLabels) | **GET** /repos/{owner}/{repo}/project_labels | 获取仓库所有标签
[**patchReposOwnerRepoLabelsOriginalName**](LabelsApi.md#patchReposOwnerRepoLabelsOriginalName) | **PATCH** /repos/{owner}/{repo}/labels/{original_name} | 更新一个仓库任务标签
[**postReposOwnerRepoIssuesNumberLabels**](LabelsApi.md#postReposOwnerRepoIssuesNumberLabels) | **POST** /repos/{owner}/{repo}/issues/{number}/labels | 创建Issue标签
[**postReposOwnerRepoLabels**](LabelsApi.md#postReposOwnerRepoLabels) | **POST** /repos/{owner}/{repo}/labels | 创建仓库任务标签
[**postReposOwnerRepoProjectLabels**](LabelsApi.md#postReposOwnerRepoProjectLabels) | **POST** /repos/{owner}/{repo}/project_labels | 添加仓库标签
[**putReposOwnerRepoIssuesNumberLabels**](LabelsApi.md#putReposOwnerRepoIssuesNumberLabels) | **PUT** /repos/{owner}/{repo}/issues/{number}/labels | 替换Issue所有标签
[**putReposOwnerRepoProjectLabels**](LabelsApi.md#putReposOwnerRepoProjectLabels) | **PUT** /repos/{owner}/{repo}/project_labels | 替换所有仓库标签

<a name="deleteReposOwnerRepoIssuesNumberLabels"></a>
# **deleteReposOwnerRepoIssuesNumberLabels**
> deleteReposOwnerRepoIssuesNumberLabels(owner, repo, number)

删除Issue所有标签

删除Issue所有标签

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.LabelsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

LabelsApi apiInstance = new LabelsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
try {
    apiInstance.deleteReposOwnerRepoIssuesNumberLabels(owner, repo, number);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelsApi#deleteReposOwnerRepoIssuesNumberLabels");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteReposOwnerRepoIssuesNumberLabelsName"></a>
# **deleteReposOwnerRepoIssuesNumberLabelsName**
> deleteReposOwnerRepoIssuesNumberLabelsName(owner, repo, number, name)

删除Issue标签

删除Issue标签

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.LabelsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

LabelsApi apiInstance = new LabelsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
String name = "name_example"; // String | 标签名称(批量删除用英文逗号分隔，如: bug,feature)
try {
    apiInstance.deleteReposOwnerRepoIssuesNumberLabelsName(owner, repo, number, name);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelsApi#deleteReposOwnerRepoIssuesNumberLabelsName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |
 **name** | **String**| 标签名称(批量删除用英文逗号分隔，如: bug,feature) |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteReposOwnerRepoLabelsName"></a>
# **deleteReposOwnerRepoLabelsName**
> deleteReposOwnerRepoLabelsName(owner, repo, name)

删除一个仓库任务标签

删除一个仓库任务标签

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.LabelsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

LabelsApi apiInstance = new LabelsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String name = "name_example"; // String | 标签名称
try {
    apiInstance.deleteReposOwnerRepoLabelsName(owner, repo, name);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelsApi#deleteReposOwnerRepoLabelsName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **name** | **String**| 标签名称 |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteReposOwnerRepoProjectLabels"></a>
# **deleteReposOwnerRepoProjectLabels**
> deleteReposOwnerRepoProjectLabels(owner, repo, body)

删除仓库标签

删除仓库标签

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.LabelsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

LabelsApi apiInstance = new LabelsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
List<String> body = Arrays.asList("body_example"); // List<String> | 标签名数组，如: ["feat", "bug"]
try {
    apiInstance.deleteReposOwnerRepoProjectLabels(owner, repo, body);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelsApi#deleteReposOwnerRepoProjectLabels");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **body** | [**List&lt;String&gt;**](String.md)| 标签名数组，如: [&quot;feat&quot;, &quot;bug&quot;] | [optional]

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: Not defined

<a name="getEnterprisesEnterpriseLabels"></a>
# **getEnterprisesEnterpriseLabels**
> List&lt;Label&gt; getEnterprisesEnterpriseLabels(enterprise)

获取企业所有标签

获取企业所有标签

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.LabelsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

LabelsApi apiInstance = new LabelsApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
try {
    List<Label> result = apiInstance.getEnterprisesEnterpriseLabels(enterprise);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelsApi#getEnterprisesEnterpriseLabels");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |

### Return type

[**List&lt;Label&gt;**](Label.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getEnterprisesEnterpriseLabelsName"></a>
# **getEnterprisesEnterpriseLabelsName**
> Label getEnterprisesEnterpriseLabelsName(enterprise, name)

获取企业某个标签

获取企业某个标签

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.LabelsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

LabelsApi apiInstance = new LabelsApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String name = "name_example"; // String | 标签名称
try {
    Label result = apiInstance.getEnterprisesEnterpriseLabelsName(enterprise, name);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelsApi#getEnterprisesEnterpriseLabelsName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **name** | **String**| 标签名称 |

### Return type

[**Label**](Label.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoIssuesNumberLabels"></a>
# **getReposOwnerRepoIssuesNumberLabels**
> List&lt;Label&gt; getReposOwnerRepoIssuesNumberLabels(owner, repo, number)

获取仓库任务的所有标签

获取仓库任务的所有标签

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.LabelsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

LabelsApi apiInstance = new LabelsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
try {
    List<Label> result = apiInstance.getReposOwnerRepoIssuesNumberLabels(owner, repo, number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelsApi#getReposOwnerRepoIssuesNumberLabels");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |

### Return type

[**List&lt;Label&gt;**](Label.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoLabels"></a>
# **getReposOwnerRepoLabels**
> List&lt;Label&gt; getReposOwnerRepoLabels(owner, repo)

获取仓库所有任务标签

获取仓库所有任务标签

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.LabelsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

LabelsApi apiInstance = new LabelsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
try {
    List<Label> result = apiInstance.getReposOwnerRepoLabels(owner, repo);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelsApi#getReposOwnerRepoLabels");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |

### Return type

[**List&lt;Label&gt;**](Label.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoLabelsName"></a>
# **getReposOwnerRepoLabelsName**
> Label getReposOwnerRepoLabelsName(owner, repo, name)

根据标签名称获取单个标签

根据标签名称获取单个标签

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.LabelsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

LabelsApi apiInstance = new LabelsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String name = "name_example"; // String | 标签名称
try {
    Label result = apiInstance.getReposOwnerRepoLabelsName(owner, repo, name);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelsApi#getReposOwnerRepoLabelsName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **name** | **String**| 标签名称 |

### Return type

[**Label**](Label.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoProjectLabels"></a>
# **getReposOwnerRepoProjectLabels**
> List&lt;ProjectLabel&gt; getReposOwnerRepoProjectLabels(owner, repo)

获取仓库所有标签

获取仓库所有标签

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.LabelsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

LabelsApi apiInstance = new LabelsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
try {
    List<ProjectLabel> result = apiInstance.getReposOwnerRepoProjectLabels(owner, repo);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelsApi#getReposOwnerRepoProjectLabels");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |

### Return type

[**List&lt;ProjectLabel&gt;**](ProjectLabel.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="patchReposOwnerRepoLabelsOriginalName"></a>
# **patchReposOwnerRepoLabelsOriginalName**
> Label patchReposOwnerRepoLabelsOriginalName(owner, repo, originalName, body)

更新一个仓库任务标签

更新一个仓库任务标签

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.LabelsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

LabelsApi apiInstance = new LabelsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String originalName = "originalName_example"; // String | 标签原有名称
LabelsOriginalNameBody body = new LabelsOriginalNameBody(); // LabelsOriginalNameBody | 
try {
    Label result = apiInstance.patchReposOwnerRepoLabelsOriginalName(owner, repo, originalName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelsApi#patchReposOwnerRepoLabelsOriginalName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **originalName** | **String**| 标签原有名称 |
 **body** | [**LabelsOriginalNameBody**](LabelsOriginalNameBody.md)|  | [optional]

### Return type

[**Label**](Label.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/json

<a name="postReposOwnerRepoIssuesNumberLabels"></a>
# **postReposOwnerRepoIssuesNumberLabels**
> List&lt;Label&gt; postReposOwnerRepoIssuesNumberLabels(owner, repo, number, body)

创建Issue标签

创建Issue标签  需要在请求的body里填上数组，元素为标签的名字。如: [\&quot;performance\&quot;, \&quot;bug\&quot;]

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.LabelsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

LabelsApi apiInstance = new LabelsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
List<String> body = Arrays.asList("body_example"); // List<String> | 标签名数组，如: ["feat", "bug"]
try {
    List<Label> result = apiInstance.postReposOwnerRepoIssuesNumberLabels(owner, repo, number, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelsApi#postReposOwnerRepoIssuesNumberLabels");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |
 **body** | [**List&lt;String&gt;**](String.md)| 标签名数组，如: [&quot;feat&quot;, &quot;bug&quot;] | [optional]

### Return type

[**List&lt;Label&gt;**](Label.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postReposOwnerRepoLabels"></a>
# **postReposOwnerRepoLabels**
> Label postReposOwnerRepoLabels(owner, repo, body)

创建仓库任务标签

创建仓库任务标签

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.LabelsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

LabelsApi apiInstance = new LabelsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
RepoLabelsBody body = new RepoLabelsBody(); // RepoLabelsBody | 
try {
    Label result = apiInstance.postReposOwnerRepoLabels(owner, repo, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelsApi#postReposOwnerRepoLabels");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **body** | [**RepoLabelsBody**](RepoLabelsBody.md)|  | [optional]

### Return type

[**Label**](Label.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postReposOwnerRepoProjectLabels"></a>
# **postReposOwnerRepoProjectLabels**
> List&lt;ProjectLabel&gt; postReposOwnerRepoProjectLabels(owner, repo, body)

添加仓库标签

添加仓库标签

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.LabelsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

LabelsApi apiInstance = new LabelsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
List<String> body = Arrays.asList("body_example"); // List<String> | 标签名数组，如: ["feat", "bug"]
try {
    List<ProjectLabel> result = apiInstance.postReposOwnerRepoProjectLabels(owner, repo, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelsApi#postReposOwnerRepoProjectLabels");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **body** | [**List&lt;String&gt;**](String.md)| 标签名数组，如: [&quot;feat&quot;, &quot;bug&quot;] | [optional]

### Return type

[**List&lt;ProjectLabel&gt;**](ProjectLabel.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putReposOwnerRepoIssuesNumberLabels"></a>
# **putReposOwnerRepoIssuesNumberLabels**
> List&lt;Label&gt; putReposOwnerRepoIssuesNumberLabels(owner, repo, number, body)

替换Issue所有标签

替换Issue所有标签  需要在请求的body里填上数组，元素为标签的名字。如: [\&quot;performance\&quot;, \&quot;bug\&quot;]

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.LabelsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

LabelsApi apiInstance = new LabelsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
List<String> body = Arrays.asList("body_example"); // List<String> | 标签名数组，如: ["feat", "bug"]
try {
    List<Label> result = apiInstance.putReposOwnerRepoIssuesNumberLabels(owner, repo, number, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelsApi#putReposOwnerRepoIssuesNumberLabels");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |
 **body** | [**List&lt;String&gt;**](String.md)| 标签名数组，如: [&quot;feat&quot;, &quot;bug&quot;] | [optional]

### Return type

[**List&lt;Label&gt;**](Label.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putReposOwnerRepoProjectLabels"></a>
# **putReposOwnerRepoProjectLabels**
> List&lt;ProjectLabel&gt; putReposOwnerRepoProjectLabels(owner, repo, body)

替换所有仓库标签

替换所有仓库标签

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.LabelsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

LabelsApi apiInstance = new LabelsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
List<String> body = Arrays.asList("body_example"); // List<String> | 标签名数组，如: ["feat", "bug"]
try {
    List<ProjectLabel> result = apiInstance.putReposOwnerRepoProjectLabels(owner, repo, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelsApi#putReposOwnerRepoProjectLabels");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **body** | [**List&lt;String&gt;**](String.md)| 标签名数组，如: [&quot;feat&quot;, &quot;bug&quot;] | [optional]

### Return type

[**List&lt;ProjectLabel&gt;**](ProjectLabel.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

