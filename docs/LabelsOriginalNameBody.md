# LabelsOriginalNameBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**color** | **String** | 标签新颜色 |  [optional]
**name** | **String** | 标签新名称 |  [optional]
