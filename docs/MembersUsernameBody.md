# MembersUsernameBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **Boolean** | 是否可访问企业资源，默认:是。（若选否则禁止该用户访问企业资源） |  [optional]
**name** | **String** | 企业成员真实姓名（备注） |  [optional]
**role** | [**RoleEnum**](#RoleEnum) | 企业角色：member &#x3D;&gt; 普通成员, outsourced &#x3D;&gt; 外包成员, admin &#x3D;&gt; 管理员 |  [optional]

<a name="RoleEnum"></a>
## Enum: RoleEnum
Name | Value
---- | -----
ADMIN | &quot;admin&quot;
MEMBER | &quot;member&quot;
OUTSOURCED | &quot;outsourced&quot;
