# MembershipsUsernameBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**role** | [**RoleEnum**](#RoleEnum) | 设置用户在组织的角色 |  [optional]

<a name="RoleEnum"></a>
## Enum: RoleEnum
Name | Value
---- | -----
ADMIN | &quot;admin&quot;
MEMBER | &quot;member&quot;
