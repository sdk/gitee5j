# Milestone

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**closedIssues** | **Integer** |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**description** | **String** |  |  [optional]
**dueOn** | [**LocalDate**](LocalDate.md) |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**number** | **Integer** |  |  [optional]
**openIssues** | **Integer** |  |  [optional]
**repositoryId** | **Integer** |  |  [optional]
**state** | **String** |  |  [optional]
**title** | **String** |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**url** | **String** |  |  [optional]
