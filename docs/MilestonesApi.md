# MilestonesApi

All URIs are relative to *https://gitee.com/api/v5*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteReposOwnerRepoMilestonesNumber**](MilestonesApi.md#deleteReposOwnerRepoMilestonesNumber) | **DELETE** /repos/{owner}/{repo}/milestones/{number} | 删除仓库单个里程碑
[**getReposOwnerRepoMilestones**](MilestonesApi.md#getReposOwnerRepoMilestones) | **GET** /repos/{owner}/{repo}/milestones | 获取仓库所有里程碑
[**getReposOwnerRepoMilestonesNumber**](MilestonesApi.md#getReposOwnerRepoMilestonesNumber) | **GET** /repos/{owner}/{repo}/milestones/{number} | 获取仓库单个里程碑
[**patchReposOwnerRepoMilestonesNumber**](MilestonesApi.md#patchReposOwnerRepoMilestonesNumber) | **PATCH** /repos/{owner}/{repo}/milestones/{number} | 更新仓库里程碑
[**postReposOwnerRepoMilestones**](MilestonesApi.md#postReposOwnerRepoMilestones) | **POST** /repos/{owner}/{repo}/milestones | 创建仓库里程碑

<a name="deleteReposOwnerRepoMilestonesNumber"></a>
# **deleteReposOwnerRepoMilestonesNumber**
> deleteReposOwnerRepoMilestonesNumber(owner, repo, number)

删除仓库单个里程碑

删除仓库单个里程碑

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.MilestonesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

MilestonesApi apiInstance = new MilestonesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 里程碑序号(id)
try {
    apiInstance.deleteReposOwnerRepoMilestonesNumber(owner, repo, number);
} catch (ApiException e) {
    System.err.println("Exception when calling MilestonesApi#deleteReposOwnerRepoMilestonesNumber");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 里程碑序号(id) |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getReposOwnerRepoMilestones"></a>
# **getReposOwnerRepoMilestones**
> List&lt;Milestone&gt; getReposOwnerRepoMilestones(owner, repo, state, sort, direction, page, perPage)

获取仓库所有里程碑

获取仓库所有里程碑

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.MilestonesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

MilestonesApi apiInstance = new MilestonesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String state = "open"; // String | 里程碑状态: open, closed, all。默认: open
String sort = "due_on"; // String | 排序方式: due_on
String direction = "direction_example"; // String | 升序(asc)或是降序(desc)。默认: asc
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Milestone> result = apiInstance.getReposOwnerRepoMilestones(owner, repo, state, sort, direction, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MilestonesApi#getReposOwnerRepoMilestones");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **state** | **String**| 里程碑状态: open, closed, all。默认: open | [optional] [default to open] [enum: open, closed, all]
 **sort** | **String**| 排序方式: due_on | [optional] [default to due_on] [enum: due_on]
 **direction** | **String**| 升序(asc)或是降序(desc)。默认: asc | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;Milestone&gt;**](Milestone.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoMilestonesNumber"></a>
# **getReposOwnerRepoMilestonesNumber**
> Milestone getReposOwnerRepoMilestonesNumber(owner, repo, number)

获取仓库单个里程碑

获取仓库单个里程碑

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.MilestonesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

MilestonesApi apiInstance = new MilestonesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 里程碑序号(id)
try {
    Milestone result = apiInstance.getReposOwnerRepoMilestonesNumber(owner, repo, number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MilestonesApi#getReposOwnerRepoMilestonesNumber");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 里程碑序号(id) |

### Return type

[**Milestone**](Milestone.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="patchReposOwnerRepoMilestonesNumber"></a>
# **patchReposOwnerRepoMilestonesNumber**
> Milestone patchReposOwnerRepoMilestonesNumber(owner, repo, number, body)

更新仓库里程碑

更新仓库里程碑

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.MilestonesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

MilestonesApi apiInstance = new MilestonesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 里程碑序号(id)
MilestonesNumberBody body = new MilestonesNumberBody(); // MilestonesNumberBody | 
try {
    Milestone result = apiInstance.patchReposOwnerRepoMilestonesNumber(owner, repo, number, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MilestonesApi#patchReposOwnerRepoMilestonesNumber");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 里程碑序号(id) |
 **body** | [**MilestonesNumberBody**](MilestonesNumberBody.md)|  | [optional]

### Return type

[**Milestone**](Milestone.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/json

<a name="postReposOwnerRepoMilestones"></a>
# **postReposOwnerRepoMilestones**
> Milestone postReposOwnerRepoMilestones(owner, repo, body)

创建仓库里程碑

创建仓库里程碑

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.MilestonesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

MilestonesApi apiInstance = new MilestonesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
RepoMilestonesBody body = new RepoMilestonesBody(); // RepoMilestonesBody | 
try {
    Milestone result = apiInstance.postReposOwnerRepoMilestones(owner, repo, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MilestonesApi#postReposOwnerRepoMilestones");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **body** | [**RepoMilestonesBody**](RepoMilestonesBody.md)|  | [optional]

### Return type

[**Milestone**](Milestone.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

