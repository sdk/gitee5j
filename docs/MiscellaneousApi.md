# MiscellaneousApi

All URIs are relative to *https://gitee.com/api/v5*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getEmojis**](MiscellaneousApi.md#getEmojis) | **GET** /emojis | 列出可使用的 Emoji
[**getGitignoreTemplates**](MiscellaneousApi.md#getGitignoreTemplates) | **GET** /gitignore/templates | 列出可使用的 .gitignore 模板
[**getGitignoreTemplatesName**](MiscellaneousApi.md#getGitignoreTemplatesName) | **GET** /gitignore/templates/{name} | 获取一个 .gitignore 模板
[**getGitignoreTemplatesNameRaw**](MiscellaneousApi.md#getGitignoreTemplatesNameRaw) | **GET** /gitignore/templates/{name}/raw | 获取一个 .gitignore 模板原始文件
[**getLicenses**](MiscellaneousApi.md#getLicenses) | **GET** /licenses | 列出可使用的开源许可协议
[**getLicensesLicense**](MiscellaneousApi.md#getLicensesLicense) | **GET** /licenses/{license} | 获取一个开源许可协议
[**getLicensesLicenseRaw**](MiscellaneousApi.md#getLicensesLicenseRaw) | **GET** /licenses/{license}/raw | 获取一个开源许可协议原始文件
[**getReposOwnerRepoLicense**](MiscellaneousApi.md#getReposOwnerRepoLicense) | **GET** /repos/{owner}/{repo}/license | 获取一个仓库使用的开源许可协议
[**postMarkdown**](MiscellaneousApi.md#postMarkdown) | **POST** /markdown | 渲染 Markdown 文本

<a name="getEmojis"></a>
# **getEmojis**
> getEmojis()

列出可使用的 Emoji

列出可使用的 Emoji

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.MiscellaneousApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

MiscellaneousApi apiInstance = new MiscellaneousApi();
try {
    apiInstance.getEmojis();
} catch (ApiException e) {
    System.err.println("Exception when calling MiscellaneousApi#getEmojis");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getGitignoreTemplates"></a>
# **getGitignoreTemplates**
> getGitignoreTemplates()

列出可使用的 .gitignore 模板

列出可使用的 .gitignore 模板

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.MiscellaneousApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

MiscellaneousApi apiInstance = new MiscellaneousApi();
try {
    apiInstance.getGitignoreTemplates();
} catch (ApiException e) {
    System.err.println("Exception when calling MiscellaneousApi#getGitignoreTemplates");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getGitignoreTemplatesName"></a>
# **getGitignoreTemplatesName**
> getGitignoreTemplatesName(name)

获取一个 .gitignore 模板

获取一个 .gitignore 模板

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.MiscellaneousApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

MiscellaneousApi apiInstance = new MiscellaneousApi();
String name = "name_example"; // String | .gitignore 模板名
try {
    apiInstance.getGitignoreTemplatesName(name);
} catch (ApiException e) {
    System.err.println("Exception when calling MiscellaneousApi#getGitignoreTemplatesName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| .gitignore 模板名 | [enum: Actionscript, Ada, Agda, Android, Anjuta, Ansible, AppEngine, AppceleratorTitanium, ArchLinuxPackages, Archives, Autotools, Backup, Bazaar, BricxCC, C, C++, CFWheels, CMake, CUDA, CVS, CakePHP, Calabash, ChefCookbook, Clojure, Cloud9, CodeIgniter, CodeKit, CommonLisp, Composer, Concrete5, Coq, CraftCMS, D, DM, Dart, DartEditor, Delphi, Diff, Dreamweaver, Dropbox, Drupal, EPiServer, Eagle, Eclipse, EiffelStudio, Elisp, Elixir, Elm, Emacs, Ensime, Erlang, Espresso, ExpressionEngine, ExtJs, Fancy, Finale, FlexBuilder, Flutter, ForceDotCom, Fortran, FuelPHP, GPG, GWT, Gcov, GitBook, Go, Godot, Gradle, Grails, Haskell, IGORPro, Idris, Images, JBoss, JDeveloper, JENKINS_HOME, JEnv, Java, Jekyll, JetBrains, Joomla, Julia, KDevelop4, Kate, KiCad, Kohana, Kotlin, LabVIEW, Laravel, Lazarus, Leiningen, LemonStand, LibreOffice, Lilypond, Linux, Lithium, Lua, LyX, MATLAB, Magento, Maven, Mercurial, Mercury, MetaProgrammingSystem, Metals, MicrosoftOffice, MiniProgram, ModelSim, Momentics, MonoDevelop, Nanoc, NetBeans, Nim, Ninja, Node, NotepadPP, OCaml, Objective-C, Octave, Opa, OpenCart, OracleForms, Otto, PSoCCreator, Packer, Patch, Perl, Phalcon, PlayFramework, Plone, Prestashop, Processing, PuTTY, PureScript, Python, Qooxdoo, Qt, R, ROS, Rails, Raku, Redcar, Redis, RhodesRhomobile, Ruby, Rust, SBT, SCons, SVN, Sass, Scala, Scheme, Scrivener, Sdcc, SeamGen, SketchUp, SlickEdit, Smalltalk, Stata, Stella, SublimeText, SugarCRM, Swift, Symfony, SymphonyCMS, SynopsysVCS, Tags, TeX, Terraform, TextMate, Textpattern, TortoiseGit, TurboGears2, Typo3, Umbraco, Unity, UnrealEngine, VVVV, Vagrant, Vim, VirtualEnv, Virtuoso, VisualStudio, VisualStudioCode, Waf, WebMethods, Windows, WordPress, Xcode, XilinxISE, Xojo, Yeoman, Yii, ZendFramework, Zephir, macOS]

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getGitignoreTemplatesNameRaw"></a>
# **getGitignoreTemplatesNameRaw**
> getGitignoreTemplatesNameRaw(name)

获取一个 .gitignore 模板原始文件

获取一个 .gitignore 模板原始文件

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.MiscellaneousApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

MiscellaneousApi apiInstance = new MiscellaneousApi();
String name = "name_example"; // String | .gitignore 模板名
try {
    apiInstance.getGitignoreTemplatesNameRaw(name);
} catch (ApiException e) {
    System.err.println("Exception when calling MiscellaneousApi#getGitignoreTemplatesNameRaw");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| .gitignore 模板名 | [enum: Actionscript, Ada, Agda, Android, Anjuta, Ansible, AppEngine, AppceleratorTitanium, ArchLinuxPackages, Archives, Autotools, Backup, Bazaar, BricxCC, C, C++, CFWheels, CMake, CUDA, CVS, CakePHP, Calabash, ChefCookbook, Clojure, Cloud9, CodeIgniter, CodeKit, CommonLisp, Composer, Concrete5, Coq, CraftCMS, D, DM, Dart, DartEditor, Delphi, Diff, Dreamweaver, Dropbox, Drupal, EPiServer, Eagle, Eclipse, EiffelStudio, Elisp, Elixir, Elm, Emacs, Ensime, Erlang, Espresso, ExpressionEngine, ExtJs, Fancy, Finale, FlexBuilder, Flutter, ForceDotCom, Fortran, FuelPHP, GPG, GWT, Gcov, GitBook, Go, Godot, Gradle, Grails, Haskell, IGORPro, Idris, Images, JBoss, JDeveloper, JENKINS_HOME, JEnv, Java, Jekyll, JetBrains, Joomla, Julia, KDevelop4, Kate, KiCad, Kohana, Kotlin, LabVIEW, Laravel, Lazarus, Leiningen, LemonStand, LibreOffice, Lilypond, Linux, Lithium, Lua, LyX, MATLAB, Magento, Maven, Mercurial, Mercury, MetaProgrammingSystem, Metals, MicrosoftOffice, MiniProgram, ModelSim, Momentics, MonoDevelop, Nanoc, NetBeans, Nim, Ninja, Node, NotepadPP, OCaml, Objective-C, Octave, Opa, OpenCart, OracleForms, Otto, PSoCCreator, Packer, Patch, Perl, Phalcon, PlayFramework, Plone, Prestashop, Processing, PuTTY, PureScript, Python, Qooxdoo, Qt, R, ROS, Rails, Raku, Redcar, Redis, RhodesRhomobile, Ruby, Rust, SBT, SCons, SVN, Sass, Scala, Scheme, Scrivener, Sdcc, SeamGen, SketchUp, SlickEdit, Smalltalk, Stata, Stella, SublimeText, SugarCRM, Swift, Symfony, SymphonyCMS, SynopsysVCS, Tags, TeX, Terraform, TextMate, Textpattern, TortoiseGit, TurboGears2, Typo3, Umbraco, Unity, UnrealEngine, VVVV, Vagrant, Vim, VirtualEnv, Virtuoso, VisualStudio, VisualStudioCode, Waf, WebMethods, Windows, WordPress, Xcode, XilinxISE, Xojo, Yeoman, Yii, ZendFramework, Zephir, macOS]

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getLicenses"></a>
# **getLicenses**
> getLicenses()

列出可使用的开源许可协议

列出可使用的开源许可协议

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.MiscellaneousApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

MiscellaneousApi apiInstance = new MiscellaneousApi();
try {
    apiInstance.getLicenses();
} catch (ApiException e) {
    System.err.println("Exception when calling MiscellaneousApi#getLicenses");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getLicensesLicense"></a>
# **getLicensesLicense**
> getLicensesLicense(license)

获取一个开源许可协议

获取一个开源许可协议

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.MiscellaneousApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

MiscellaneousApi apiInstance = new MiscellaneousApi();
String license = "license_example"; // String | 协议名称
try {
    apiInstance.getLicensesLicense(license);
} catch (ApiException e) {
    System.err.println("Exception when calling MiscellaneousApi#getLicensesLicense");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **license** | **String**| 协议名称 | [enum: MulanPSL-2.0, 0BSD, AFL-3.0, AGPL-3.0, Apache-2.0, Artistic-2.0, BSD-2-Clause, BSD-3-Clause, BSD-3-Clause-Clear, BSL-1.0, CC-BY-4.0, CC-BY-SA-4.0, CC0-1.0, ECL-2.0, EPL-1.0, EPL-2.0, EUPL-1.1, EUPL-1.2, GPL-2.0, GPL-3.0, ISC, LGPL-2.1, LGPL-3.0, LPPL-1.3c, MIT, MPL-2.0, MS-PL, MS-RL, MulanPSL-1.0, MulanPubL-1.0, MulanPubL-2.0, NCSA, OFL-1.1, OSL-3.0, PostgreSQL, UPL-1.0, Unlicense, WTFPL, Zlib]

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getLicensesLicenseRaw"></a>
# **getLicensesLicenseRaw**
> getLicensesLicenseRaw(license)

获取一个开源许可协议原始文件

获取一个开源许可协议原始文件

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.MiscellaneousApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

MiscellaneousApi apiInstance = new MiscellaneousApi();
String license = "license_example"; // String | 协议名称
try {
    apiInstance.getLicensesLicenseRaw(license);
} catch (ApiException e) {
    System.err.println("Exception when calling MiscellaneousApi#getLicensesLicenseRaw");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **license** | **String**| 协议名称 | [enum: MulanPSL-2.0, 0BSD, AFL-3.0, AGPL-3.0, Apache-2.0, Artistic-2.0, BSD-2-Clause, BSD-3-Clause, BSD-3-Clause-Clear, BSL-1.0, CC-BY-4.0, CC-BY-SA-4.0, CC0-1.0, ECL-2.0, EPL-1.0, EPL-2.0, EUPL-1.1, EUPL-1.2, GPL-2.0, GPL-3.0, ISC, LGPL-2.1, LGPL-3.0, LPPL-1.3c, MIT, MPL-2.0, MS-PL, MS-RL, MulanPSL-1.0, MulanPubL-1.0, MulanPubL-2.0, NCSA, OFL-1.1, OSL-3.0, PostgreSQL, UPL-1.0, Unlicense, WTFPL, Zlib]

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getReposOwnerRepoLicense"></a>
# **getReposOwnerRepoLicense**
> getReposOwnerRepoLicense(owner, repo)

获取一个仓库使用的开源许可协议

获取一个仓库使用的开源许可协议

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.MiscellaneousApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

MiscellaneousApi apiInstance = new MiscellaneousApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
try {
    apiInstance.getReposOwnerRepoLicense(owner, repo);
} catch (ApiException e) {
    System.err.println("Exception when calling MiscellaneousApi#getReposOwnerRepoLicense");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="postMarkdown"></a>
# **postMarkdown**
> postMarkdown(body)

渲染 Markdown 文本

渲染 Markdown 文本

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.MiscellaneousApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

MiscellaneousApi apiInstance = new MiscellaneousApi();
MarkdownBody body = new MarkdownBody(); // MarkdownBody | 
try {
    apiInstance.postMarkdown(body);
} catch (ApiException e) {
    System.err.println("Exception when calling MiscellaneousApi#postMarkdown");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**MarkdownBody**](MarkdownBody.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

