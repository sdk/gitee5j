# Namespace

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**htmlUrl** | **String** | namespace 地址 |  [optional]
**id** | **Integer** | namespace id |  [optional]
**name** | **String** | namespace 名称 |  [optional]
**parent** | [**NamespaceMini**](NamespaceMini.md) |  |  [optional]
**path** | **String** | namespace 路径 |  [optional]
**type** | **String** | namespace 类型，企业：Enterprise，组织：Group，用户：null |  [optional]
