# Note

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**body** | **String** |  |  [optional]
**bodyHtml** | **String** |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**id** | **Integer** |  |  [optional]
**inReplyToId** | **Integer** |  |  [optional]
**inReplyToUser** | [**UserBasic**](UserBasic.md) |  |  [optional]
**source** | **String** |  |  [optional]
**target** | **Object** |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**user** | [**UserBasic**](UserBasic.md) |  |  [optional]
