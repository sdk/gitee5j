# NotificationsMessagesBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ids** | **String** | 指定一组私信 ID，以 , 分隔 |  [optional]
