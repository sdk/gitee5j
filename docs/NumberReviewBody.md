# NumberReviewBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**force** | **Boolean** | 是否强制审查通过（默认否），只对管理员生效 |  [optional]
