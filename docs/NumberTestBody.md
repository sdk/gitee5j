# NumberTestBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**force** | **Boolean** | 是否强制测试通过（默认否），只对管理员生效 |  [optional]
