# NumberTestersBody1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**resetAll** | **Boolean** | 是否重置所有测试人，默认：false，只对管理员生效 |  [optional]
