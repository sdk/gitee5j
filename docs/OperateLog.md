# OperateLog

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**actionType** | **String** |  |  [optional]
**content** | **String** |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**icon** | **String** |  |  [optional]
**id** | **Integer** |  |  [optional]
**linkTarget** | **Object** |  |  [optional]
**target** | **Object** |  |  [optional]
**user** | [**UserBasic**](UserBasic.md) |  |  [optional]
