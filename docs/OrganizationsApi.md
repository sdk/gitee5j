# OrganizationsApi

All URIs are relative to *https://gitee.com/api/v5*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteOrgsOrgMembershipsUsername**](OrganizationsApi.md#deleteOrgsOrgMembershipsUsername) | **DELETE** /orgs/{org}/memberships/{username} | 移除授权用户所管理组织中的成员
[**deleteUserMembershipsOrgsOrg**](OrganizationsApi.md#deleteUserMembershipsOrgsOrg) | **DELETE** /user/memberships/orgs/{org} | 退出一个组织
[**getOrgsOrg**](OrganizationsApi.md#getOrgsOrg) | **GET** /orgs/{org} | 获取一个组织
[**getOrgsOrgFollowers**](OrganizationsApi.md#getOrgsOrgFollowers) | **GET** /orgs/{org}/followers | 列出指定组织的所有关注者
[**getOrgsOrgMembers**](OrganizationsApi.md#getOrgsOrgMembers) | **GET** /orgs/{org}/members | 列出一个组织的所有成员
[**getOrgsOrgMembershipsUsername**](OrganizationsApi.md#getOrgsOrgMembershipsUsername) | **GET** /orgs/{org}/memberships/{username} | 获取授权用户所属组织的一个成员
[**getUserMembershipsOrgs**](OrganizationsApi.md#getUserMembershipsOrgs) | **GET** /user/memberships/orgs | 列出授权用户在所属组织的成员资料
[**getUserMembershipsOrgsOrg**](OrganizationsApi.md#getUserMembershipsOrgsOrg) | **GET** /user/memberships/orgs/{org} | 获取授权用户在一个组织的成员资料
[**getUserOrgs**](OrganizationsApi.md#getUserOrgs) | **GET** /user/orgs | 列出授权用户所属的组织
[**getUsersUsernameOrgs**](OrganizationsApi.md#getUsersUsernameOrgs) | **GET** /users/{username}/orgs | 列出用户所属的组织
[**patchOrgsOrg**](OrganizationsApi.md#patchOrgsOrg) | **PATCH** /orgs/{org} | 更新授权用户所管理的组织资料
[**patchUserMembershipsOrgsOrg**](OrganizationsApi.md#patchUserMembershipsOrgsOrg) | **PATCH** /user/memberships/orgs/{org} | 更新授权用户在一个组织的成员资料
[**postUsersOrganization**](OrganizationsApi.md#postUsersOrganization) | **POST** /users/organization | 创建组织
[**putOrgsOrgMembershipsUsername**](OrganizationsApi.md#putOrgsOrgMembershipsUsername) | **PUT** /orgs/{org}/memberships/{username} | 增加或更新授权用户所管理组织的成员

<a name="deleteOrgsOrgMembershipsUsername"></a>
# **deleteOrgsOrgMembershipsUsername**
> deleteOrgsOrgMembershipsUsername(org, username)

移除授权用户所管理组织中的成员

移除授权用户所管理组织中的成员

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.OrganizationsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationsApi apiInstance = new OrganizationsApi();
String org = "org_example"; // String | 组织的路径(path/login)
String username = "username_example"; // String | 用户名(username/login)
try {
    apiInstance.deleteOrgsOrgMembershipsUsername(org, username);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationsApi#deleteOrgsOrgMembershipsUsername");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |
 **username** | **String**| 用户名(username/login) |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteUserMembershipsOrgsOrg"></a>
# **deleteUserMembershipsOrgsOrg**
> deleteUserMembershipsOrgsOrg(org)

退出一个组织

退出一个组织

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.OrganizationsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationsApi apiInstance = new OrganizationsApi();
String org = "org_example"; // String | 组织的路径(path/login)
try {
    apiInstance.deleteUserMembershipsOrgsOrg(org);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationsApi#deleteUserMembershipsOrgsOrg");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getOrgsOrg"></a>
# **getOrgsOrg**
> GroupDetail getOrgsOrg(org)

获取一个组织

获取一个组织

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.OrganizationsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationsApi apiInstance = new OrganizationsApi();
String org = "org_example"; // String | 组织的路径(path/login)
try {
    GroupDetail result = apiInstance.getOrgsOrg(org);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationsApi#getOrgsOrg");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |

### Return type

[**GroupDetail**](GroupDetail.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getOrgsOrgFollowers"></a>
# **getOrgsOrgFollowers**
> List&lt;GroupFollowers&gt; getOrgsOrgFollowers(org, page, perPage)

列出指定组织的所有关注者

列出指定组织的所有关注者

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.OrganizationsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationsApi apiInstance = new OrganizationsApi();
String org = "org_example"; // String | 组织的路径(path/login)
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<GroupFollowers> result = apiInstance.getOrgsOrgFollowers(org, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationsApi#getOrgsOrgFollowers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;GroupFollowers&gt;**](GroupFollowers.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getOrgsOrgMembers"></a>
# **getOrgsOrgMembers**
> List&lt;UserBasic&gt; getOrgsOrgMembers(org, page, perPage, role)

列出一个组织的所有成员

列出一个组织的所有成员

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.OrganizationsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationsApi apiInstance = new OrganizationsApi();
String org = "org_example"; // String | 组织的路径(path/login)
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
String role = "all"; // String | 根据角色筛选成员
try {
    List<UserBasic> result = apiInstance.getOrgsOrgMembers(org, page, perPage, role);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationsApi#getOrgsOrgMembers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]
 **role** | **String**| 根据角色筛选成员 | [optional] [default to all] [enum: all, admin, member]

### Return type

[**List&lt;UserBasic&gt;**](UserBasic.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getOrgsOrgMembershipsUsername"></a>
# **getOrgsOrgMembershipsUsername**
> GroupMember getOrgsOrgMembershipsUsername(org, username)

获取授权用户所属组织的一个成员

获取授权用户所属组织的一个成员

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.OrganizationsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationsApi apiInstance = new OrganizationsApi();
String org = "org_example"; // String | 组织的路径(path/login)
String username = "username_example"; // String | 用户名(username/login)
try {
    GroupMember result = apiInstance.getOrgsOrgMembershipsUsername(org, username);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationsApi#getOrgsOrgMembershipsUsername");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |
 **username** | **String**| 用户名(username/login) |

### Return type

[**GroupMember**](GroupMember.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUserMembershipsOrgs"></a>
# **getUserMembershipsOrgs**
> List&lt;GroupMember&gt; getUserMembershipsOrgs(active, page, perPage)

列出授权用户在所属组织的成员资料

列出授权用户在所属组织的成员资料

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.OrganizationsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationsApi apiInstance = new OrganizationsApi();
Boolean active = true; // Boolean | 根据成员是否已激活进行筛选资料，缺省返回所有资料
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<GroupMember> result = apiInstance.getUserMembershipsOrgs(active, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationsApi#getUserMembershipsOrgs");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **active** | **Boolean**| 根据成员是否已激活进行筛选资料，缺省返回所有资料 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;GroupMember&gt;**](GroupMember.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUserMembershipsOrgsOrg"></a>
# **getUserMembershipsOrgsOrg**
> GroupMember getUserMembershipsOrgsOrg(org)

获取授权用户在一个组织的成员资料

获取授权用户在一个组织的成员资料

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.OrganizationsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationsApi apiInstance = new OrganizationsApi();
String org = "org_example"; // String | 组织的路径(path/login)
try {
    GroupMember result = apiInstance.getUserMembershipsOrgsOrg(org);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationsApi#getUserMembershipsOrgsOrg");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |

### Return type

[**GroupMember**](GroupMember.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUserOrgs"></a>
# **getUserOrgs**
> List&lt;Group&gt; getUserOrgs(page, perPage, admin)

列出授权用户所属的组织

列出授权用户所属的组织

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.OrganizationsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationsApi apiInstance = new OrganizationsApi();
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
Boolean admin = true; // Boolean | 只列出授权用户管理的组织
try {
    List<Group> result = apiInstance.getUserOrgs(page, perPage, admin);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationsApi#getUserOrgs");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]
 **admin** | **Boolean**| 只列出授权用户管理的组织 | [optional]

### Return type

[**List&lt;Group&gt;**](Group.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUsersUsernameOrgs"></a>
# **getUsersUsernameOrgs**
> List&lt;Group&gt; getUsersUsernameOrgs(username, page, perPage)

列出用户所属的组织

列出用户所属的组织

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.OrganizationsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationsApi apiInstance = new OrganizationsApi();
String username = "username_example"; // String | 用户名(username/login)
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Group> result = apiInstance.getUsersUsernameOrgs(username, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationsApi#getUsersUsernameOrgs");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;Group&gt;**](Group.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="patchOrgsOrg"></a>
# **patchOrgsOrg**
> GroupDetail patchOrgsOrg(org, body)

更新授权用户所管理的组织资料

更新授权用户所管理的组织资料

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.OrganizationsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationsApi apiInstance = new OrganizationsApi();
String org = "org_example"; // String | 组织的路径(path/login)
OrgsOrgBody body = new OrgsOrgBody(); // OrgsOrgBody | 
try {
    GroupDetail result = apiInstance.patchOrgsOrg(org, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationsApi#patchOrgsOrg");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |
 **body** | [**OrgsOrgBody**](OrgsOrgBody.md)|  | [optional]

### Return type

[**GroupDetail**](GroupDetail.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/json

<a name="patchUserMembershipsOrgsOrg"></a>
# **patchUserMembershipsOrgsOrg**
> GroupMember patchUserMembershipsOrgsOrg(org, body)

更新授权用户在一个组织的成员资料

更新授权用户在一个组织的成员资料

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.OrganizationsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationsApi apiInstance = new OrganizationsApi();
String org = "org_example"; // String | 组织的路径(path/login)
OrgsOrgBody1 body = new OrgsOrgBody1(); // OrgsOrgBody1 | 
try {
    GroupMember result = apiInstance.patchUserMembershipsOrgsOrg(org, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationsApi#patchUserMembershipsOrgsOrg");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |
 **body** | [**OrgsOrgBody1**](OrgsOrgBody1.md)|  | [optional]

### Return type

[**GroupMember**](GroupMember.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/json

<a name="postUsersOrganization"></a>
# **postUsersOrganization**
> Group postUsersOrganization(body)

创建组织

创建组织

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.OrganizationsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationsApi apiInstance = new OrganizationsApi();
UsersOrganizationBody body = new UsersOrganizationBody(); // UsersOrganizationBody | 
try {
    Group result = apiInstance.postUsersOrganization(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationsApi#postUsersOrganization");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UsersOrganizationBody**](UsersOrganizationBody.md)|  | [optional]

### Return type

[**Group**](Group.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putOrgsOrgMembershipsUsername"></a>
# **putOrgsOrgMembershipsUsername**
> GroupMember putOrgsOrgMembershipsUsername(org, username, body)

增加或更新授权用户所管理组织的成员

增加或更新授权用户所管理组织的成员

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.OrganizationsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationsApi apiInstance = new OrganizationsApi();
String org = "org_example"; // String | 组织的路径(path/login)
String username = "username_example"; // String | 用户名(username/login)
MembershipsUsernameBody body = new MembershipsUsernameBody(); // MembershipsUsernameBody | 
try {
    GroupMember result = apiInstance.putOrgsOrgMembershipsUsername(org, username, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationsApi#putOrgsOrgMembershipsUsername");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |
 **username** | **String**| 用户名(username/login) |
 **body** | [**MembershipsUsernameBody**](MembershipsUsernameBody.md)|  | [optional]

### Return type

[**GroupMember**](GroupMember.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

