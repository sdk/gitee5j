# OrgsOrgBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **String** | 组织简介 |  [optional]
**email** | **String** | 组织公开的邮箱地址 |  [optional]
**htmlUrl** | **String** | 组织站点 |  [optional]
**location** | **String** | 组织所在地 |  [optional]
**name** | **String** | 组织名称 |  [optional]
