# OwnerIssuesBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assignee** | **String** | Issue负责人的个人空间地址 |  [optional]
**body** | **String** | Issue描述 |  [optional]
**collaborators** | **String** | Issue协助者的个人空间地址, 以 , 分隔 |  [optional]
**issueType** | **String** | 企业自定义任务类型，非企业默认任务类型为“任务” |  [optional]
**labels** | **String** | 用逗号分开的标签，名称要求长度在 2-20 之间且非特殊字符。如: bug,performance |  [optional]
**milestone** | **Integer** | 里程碑序号 |  [optional]
**program** | **String** | 项目ID |  [optional]
**repo** | **String** | 仓库路径(path) |  [optional]
**securityHole** | **Boolean** | 是否是私有issue(默认为false) |  [optional]
**title** | **String** | Issue标题 | 
