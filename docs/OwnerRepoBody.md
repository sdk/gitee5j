# OwnerRepoBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**canComment** | **Boolean** | 允许用户对仓库进行评论。默认： 允许(true) |  [optional]
**defaultBranch** | **String** | 更新默认分支 |  [optional]
**defaultMergeMethod** | [**DefaultMergeMethodEnum**](#DefaultMergeMethodEnum) | 选择默认合并 Pull Request 的方式,分别为 merge squash rebase |  [optional]
**description** | **String** | 仓库描述 |  [optional]
**hasIssues** | **Boolean** | 允许提Issue与否。默认: 允许(true) |  [optional]
**hasWiki** | **Boolean** | 提供Wiki与否。默认: 提供(true) |  [optional]
**homepage** | **String** | 主页(eg: https://gitee.com) |  [optional]
**issueComment** | **Boolean** | 允许对“关闭”状态的 Issue 进行评论。默认: 不允许(false) |  [optional]
**issueTemplateSource** | [**IssueTemplateSourceEnum**](#IssueTemplateSourceEnum) | Issue 模版来源 project: 使用仓库 Issue Template 作为模版； enterprise: 使用企业工作项作为模版 |  [optional]
**lightweightPrEnabled** | **Boolean** | 是否接受轻量级 pull request |  [optional]
**mergeEnabled** | **Boolean** | 是否开启 merge 合并方式, 默认为开启 |  [optional]
**name** | **String** | 仓库名称 | 
**onlineEditEnabled** | **Boolean** | 是否允许仓库文件在线编辑 |  [optional]
**path** | **String** | 更新仓库路径 |  [optional]
**_private** | **Boolean** | 仓库公开或私有。 |  [optional]
**pullRequestsEnabled** | **Boolean** | 接受 pull request，协作开发 |  [optional]
**rebaseEnabled** | **Boolean** | 是否开启 rebase 合并方式, 默认为开启 |  [optional]
**securityHoleEnabled** | **Boolean** | 这个Issue涉及到安全/隐私问题，提交后不公开此Issue（可见范围：仓库成员, 企业成员） |  [optional]
**squashEnabled** | **Boolean** | 是否开启 squash 合并方式, 默认为开启 |  [optional]

<a name="DefaultMergeMethodEnum"></a>
## Enum: DefaultMergeMethodEnum
Name | Value
---- | -----
MERGE | &quot;merge&quot;
SQUASH | &quot;squash&quot;
REBASE | &quot;rebase&quot;

<a name="IssueTemplateSourceEnum"></a>
## Enum: IssueTemplateSourceEnum
Name | Value
---- | -----
PROJECT | &quot;project&quot;
ENTERPRISE | &quot;enterprise&quot;
