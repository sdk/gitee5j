# OwnerRepoBody1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**watchType** | [**WatchTypeEnum**](#WatchTypeEnum) | watch策略, watching: 关注所有动态, ignoring: 关注但不提醒动态 | 

<a name="WatchTypeEnum"></a>
## Enum: WatchTypeEnum
Name | Value
---- | -----
WATCHING | &quot;watching&quot;
IGNORING | &quot;ignoring&quot;
