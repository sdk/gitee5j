# Patch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**aMode** | **String** |  |  [optional]
**bMode** | **String** |  |  [optional]
**deletedFile** | **Boolean** |  |  [optional]
**diff** | **String** |  |  [optional]
**newPath** | **String** |  |  [optional]
**oldPath** | **String** |  |  [optional]
**renamedFile** | **Boolean** |  |  [optional]
**tooLarge** | **Boolean** |  |  [optional]
