# ProgramBasic

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assignee** | [**UserBasic**](UserBasic.md) |  |  [optional]
**author** | [**UserBasic**](UserBasic.md) |  |  [optional]
**description** | **String** | 项目描述 |  [optional]
**id** | **Integer** | 项目id |  [optional]
**name** | **String** | 项目名称 |  [optional]
