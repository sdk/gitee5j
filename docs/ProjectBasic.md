# ProjectBasic

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assigner** | [**UserBasic**](UserBasic.md) |  |  [optional]
**description** | **String** | 仓库描述 |  [optional]
**fork** | **Boolean** | 是否是fork仓库 |  [optional]
**fullName** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**humanName** | **String** |  |  [optional]
**id** | **Integer** |  |  [optional]
**internal** | **Boolean** | 是否内部开源 |  [optional]
**name** | **String** | 仓库名称 |  [optional]
**namespace** | [**NamespaceMini**](NamespaceMini.md) |  |  [optional]
**owner** | [**UserBasic**](UserBasic.md) |  |  [optional]
**path** | **String** | 仓库路径 |  [optional]
**_private** | **Boolean** | 是否私有 |  [optional]
**_public** | **Boolean** | 是否公开 |  [optional]
**sshUrl** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
