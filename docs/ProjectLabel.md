# ProjectLabel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**ident** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
