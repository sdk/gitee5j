# ProjectMemberPermission

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**avatarUrl** | **String** |  |  [optional]
**eventsUrl** | **String** |  |  [optional]
**followersUrl** | **String** |  |  [optional]
**followingUrl** | **String** |  |  [optional]
**gistsUrl** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**id** | **Integer** |  |  [optional]
**login** | **String** |  |  [optional]
**memberRole** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**organizationsUrl** | **String** |  |  [optional]
**permission** | **String** |  |  [optional]
**receivedEventsUrl** | **String** |  |  [optional]
**remark** | **String** | 企业备注名 |  [optional]
**reposUrl** | **String** |  |  [optional]
**starredUrl** | **String** |  |  [optional]
**subscriptionsUrl** | **String** |  |  [optional]
**type** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
