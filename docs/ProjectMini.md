# ProjectMini

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fullName** | **String** |  |  [optional]
**humanName** | **String** |  |  [optional]
**id** | **Integer** |  |  [optional]
**namespace** | [**NamespaceMini**](NamespaceMini.md) |  |  [optional]
**url** | **String** |  |  [optional]
