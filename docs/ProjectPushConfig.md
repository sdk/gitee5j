# ProjectPushConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**authorEmailSuffix** | **String** |  |  [optional]
**commitMessageRegex** | **String** |  |  [optional]
**exceptManager** | **Boolean** |  |  [optional]
**maxFileSize** | **Integer** |  |  [optional]
**restrictAuthorEmailSuffix** | **Boolean** |  |  [optional]
**restrictCommitMessage** | **Boolean** |  |  [optional]
**restrictFileSize** | **Boolean** |  |  [optional]
**restrictPushOwnCommit** | **Boolean** |  |  [optional]
