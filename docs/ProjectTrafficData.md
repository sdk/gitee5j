# ProjectTrafficData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**counts** | [**List&lt;ProjectTrafficDataDesc&gt;**](ProjectTrafficDataDesc.md) | 每天的访问量数据集 |  [optional]
**summary** | [**ProjectTrafficDataSummary**](ProjectTrafficDataSummary.md) |  |  [optional]
