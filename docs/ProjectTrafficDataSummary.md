# ProjectTrafficDataSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**downloadZip** | **Integer** | 每天的ZIP包下载次数 |  [optional]
**ip** | **Integer** | 浏览次数 |  [optional]
**pull** | **Integer** | 拉取次数 |  [optional]
**push** | **Integer** | 推送次数 |  [optional]
