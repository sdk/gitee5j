# ProtectionRule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contexts** | **List&lt;String&gt;** | 检查项列表 |  [optional]
**escapseProtectBranchList** | [**List**](List.md) | 不受规则影响的常规分支列表，以英文逗号分隔，形如：[&#x27;a&#x27;, &#x27;b&#x27;] |  [optional]
**id** | **Integer** |  |  [optional]
**mergers** | **Object** |  |  [optional]
**mode** | **String** | 模式 standard: 标准模式, review: 评审模式 |  [optional]
**projectId** | **Integer** |  |  [optional]
**pushers** | **Object** |  |  [optional]
**strict** | **Boolean** | 是否严格检查 |  [optional]
**wildcard** | **String** |  |  [optional]
