# PullRequestBranch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**label** | **String** |  |  [optional]
**ref** | **String** |  |  [optional]
**repo** | [**ProjectBasic**](ProjectBasic.md) |  |  [optional]
**sha** | **String** |  |  [optional]
**user** | [**UserBasic**](UserBasic.md) |  |  [optional]
