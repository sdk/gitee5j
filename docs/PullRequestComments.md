# PullRequestComments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_links** | [**PullRequestLinks**](PullRequestLinks.md) |  |  [optional]
**body** | **String** |  |  [optional]
**commentType** | **String** |  |  [optional]
**commitId** | **String** |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**id** | **Integer** |  |  [optional]
**inReplyToId** | **Integer** |  |  [optional]
**newLine** | **String** |  |  [optional]
**originalCommitId** | **String** |  |  [optional]
**originalPosition** | **String** |  |  [optional]
**path** | **String** |  |  [optional]
**position** | **String** |  |  [optional]
**pullRequestUrl** | **String** |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**url** | **String** |  |  [optional]
**user** | [**UserBasic**](UserBasic.md) |  |  [optional]
