# PullRequestCommits

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**author** | [**UserBasic**](UserBasic.md) |  |  [optional]
**commentsUrl** | **String** |  |  [optional]
**commit** | [**Commit**](Commit.md) |  |  [optional]
**committer** | [**UserBasic**](UserBasic.md) |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**parents** | [**CommitParentsBasic**](CommitParentsBasic.md) |  |  [optional]
**sha** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
