# PullRequestFiles

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**additions** | **Integer** |  |  [optional]
**blobUrl** | **String** |  |  [optional]
**deletions** | **Integer** |  |  [optional]
**filename** | **String** |  |  [optional]
**patch** | [**Patch**](Patch.md) |  |  [optional]
**rawUrl** | **String** |  |  [optional]
**sha** | **String** |  |  [optional]
**status** | **String** |  |  [optional]
