# PullRequestLinks

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**comments** | [**Href**](Href.md) |  |  [optional]
**commits** | [**Href**](Href.md) |  |  [optional]
**html** | [**Href**](Href.md) |  |  [optional]
**issue** | [**Href**](Href.md) |  |  [optional]
**reviewComments** | [**Href**](Href.md) |  |  [optional]
**self** | [**Href**](Href.md) |  |  [optional]
