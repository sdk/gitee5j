# PullRequestsApi

All URIs are relative to *https://gitee.com/api/v5*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteReposOwnerRepoPullsCommentsId**](PullRequestsApi.md#deleteReposOwnerRepoPullsCommentsId) | **DELETE** /repos/{owner}/{repo}/pulls/comments/{id} | 删除评论
[**deleteReposOwnerRepoPullsNumberAssignees**](PullRequestsApi.md#deleteReposOwnerRepoPullsNumberAssignees) | **DELETE** /repos/{owner}/{repo}/pulls/{number}/assignees | 取消用户审查 Pull Request
[**deleteReposOwnerRepoPullsNumberLabelsName**](PullRequestsApi.md#deleteReposOwnerRepoPullsNumberLabelsName) | **DELETE** /repos/{owner}/{repo}/pulls/{number}/labels/{name} | 删除 Pull Request 标签
[**deleteReposOwnerRepoPullsNumberTesters**](PullRequestsApi.md#deleteReposOwnerRepoPullsNumberTesters) | **DELETE** /repos/{owner}/{repo}/pulls/{number}/testers | 取消用户测试 Pull Request
[**getReposOwnerRepoPulls**](PullRequestsApi.md#getReposOwnerRepoPulls) | **GET** /repos/{owner}/{repo}/pulls | 获取Pull Request列表
[**getReposOwnerRepoPullsCommentsId**](PullRequestsApi.md#getReposOwnerRepoPullsCommentsId) | **GET** /repos/{owner}/{repo}/pulls/comments/{id} | 获取Pull Request的某个评论
[**getReposOwnerRepoPullsNumber**](PullRequestsApi.md#getReposOwnerRepoPullsNumber) | **GET** /repos/{owner}/{repo}/pulls/{number} | 获取单个Pull Request
[**getReposOwnerRepoPullsNumberComments**](PullRequestsApi.md#getReposOwnerRepoPullsNumberComments) | **GET** /repos/{owner}/{repo}/pulls/{number}/comments | 获取某个Pull Request的所有评论
[**getReposOwnerRepoPullsNumberCommits**](PullRequestsApi.md#getReposOwnerRepoPullsNumberCommits) | **GET** /repos/{owner}/{repo}/pulls/{number}/commits | 获取某Pull Request的所有Commit信息。最多显示250条Commit
[**getReposOwnerRepoPullsNumberFiles**](PullRequestsApi.md#getReposOwnerRepoPullsNumberFiles) | **GET** /repos/{owner}/{repo}/pulls/{number}/files | Pull Request Commit文件列表。最多显示300条diff
[**getReposOwnerRepoPullsNumberIssues**](PullRequestsApi.md#getReposOwnerRepoPullsNumberIssues) | **GET** /repos/{owner}/{repo}/pulls/{number}/issues | 获取 Pull Request 关联的 issues
[**getReposOwnerRepoPullsNumberLabels**](PullRequestsApi.md#getReposOwnerRepoPullsNumberLabels) | **GET** /repos/{owner}/{repo}/pulls/{number}/labels | 获取某个 Pull Request 的所有标签
[**getReposOwnerRepoPullsNumberMerge**](PullRequestsApi.md#getReposOwnerRepoPullsNumberMerge) | **GET** /repos/{owner}/{repo}/pulls/{number}/merge | 判断Pull Request是否已经合并
[**getReposOwnerRepoPullsNumberOperateLogs**](PullRequestsApi.md#getReposOwnerRepoPullsNumberOperateLogs) | **GET** /repos/{owner}/{repo}/pulls/{number}/operate_logs | 获取某个Pull Request的操作日志
[**patchReposOwnerRepoPullsCommentsId**](PullRequestsApi.md#patchReposOwnerRepoPullsCommentsId) | **PATCH** /repos/{owner}/{repo}/pulls/comments/{id} | 编辑评论
[**patchReposOwnerRepoPullsNumber**](PullRequestsApi.md#patchReposOwnerRepoPullsNumber) | **PATCH** /repos/{owner}/{repo}/pulls/{number} | 更新Pull Request信息
[**patchReposOwnerRepoPullsNumberAssignees**](PullRequestsApi.md#patchReposOwnerRepoPullsNumberAssignees) | **PATCH** /repos/{owner}/{repo}/pulls/{number}/assignees | 重置 Pull Request 审查 的状态
[**patchReposOwnerRepoPullsNumberTesters**](PullRequestsApi.md#patchReposOwnerRepoPullsNumberTesters) | **PATCH** /repos/{owner}/{repo}/pulls/{number}/testers | 重置 Pull Request 测试 的状态
[**postReposOwnerRepoPulls**](PullRequestsApi.md#postReposOwnerRepoPulls) | **POST** /repos/{owner}/{repo}/pulls | 创建Pull Request
[**postReposOwnerRepoPullsNumberAssignees**](PullRequestsApi.md#postReposOwnerRepoPullsNumberAssignees) | **POST** /repos/{owner}/{repo}/pulls/{number}/assignees | 指派用户审查 Pull Request
[**postReposOwnerRepoPullsNumberComments**](PullRequestsApi.md#postReposOwnerRepoPullsNumberComments) | **POST** /repos/{owner}/{repo}/pulls/{number}/comments | 提交Pull Request评论
[**postReposOwnerRepoPullsNumberLabels**](PullRequestsApi.md#postReposOwnerRepoPullsNumberLabels) | **POST** /repos/{owner}/{repo}/pulls/{number}/labels | 创建 Pull Request 标签
[**postReposOwnerRepoPullsNumberReview**](PullRequestsApi.md#postReposOwnerRepoPullsNumberReview) | **POST** /repos/{owner}/{repo}/pulls/{number}/review | 处理 Pull Request 审查
[**postReposOwnerRepoPullsNumberTest**](PullRequestsApi.md#postReposOwnerRepoPullsNumberTest) | **POST** /repos/{owner}/{repo}/pulls/{number}/test | 处理 Pull Request 测试
[**postReposOwnerRepoPullsNumberTesters**](PullRequestsApi.md#postReposOwnerRepoPullsNumberTesters) | **POST** /repos/{owner}/{repo}/pulls/{number}/testers | 指派用户测试 Pull Request
[**putReposOwnerRepoPullsNumberLabels**](PullRequestsApi.md#putReposOwnerRepoPullsNumberLabels) | **PUT** /repos/{owner}/{repo}/pulls/{number}/labels | 替换 Pull Request 所有标签
[**putReposOwnerRepoPullsNumberMerge**](PullRequestsApi.md#putReposOwnerRepoPullsNumberMerge) | **PUT** /repos/{owner}/{repo}/pulls/{number}/merge | 合并Pull Request

<a name="deleteReposOwnerRepoPullsCommentsId"></a>
# **deleteReposOwnerRepoPullsCommentsId**
> deleteReposOwnerRepoPullsCommentsId(owner, repo, id)

删除评论

删除评论

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.PullRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 评论的ID
try {
    apiInstance.deleteReposOwnerRepoPullsCommentsId(owner, repo, id);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#deleteReposOwnerRepoPullsCommentsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| 评论的ID |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteReposOwnerRepoPullsNumberAssignees"></a>
# **deleteReposOwnerRepoPullsNumberAssignees**
> PullRequest deleteReposOwnerRepoPullsNumberAssignees(owner, repo, number, assignees)

取消用户审查 Pull Request

取消用户审查 Pull Request

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.PullRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
String assignees = "assignees_example"; // String | 用户的个人空间地址, 以 , 分隔
try {
    PullRequest result = apiInstance.deleteReposOwnerRepoPullsNumberAssignees(owner, repo, number, assignees);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#deleteReposOwnerRepoPullsNumberAssignees");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **assignees** | **String**| 用户的个人空间地址, 以 , 分隔 |

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteReposOwnerRepoPullsNumberLabelsName"></a>
# **deleteReposOwnerRepoPullsNumberLabelsName**
> deleteReposOwnerRepoPullsNumberLabelsName(owner, repo, number, name)

删除 Pull Request 标签

删除 Pull Request 标签

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.PullRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
String name = "name_example"; // String | 标签名称(批量删除用英文逗号分隔，如: bug,feature)
try {
    apiInstance.deleteReposOwnerRepoPullsNumberLabelsName(owner, repo, number, name);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#deleteReposOwnerRepoPullsNumberLabelsName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **name** | **String**| 标签名称(批量删除用英文逗号分隔，如: bug,feature) |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteReposOwnerRepoPullsNumberTesters"></a>
# **deleteReposOwnerRepoPullsNumberTesters**
> PullRequest deleteReposOwnerRepoPullsNumberTesters(owner, repo, number, testers)

取消用户测试 Pull Request

取消用户测试 Pull Request

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.PullRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
String testers = "testers_example"; // String | 用户的个人空间地址, 以 , 分隔
try {
    PullRequest result = apiInstance.deleteReposOwnerRepoPullsNumberTesters(owner, repo, number, testers);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#deleteReposOwnerRepoPullsNumberTesters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **testers** | **String**| 用户的个人空间地址, 以 , 分隔 |

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoPulls"></a>
# **getReposOwnerRepoPulls**
> List&lt;PullRequest&gt; getReposOwnerRepoPulls(owner, repo, state, head, base, sort, since, direction, milestoneNumber, labels, author, assignee, tester, page, perPage)

获取Pull Request列表

获取Pull Request列表

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.PullRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String state = "open"; // String | 可选。Pull Request 状态
String head = "head_example"; // String | 可选。Pull Request 提交的源分支。格式：branch 或者：username:branch
String base = "base_example"; // String | 可选。Pull Request 提交目标分支的名称。
String sort = "created"; // String | 可选。排序字段，默认按创建时间
String since = "since_example"; // String | 可选。起始的更新时间，要求时间格式为 ISO 8601
String direction = "desc"; // String | 可选。升序/降序
Integer milestoneNumber = 56; // Integer | 可选。里程碑序号(id)
String labels = "labels_example"; // String | 用逗号分开的标签。如: bug,performance
String author = "author_example"; // String | 可选。PR 创建者用户名
String assignee = "assignee_example"; // String | 可选。评审者用户名
String tester = "tester_example"; // String | 可选。测试者用户名
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<PullRequest> result = apiInstance.getReposOwnerRepoPulls(owner, repo, state, head, base, sort, since, direction, milestoneNumber, labels, author, assignee, tester, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#getReposOwnerRepoPulls");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **state** | **String**| 可选。Pull Request 状态 | [optional] [default to open] [enum: open, closed, merged, all]
 **head** | **String**| 可选。Pull Request 提交的源分支。格式：branch 或者：username:branch | [optional]
 **base** | **String**| 可选。Pull Request 提交目标分支的名称。 | [optional]
 **sort** | **String**| 可选。排序字段，默认按创建时间 | [optional] [default to created] [enum: created, updated, popularity, long-running]
 **since** | **String**| 可选。起始的更新时间，要求时间格式为 ISO 8601 | [optional]
 **direction** | **String**| 可选。升序/降序 | [optional] [default to desc] [enum: asc, desc]
 **milestoneNumber** | **Integer**| 可选。里程碑序号(id) | [optional]
 **labels** | **String**| 用逗号分开的标签。如: bug,performance | [optional]
 **author** | **String**| 可选。PR 创建者用户名 | [optional]
 **assignee** | **String**| 可选。评审者用户名 | [optional]
 **tester** | **String**| 可选。测试者用户名 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;PullRequest&gt;**](PullRequest.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoPullsCommentsId"></a>
# **getReposOwnerRepoPullsCommentsId**
> PullRequestComments getReposOwnerRepoPullsCommentsId(owner, repo, id)

获取Pull Request的某个评论

获取Pull Request的某个评论

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.PullRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 
try {
    PullRequestComments result = apiInstance.getReposOwnerRepoPullsCommentsId(owner, repo, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#getReposOwnerRepoPullsCommentsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**|  |

### Return type

[**PullRequestComments**](PullRequestComments.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoPullsNumber"></a>
# **getReposOwnerRepoPullsNumber**
> PullRequest getReposOwnerRepoPullsNumber(owner, repo, number)

获取单个Pull Request

获取单个Pull Request

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.PullRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
try {
    PullRequest result = apiInstance.getReposOwnerRepoPullsNumber(owner, repo, number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#getReposOwnerRepoPullsNumber");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoPullsNumberComments"></a>
# **getReposOwnerRepoPullsNumberComments**
> List&lt;PullRequestComments&gt; getReposOwnerRepoPullsNumberComments(owner, repo, number, page, perPage, direction, commentType)

获取某个Pull Request的所有评论

获取某个Pull Request的所有评论

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.PullRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
String direction = "direction_example"; // String | 可选。升序/降序
String commentType = "commentType_example"; // String | 可选。筛选评论类型。代码行评论/pr普通评论
try {
    List<PullRequestComments> result = apiInstance.getReposOwnerRepoPullsNumberComments(owner, repo, number, page, perPage, direction, commentType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#getReposOwnerRepoPullsNumberComments");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]
 **direction** | **String**| 可选。升序/降序 | [optional] [enum: asc, desc]
 **commentType** | **String**| 可选。筛选评论类型。代码行评论/pr普通评论 | [optional] [enum: diff_comment, pr_comment]

### Return type

[**List&lt;PullRequestComments&gt;**](PullRequestComments.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoPullsNumberCommits"></a>
# **getReposOwnerRepoPullsNumberCommits**
> List&lt;PullRequestCommits&gt; getReposOwnerRepoPullsNumberCommits(owner, repo, number)

获取某Pull Request的所有Commit信息。最多显示250条Commit

获取某Pull Request的所有Commit信息。最多显示250条Commit

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.PullRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
try {
    List<PullRequestCommits> result = apiInstance.getReposOwnerRepoPullsNumberCommits(owner, repo, number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#getReposOwnerRepoPullsNumberCommits");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |

### Return type

[**List&lt;PullRequestCommits&gt;**](PullRequestCommits.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoPullsNumberFiles"></a>
# **getReposOwnerRepoPullsNumberFiles**
> List&lt;PullRequestFiles&gt; getReposOwnerRepoPullsNumberFiles(owner, repo, number)

Pull Request Commit文件列表。最多显示300条diff

Pull Request Commit文件列表。最多显示300条diff

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.PullRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
try {
    List<PullRequestFiles> result = apiInstance.getReposOwnerRepoPullsNumberFiles(owner, repo, number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#getReposOwnerRepoPullsNumberFiles");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |

### Return type

[**List&lt;PullRequestFiles&gt;**](PullRequestFiles.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoPullsNumberIssues"></a>
# **getReposOwnerRepoPullsNumberIssues**
> List&lt;Issue&gt; getReposOwnerRepoPullsNumberIssues(owner, repo, number, page, perPage)

获取 Pull Request 关联的 issues

获取 Pull Request 关联的 issues

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.PullRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Issue> result = apiInstance.getReposOwnerRepoPullsNumberIssues(owner, repo, number, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#getReposOwnerRepoPullsNumberIssues");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**|  |
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;Issue&gt;**](Issue.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoPullsNumberLabels"></a>
# **getReposOwnerRepoPullsNumberLabels**
> List&lt;Label&gt; getReposOwnerRepoPullsNumberLabels(owner, repo, number, page, perPage)

获取某个 Pull Request 的所有标签

获取某个 Pull Request 的所有标签

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.PullRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Label> result = apiInstance.getReposOwnerRepoPullsNumberLabels(owner, repo, number, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#getReposOwnerRepoPullsNumberLabels");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;Label&gt;**](Label.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoPullsNumberMerge"></a>
# **getReposOwnerRepoPullsNumberMerge**
> getReposOwnerRepoPullsNumberMerge(owner, repo, number)

判断Pull Request是否已经合并

判断Pull Request是否已经合并

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.PullRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
try {
    apiInstance.getReposOwnerRepoPullsNumberMerge(owner, repo, number);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#getReposOwnerRepoPullsNumberMerge");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getReposOwnerRepoPullsNumberOperateLogs"></a>
# **getReposOwnerRepoPullsNumberOperateLogs**
> List&lt;OperateLog&gt; getReposOwnerRepoPullsNumberOperateLogs(owner, repo, number, sort)

获取某个Pull Request的操作日志

获取某个Pull Request的操作日志

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.PullRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
String sort = "desc"; // String | 按递增(asc)或递减(desc)排序，默认：递减
try {
    List<OperateLog> result = apiInstance.getReposOwnerRepoPullsNumberOperateLogs(owner, repo, number, sort);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#getReposOwnerRepoPullsNumberOperateLogs");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **sort** | **String**| 按递增(asc)或递减(desc)排序，默认：递减 | [optional] [default to desc] [enum: desc, asc]

### Return type

[**List&lt;OperateLog&gt;**](OperateLog.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="patchReposOwnerRepoPullsCommentsId"></a>
# **patchReposOwnerRepoPullsCommentsId**
> PullRequestComments patchReposOwnerRepoPullsCommentsId(owner, repo, id, body)

编辑评论

编辑评论

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.PullRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 评论的ID
CommentBody body = new CommentBody(); // CommentBody | 
try {
    PullRequestComments result = apiInstance.patchReposOwnerRepoPullsCommentsId(owner, repo, id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#patchReposOwnerRepoPullsCommentsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| 评论的ID |
 **body** | [**CommentBody**](CommentBody.md)|  | [optional]

### Return type

[**PullRequestComments**](PullRequestComments.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/json

<a name="patchReposOwnerRepoPullsNumber"></a>
# **patchReposOwnerRepoPullsNumber**
> PullRequest patchReposOwnerRepoPullsNumber(owner, repo, number, body)

更新Pull Request信息

更新Pull Request信息

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.PullRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
PullsNumberBody body = new PullsNumberBody(); // PullsNumberBody | 
try {
    PullRequest result = apiInstance.patchReposOwnerRepoPullsNumber(owner, repo, number, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#patchReposOwnerRepoPullsNumber");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **body** | [**PullsNumberBody**](PullsNumberBody.md)|  | [optional]

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/json

<a name="patchReposOwnerRepoPullsNumberAssignees"></a>
# **patchReposOwnerRepoPullsNumberAssignees**
> PullRequest patchReposOwnerRepoPullsNumberAssignees(owner, repo, number, body)

重置 Pull Request 审查 的状态

重置 Pull Request 审查 的状态

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.PullRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
NumberAssigneesBody1 body = new NumberAssigneesBody1(); // NumberAssigneesBody1 | 
try {
    PullRequest result = apiInstance.patchReposOwnerRepoPullsNumberAssignees(owner, repo, number, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#patchReposOwnerRepoPullsNumberAssignees");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **body** | [**NumberAssigneesBody1**](NumberAssigneesBody1.md)|  | [optional]

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/json

<a name="patchReposOwnerRepoPullsNumberTesters"></a>
# **patchReposOwnerRepoPullsNumberTesters**
> PullRequest patchReposOwnerRepoPullsNumberTesters(owner, repo, number, body)

重置 Pull Request 测试 的状态

重置 Pull Request 测试 的状态

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.PullRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
NumberTestersBody1 body = new NumberTestersBody1(); // NumberTestersBody1 | 
try {
    PullRequest result = apiInstance.patchReposOwnerRepoPullsNumberTesters(owner, repo, number, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#patchReposOwnerRepoPullsNumberTesters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **body** | [**NumberTestersBody1**](NumberTestersBody1.md)|  | [optional]

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/json

<a name="postReposOwnerRepoPulls"></a>
# **postReposOwnerRepoPulls**
> PullRequest postReposOwnerRepoPulls(owner, repo, body)

创建Pull Request

创建Pull Request

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.PullRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
RepoPullsBody body = new RepoPullsBody(); // RepoPullsBody | 
try {
    PullRequest result = apiInstance.postReposOwnerRepoPulls(owner, repo, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#postReposOwnerRepoPulls");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **body** | [**RepoPullsBody**](RepoPullsBody.md)|  | [optional]

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postReposOwnerRepoPullsNumberAssignees"></a>
# **postReposOwnerRepoPullsNumberAssignees**
> PullRequest postReposOwnerRepoPullsNumberAssignees(owner, repo, number, body)

指派用户审查 Pull Request

指派用户审查 Pull Request

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.PullRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
NumberAssigneesBody body = new NumberAssigneesBody(); // NumberAssigneesBody | 
try {
    PullRequest result = apiInstance.postReposOwnerRepoPullsNumberAssignees(owner, repo, number, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#postReposOwnerRepoPullsNumberAssignees");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **body** | [**NumberAssigneesBody**](NumberAssigneesBody.md)|  | [optional]

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postReposOwnerRepoPullsNumberComments"></a>
# **postReposOwnerRepoPullsNumberComments**
> PullRequestComments postReposOwnerRepoPullsNumberComments(owner, repo, number, body)

提交Pull Request评论

提交Pull Request评论

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.PullRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
NumberCommentsBody1 body = new NumberCommentsBody1(); // NumberCommentsBody1 | 
try {
    PullRequestComments result = apiInstance.postReposOwnerRepoPullsNumberComments(owner, repo, number, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#postReposOwnerRepoPullsNumberComments");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **body** | [**NumberCommentsBody1**](NumberCommentsBody1.md)|  | [optional]

### Return type

[**PullRequestComments**](PullRequestComments.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postReposOwnerRepoPullsNumberLabels"></a>
# **postReposOwnerRepoPullsNumberLabels**
> List&lt;Label&gt; postReposOwnerRepoPullsNumberLabels(owner, repo, number, body)

创建 Pull Request 标签

创建 Pull Request 标签  需要在请求的 body 里填上数组，元素为标签的名字。如: [\&quot;performance\&quot;, \&quot;bug\&quot;]

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.PullRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
List<String> body = Arrays.asList("body_example"); // List<String> | 标签名数组，如: ["feat", "bug"]
try {
    List<Label> result = apiInstance.postReposOwnerRepoPullsNumberLabels(owner, repo, number, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#postReposOwnerRepoPullsNumberLabels");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **body** | [**List&lt;String&gt;**](String.md)| 标签名数组，如: [&quot;feat&quot;, &quot;bug&quot;] | [optional]

### Return type

[**List&lt;Label&gt;**](Label.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postReposOwnerRepoPullsNumberReview"></a>
# **postReposOwnerRepoPullsNumberReview**
> postReposOwnerRepoPullsNumberReview(owner, repo, number, body)

处理 Pull Request 审查

处理 Pull Request 审查

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.PullRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
NumberReviewBody body = new NumberReviewBody(); // NumberReviewBody | 
try {
    apiInstance.postReposOwnerRepoPullsNumberReview(owner, repo, number, body);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#postReposOwnerRepoPullsNumberReview");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **body** | [**NumberReviewBody**](NumberReviewBody.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="postReposOwnerRepoPullsNumberTest"></a>
# **postReposOwnerRepoPullsNumberTest**
> postReposOwnerRepoPullsNumberTest(owner, repo, number, body)

处理 Pull Request 测试

处理 Pull Request 测试

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.PullRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
NumberTestBody body = new NumberTestBody(); // NumberTestBody | 
try {
    apiInstance.postReposOwnerRepoPullsNumberTest(owner, repo, number, body);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#postReposOwnerRepoPullsNumberTest");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **body** | [**NumberTestBody**](NumberTestBody.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="postReposOwnerRepoPullsNumberTesters"></a>
# **postReposOwnerRepoPullsNumberTesters**
> PullRequest postReposOwnerRepoPullsNumberTesters(owner, repo, number, body)

指派用户测试 Pull Request

指派用户测试 Pull Request

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.PullRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
NumberTestersBody body = new NumberTestersBody(); // NumberTestersBody | 
try {
    PullRequest result = apiInstance.postReposOwnerRepoPullsNumberTesters(owner, repo, number, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#postReposOwnerRepoPullsNumberTesters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **body** | [**NumberTestersBody**](NumberTestersBody.md)|  | [optional]

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putReposOwnerRepoPullsNumberLabels"></a>
# **putReposOwnerRepoPullsNumberLabels**
> List&lt;Label&gt; putReposOwnerRepoPullsNumberLabels(owner, repo, number, body)

替换 Pull Request 所有标签

替换 Pull Request 所有标签  需要在请求的body里填上数组，元素为标签的名字。如: [\&quot;performance\&quot;, \&quot;bug\&quot;]

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.PullRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
List<String> body = Arrays.asList("body_example"); // List<String> | 标签名数组，如: ["feat", "bug"]
try {
    List<Label> result = apiInstance.putReposOwnerRepoPullsNumberLabels(owner, repo, number, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#putReposOwnerRepoPullsNumberLabels");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **body** | [**List&lt;String&gt;**](String.md)| 标签名数组，如: [&quot;feat&quot;, &quot;bug&quot;] | [optional]

### Return type

[**List&lt;Label&gt;**](Label.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putReposOwnerRepoPullsNumberMerge"></a>
# **putReposOwnerRepoPullsNumberMerge**
> putReposOwnerRepoPullsNumberMerge(owner, repo, number, body)

合并Pull Request

合并Pull Request

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.PullRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
NumberMergeBody body = new NumberMergeBody(); // NumberMergeBody | 
try {
    apiInstance.putReposOwnerRepoPullsNumberMerge(owner, repo, number, body);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#putReposOwnerRepoPullsNumberMerge");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **body** | [**NumberMergeBody**](NumberMergeBody.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

