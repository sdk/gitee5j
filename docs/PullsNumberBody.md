# PullsNumberBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assigneesNumber** | **Integer** | 可选。最少审查人数 |  [optional]
**body** | **String** | 可选。Pull Request 内容 |  [optional]
**closeRelatedIssue** | **Boolean** | 可选，合并后是否关闭关联的 Issue，默认根据仓库配置设置 |  [optional]
**draft** | **Boolean** | 是否设置为草稿 |  [optional]
**labels** | **String** | 用逗号分开的标签，名称要求长度在 2-20 之间且非特殊字符。如: bug,performance |  [optional]
**milestoneNumber** | **Integer** | 可选。里程碑序号(id) |  [optional]
**refPullRequestNumbers** | **String** | 可选。依赖的当前仓库下的PR编号，置空则清空依赖的PR。如：17,18,19 |  [optional]
**squash** | **Boolean** | 接受 Pull Request 时使用扁平化（Squash）合并 |  [optional]
**state** | [**StateEnum**](#StateEnum) | 可选。Pull Request 状态 |  [optional]
**testersNumber** | **Integer** | 可选。最少测试人数 |  [optional]
**title** | **String** | 可选。Pull Request 标题 |  [optional]

<a name="StateEnum"></a>
## Enum: StateEnum
Name | Value
---- | -----
CLOSED | &quot;closed&quot;
OPEN | &quot;open&quot;
