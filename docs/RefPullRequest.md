# RefPullRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Integer** |  |  [optional]
**state** | **String** |  |  [optional]
**title** | **String** |  |  [optional]
