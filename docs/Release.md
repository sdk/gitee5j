# Release

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assets** | [**List&lt;ReleaseAsset&gt;**](ReleaseAsset.md) |  |  [optional]
**author** | [**UserBasic**](UserBasic.md) |  |  [optional]
**body** | **String** |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**id** | **Integer** |  |  [optional]
**name** | **String** |  |  [optional]
**prerelease** | **Boolean** |  |  [optional]
**tagName** | **String** |  |  [optional]
**targetCommitish** | **String** |  |  [optional]
