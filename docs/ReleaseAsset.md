# ReleaseAsset

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**browserDownloadUrl** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
