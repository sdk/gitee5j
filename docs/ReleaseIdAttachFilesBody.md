# ReleaseIdAttachFilesBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | [**File**](File.md) | 上传的文件 |  [optional]
