# RepoBaiduStatisticKeyBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** | 通过百度统计页面获取的 hm.js? 后面的 key |  [optional]
