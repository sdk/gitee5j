# RepoBranchesBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**branchName** | **String** | 新创建的分支名称 | 
**refs** | **String** | 起点名称, 默认：master | 
