# RepoCommitWithFiles

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**author** | [**UserBasic**](UserBasic.md) |  |  [optional]
**commentsUrl** | **String** |  |  [optional]
**commit** | [**Commit**](Commit.md) |  |  [optional]
**committer** | [**UserBasic**](UserBasic.md) |  |  [optional]
**files** | [**List&lt;DiffFile&gt;**](DiffFile.md) | 文件列表 |  [optional]
**htmlUrl** | **String** |  |  [optional]
**parents** | [**List&lt;GitSha&gt;**](GitSha.md) |  |  [optional]
**sha** | **String** |  |  [optional]
**stats** | [**CommitStats**](CommitStats.md) |  |  [optional]
**truncated** | **Boolean** | 文件列表是否被截断 |  [optional]
**url** | **String** |  |  [optional]
