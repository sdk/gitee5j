# RepoCommitsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**actions** | [**List&lt;GitAction&gt;**](GitAction.md) |  | 
**author** | [**GitUserBasic**](GitUserBasic.md) |  |  [optional]
**branch** | **String** | 变更的目标分支名。创建新分支时需提供 &#x60;start_branch&#x60; 参数 | 
**message** | **String** | 提交信息 | 
**startBranch** | **String** | 分支起地点。新建分支时使用，更新分支时可选 |  [optional]
