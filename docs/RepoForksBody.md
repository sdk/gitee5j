# RepoForksBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | fork 后仓库名称。默认: 源仓库名称 |  [optional]
**organization** | **String** | 组织空间完整地址，不填写默认Fork到用户个人空间地址 |  [optional]
**path** | **String** | fork 后仓库地址。默认: 源仓库地址 |  [optional]
