# RepoKeysBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** | 公钥内容 | 
**title** | **String** | 公钥名称 | 
