# RepoLabelsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**color** | **String** | 标签颜色。为6位的数字，如: 000000 | 
**name** | **String** | 标签名称 | 
