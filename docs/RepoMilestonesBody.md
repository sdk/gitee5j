# RepoMilestonesBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **String** | 里程碑具体描述 |  [optional]
**dueOn** | [**LocalDate**](LocalDate.md) | 里程碑的截止日期 | 
**state** | [**StateEnum**](#StateEnum) | 里程碑状态: open, closed, all。默认: open |  [optional]
**title** | **String** | 里程碑标题 | 

<a name="StateEnum"></a>
## Enum: StateEnum
Name | Value
---- | -----
OPEN | &quot;open&quot;
CLOSED | &quot;closed&quot;
ALL | &quot;all&quot;
