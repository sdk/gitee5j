# RepoNotificationsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ids** | **String** | 指定一组通知 ID，以 , 分隔 |  [optional]
