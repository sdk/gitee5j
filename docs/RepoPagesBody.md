# RepoPagesBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**domain** | **String** | 自定义域名 | 
**sslCertificateCrt** | **String** | 证书文件内容（需进行BASE64编码） |  [optional]
**sslCertificateKey** | **String** | 私钥文件内容（需进行BASE64编码） |  [optional]
