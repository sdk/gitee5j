# RepoPushConfigBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**authorEmailSuffix** | **String** | 指定邮箱域名的后缀 |  [optional]
**commitMessageRegex** | **String** | 用于验证提交信息的正则表达式 |  [optional]
**exceptManager** | **Boolean** | 仓库管理员不受上述规则限制 |  [optional]
**maxFileSize** | **Integer** | 限制单文件大小（MB） |  [optional]
**restrictAuthorEmailSuffix** | **Boolean** | 启用只允许指定邮箱域名后缀的提交 |  [optional]
**restrictCommitMessage** | **Boolean** | 启用提交信息正则表达式校验 |  [optional]
**restrictFileSize** | **Boolean** | 启用限制单文件大小 |  [optional]
**restrictPushOwnCommit** | **Boolean** | 启用只能推送自己的提交（所推送提交中的邮箱必须与推送者所设置的提交邮箱一致） |  [optional]
