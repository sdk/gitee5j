# RepoReleasesBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**body** | **String** | Release 描述 | 
**name** | **String** | Release 名称 | 
**prerelease** | **Boolean** | 是否为预览版本。默认: false（非预览版本） |  [optional]
**tagName** | **String** | Tag 名称, 提倡以v字母为前缀做为Release名称，例如v1.0或者v2.3.4 | 
**targetCommitish** | **String** | 分支名称或者commit SHA, 默认是当前默认分支 | 
