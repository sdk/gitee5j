# RepoReviewerBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assignees** | **String** | 审查人员username，可多个，半角逗号分隔，如：(username1,username2) | 
**assigneesNumber** | **Integer** | 最少审查人数 | 
**testers** | **String** | 测试人员username，可多个，半角逗号分隔，如：(username1,username2) | 
**testersNumber** | **Integer** | 最少测试人数 | 
