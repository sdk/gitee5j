# RepoTagsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**refs** | **String** | 起点名称, 默认：master | 
**tagMessage** | **String** | Tag 描述, 默认为空 |  [optional]
**tagName** | **String** | 新创建的标签名称 | 
