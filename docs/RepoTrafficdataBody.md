# RepoTrafficdataBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**endDay** | **String** | 访问量的结束时间，默认七天前，格式：yyyy-MM-dd |  [optional]
**startDay** | **String** | 访问量的开始时间，默认今天，格式：yyyy-MM-dd |  [optional]
