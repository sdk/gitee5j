# RepositoriesApi

All URIs are relative to *https://gitee.com/api/v5*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteReposOwnerRepo**](RepositoriesApi.md#deleteReposOwnerRepo) | **DELETE** /repos/{owner}/{repo} | 删除一个仓库
[**deleteReposOwnerRepoBaiduStatisticKey**](RepositoriesApi.md#deleteReposOwnerRepoBaiduStatisticKey) | **DELETE** /repos/{owner}/{repo}/baidu_statistic_key | 删除仓库的百度统计 key
[**deleteReposOwnerRepoBranchesBranchProtection**](RepositoriesApi.md#deleteReposOwnerRepoBranchesBranchProtection) | **DELETE** /repos/{owner}/{repo}/branches/{branch}/protection | 取消保护分支的设置
[**deleteReposOwnerRepoBranchesWildcardSetting**](RepositoriesApi.md#deleteReposOwnerRepoBranchesWildcardSetting) | **DELETE** /repos/{owner}/{repo}/branches/{wildcard}/setting | 删除保护分支规则
[**deleteReposOwnerRepoCollaboratorsUsername**](RepositoriesApi.md#deleteReposOwnerRepoCollaboratorsUsername) | **DELETE** /repos/{owner}/{repo}/collaborators/{username} | 移除仓库成员
[**deleteReposOwnerRepoCommentsId**](RepositoriesApi.md#deleteReposOwnerRepoCommentsId) | **DELETE** /repos/{owner}/{repo}/comments/{id} | 删除Commit评论
[**deleteReposOwnerRepoContentsPath**](RepositoriesApi.md#deleteReposOwnerRepoContentsPath) | **DELETE** /repos/{owner}/{repo}/contents/{path} | 删除文件
[**deleteReposOwnerRepoKeysEnableId**](RepositoriesApi.md#deleteReposOwnerRepoKeysEnableId) | **DELETE** /repos/{owner}/{repo}/keys/enable/{id} | 停用仓库公钥
[**deleteReposOwnerRepoKeysId**](RepositoriesApi.md#deleteReposOwnerRepoKeysId) | **DELETE** /repos/{owner}/{repo}/keys/{id} | 删除一个仓库公钥
[**deleteReposOwnerRepoReleasesId**](RepositoriesApi.md#deleteReposOwnerRepoReleasesId) | **DELETE** /repos/{owner}/{repo}/releases/{id} | 删除仓库Release
[**deleteReposOwnerRepoReleasesReleaseIdAttachFilesAttachFileId**](RepositoriesApi.md#deleteReposOwnerRepoReleasesReleaseIdAttachFilesAttachFileId) | **DELETE** /repos/{owner}/{repo}/releases/{release_id}/attach_files/{attach_file_id} | 删除仓库下指定 Release 的指定附件
[**getEnterprisesEnterpriseRepos**](RepositoriesApi.md#getEnterprisesEnterpriseRepos) | **GET** /enterprises/{enterprise}/repos | 获取企业的所有仓库
[**getOrgsOrgRepos**](RepositoriesApi.md#getOrgsOrgRepos) | **GET** /orgs/{org}/repos | 获取一个组织的仓库
[**getReposOwnerRepo**](RepositoriesApi.md#getReposOwnerRepo) | **GET** /repos/{owner}/{repo} | 获取用户的某个仓库
[**getReposOwnerRepoBaiduStatisticKey**](RepositoriesApi.md#getReposOwnerRepoBaiduStatisticKey) | **GET** /repos/{owner}/{repo}/baidu_statistic_key | 获取仓库的百度统计 key
[**getReposOwnerRepoBlamePath**](RepositoriesApi.md#getReposOwnerRepoBlamePath) | **GET** /repos/{owner}/{repo}/blame/{path} | Blame
[**getReposOwnerRepoBranches**](RepositoriesApi.md#getReposOwnerRepoBranches) | **GET** /repos/{owner}/{repo}/branches | 获取所有分支
[**getReposOwnerRepoBranchesBranch**](RepositoriesApi.md#getReposOwnerRepoBranchesBranch) | **GET** /repos/{owner}/{repo}/branches/{branch} | 获取单个分支
[**getReposOwnerRepoCollaborators**](RepositoriesApi.md#getReposOwnerRepoCollaborators) | **GET** /repos/{owner}/{repo}/collaborators | 获取仓库的所有成员
[**getReposOwnerRepoCollaboratorsUsername**](RepositoriesApi.md#getReposOwnerRepoCollaboratorsUsername) | **GET** /repos/{owner}/{repo}/collaborators/{username} | 判断用户是否为仓库成员
[**getReposOwnerRepoCollaboratorsUsernamePermission**](RepositoriesApi.md#getReposOwnerRepoCollaboratorsUsernamePermission) | **GET** /repos/{owner}/{repo}/collaborators/{username}/permission | 查看仓库成员的权限
[**getReposOwnerRepoComments**](RepositoriesApi.md#getReposOwnerRepoComments) | **GET** /repos/{owner}/{repo}/comments | 获取仓库的 Commit 评论
[**getReposOwnerRepoCommentsId**](RepositoriesApi.md#getReposOwnerRepoCommentsId) | **GET** /repos/{owner}/{repo}/comments/{id} | 获取仓库的某条Commit评论
[**getReposOwnerRepoCommits**](RepositoriesApi.md#getReposOwnerRepoCommits) | **GET** /repos/{owner}/{repo}/commits | 仓库的所有提交
[**getReposOwnerRepoCommitsRefComments**](RepositoriesApi.md#getReposOwnerRepoCommitsRefComments) | **GET** /repos/{owner}/{repo}/commits/{ref}/comments | 获取单个Commit的评论
[**getReposOwnerRepoCommitsSha**](RepositoriesApi.md#getReposOwnerRepoCommitsSha) | **GET** /repos/{owner}/{repo}/commits/{sha} | 仓库的某个提交
[**getReposOwnerRepoCompareBaseHead**](RepositoriesApi.md#getReposOwnerRepoCompareBaseHead) | **GET** /repos/{owner}/{repo}/compare/{base}...{head} | Commits 对比
[**getReposOwnerRepoContentsPath**](RepositoriesApi.md#getReposOwnerRepoContentsPath) | **GET** /repos/{owner}/{repo}/contents/{path} | 获取仓库具体路径下的内容
[**getReposOwnerRepoContributors**](RepositoriesApi.md#getReposOwnerRepoContributors) | **GET** /repos/{owner}/{repo}/contributors | 获取仓库贡献者
[**getReposOwnerRepoForks**](RepositoriesApi.md#getReposOwnerRepoForks) | **GET** /repos/{owner}/{repo}/forks | 查看仓库的Forks
[**getReposOwnerRepoKeys**](RepositoriesApi.md#getReposOwnerRepoKeys) | **GET** /repos/{owner}/{repo}/keys | 获取仓库已部署的公钥
[**getReposOwnerRepoKeysAvailable**](RepositoriesApi.md#getReposOwnerRepoKeysAvailable) | **GET** /repos/{owner}/{repo}/keys/available | 获取仓库可部署的公钥
[**getReposOwnerRepoKeysId**](RepositoriesApi.md#getReposOwnerRepoKeysId) | **GET** /repos/{owner}/{repo}/keys/{id} | 获取仓库的单个公钥
[**getReposOwnerRepoPages**](RepositoriesApi.md#getReposOwnerRepoPages) | **GET** /repos/{owner}/{repo}/pages | 获取Pages信息
[**getReposOwnerRepoPushConfig**](RepositoriesApi.md#getReposOwnerRepoPushConfig) | **GET** /repos/{owner}/{repo}/push_config | 获取仓库推送规则设置
[**getReposOwnerRepoRawPath**](RepositoriesApi.md#getReposOwnerRepoRawPath) | **GET** /repos/{owner}/{repo}/raw/{path} | 获取 raw 文件（100MB 以内）
[**getReposOwnerRepoReadme**](RepositoriesApi.md#getReposOwnerRepoReadme) | **GET** /repos/{owner}/{repo}/readme | 获取仓库README
[**getReposOwnerRepoReleases**](RepositoriesApi.md#getReposOwnerRepoReleases) | **GET** /repos/{owner}/{repo}/releases | 获取仓库的所有Releases
[**getReposOwnerRepoReleasesId**](RepositoriesApi.md#getReposOwnerRepoReleasesId) | **GET** /repos/{owner}/{repo}/releases/{id} | 获取仓库的单个Releases
[**getReposOwnerRepoReleasesLatest**](RepositoriesApi.md#getReposOwnerRepoReleasesLatest) | **GET** /repos/{owner}/{repo}/releases/latest | 获取仓库的最后更新的Release
[**getReposOwnerRepoReleasesReleaseIdAttachFiles**](RepositoriesApi.md#getReposOwnerRepoReleasesReleaseIdAttachFiles) | **GET** /repos/{owner}/{repo}/releases/{release_id}/attach_files | 获取仓库下的指定 Release 的所有附件
[**getReposOwnerRepoReleasesReleaseIdAttachFilesAttachFileId**](RepositoriesApi.md#getReposOwnerRepoReleasesReleaseIdAttachFilesAttachFileId) | **GET** /repos/{owner}/{repo}/releases/{release_id}/attach_files/{attach_file_id} | 获取仓库下指定 Release 的单个附件
[**getReposOwnerRepoReleasesReleaseIdAttachFilesAttachFileIdDownload**](RepositoriesApi.md#getReposOwnerRepoReleasesReleaseIdAttachFilesAttachFileIdDownload) | **GET** /repos/{owner}/{repo}/releases/{release_id}/attach_files/{attach_file_id}/download | 下载指定 Release 的单个附件
[**getReposOwnerRepoReleasesTagsTag**](RepositoriesApi.md#getReposOwnerRepoReleasesTagsTag) | **GET** /repos/{owner}/{repo}/releases/tags/{tag} | 根据Tag名称获取仓库的Release
[**getReposOwnerRepoTags**](RepositoriesApi.md#getReposOwnerRepoTags) | **GET** /repos/{owner}/{repo}/tags | 列出仓库所有的 tags
[**getReposOwnerRepoTarball**](RepositoriesApi.md#getReposOwnerRepoTarball) | **GET** /repos/{owner}/{repo}/tarball | 下载仓库 tar.gz
[**getReposOwnerRepoZipball**](RepositoriesApi.md#getReposOwnerRepoZipball) | **GET** /repos/{owner}/{repo}/zipball | 下载仓库 zip
[**getUserRepos**](RepositoriesApi.md#getUserRepos) | **GET** /user/repos | 列出授权用户的所有仓库
[**getUsersUsernameRepos**](RepositoriesApi.md#getUsersUsernameRepos) | **GET** /users/{username}/repos | 获取某个用户的公开仓库
[**patchReposOwnerRepo**](RepositoriesApi.md#patchReposOwnerRepo) | **PATCH** /repos/{owner}/{repo} | 更新仓库设置
[**patchReposOwnerRepoCommentsId**](RepositoriesApi.md#patchReposOwnerRepoCommentsId) | **PATCH** /repos/{owner}/{repo}/comments/{id} | 更新Commit评论
[**patchReposOwnerRepoReleasesId**](RepositoriesApi.md#patchReposOwnerRepoReleasesId) | **PATCH** /repos/{owner}/{repo}/releases/{id} | 更新仓库Release
[**postEnterprisesEnterpriseRepos**](RepositoriesApi.md#postEnterprisesEnterpriseRepos) | **POST** /enterprises/{enterprise}/repos | 创建企业仓库
[**postOrgsOrgRepos**](RepositoriesApi.md#postOrgsOrgRepos) | **POST** /orgs/{org}/repos | 创建组织仓库
[**postReposOwnerRepoBaiduStatisticKey**](RepositoriesApi.md#postReposOwnerRepoBaiduStatisticKey) | **POST** /repos/{owner}/{repo}/baidu_statistic_key | 设置/更新仓库的百度统计 key
[**postReposOwnerRepoBranches**](RepositoriesApi.md#postReposOwnerRepoBranches) | **POST** /repos/{owner}/{repo}/branches | 创建分支
[**postReposOwnerRepoCommits**](RepositoriesApi.md#postReposOwnerRepoCommits) | **POST** /repos/{owner}/{repo}/commits | 提交多个文件变更
[**postReposOwnerRepoCommitsShaComments**](RepositoriesApi.md#postReposOwnerRepoCommitsShaComments) | **POST** /repos/{owner}/{repo}/commits/{sha}/comments | 创建Commit评论
[**postReposOwnerRepoContentsPath**](RepositoriesApi.md#postReposOwnerRepoContentsPath) | **POST** /repos/{owner}/{repo}/contents/{path} | 新建文件
[**postReposOwnerRepoForks**](RepositoriesApi.md#postReposOwnerRepoForks) | **POST** /repos/{owner}/{repo}/forks | Fork一个仓库
[**postReposOwnerRepoKeys**](RepositoriesApi.md#postReposOwnerRepoKeys) | **POST** /repos/{owner}/{repo}/keys | 为仓库添加公钥
[**postReposOwnerRepoOpen**](RepositoriesApi.md#postReposOwnerRepoOpen) | **POST** /repos/{owner}/{repo}/open | 开通Gitee Go
[**postReposOwnerRepoPagesBuilds**](RepositoriesApi.md#postReposOwnerRepoPagesBuilds) | **POST** /repos/{owner}/{repo}/pages/builds | 请求建立Pages
[**postReposOwnerRepoReleases**](RepositoriesApi.md#postReposOwnerRepoReleases) | **POST** /repos/{owner}/{repo}/releases | 创建仓库Release
[**postReposOwnerRepoReleasesReleaseIdAttachFiles**](RepositoriesApi.md#postReposOwnerRepoReleasesReleaseIdAttachFiles) | **POST** /repos/{owner}/{repo}/releases/{release_id}/attach_files | 上传附件到仓库指定 Release
[**postReposOwnerRepoTags**](RepositoriesApi.md#postReposOwnerRepoTags) | **POST** /repos/{owner}/{repo}/tags | 创建一个仓库的 Tag
[**postReposOwnerRepoTrafficData**](RepositoriesApi.md#postReposOwnerRepoTrafficData) | **POST** /repos/{owner}/{repo}/traffic-data | 获取最近30天的七日以内访问量
[**postUserRepos**](RepositoriesApi.md#postUserRepos) | **POST** /user/repos | 创建一个仓库
[**putReposOwnerRepoBranchesBranchProtection**](RepositoriesApi.md#putReposOwnerRepoBranchesBranchProtection) | **PUT** /repos/{owner}/{repo}/branches/{branch}/protection | 设置分支保护
[**putReposOwnerRepoBranchesSettingNew**](RepositoriesApi.md#putReposOwnerRepoBranchesSettingNew) | **PUT** /repos/{owner}/{repo}/branches/setting/new | 新建保护分支规则
[**putReposOwnerRepoBranchesWildcardSetting**](RepositoriesApi.md#putReposOwnerRepoBranchesWildcardSetting) | **PUT** /repos/{owner}/{repo}/branches/{wildcard}/setting | 更新保护分支规则
[**putReposOwnerRepoClear**](RepositoriesApi.md#putReposOwnerRepoClear) | **PUT** /repos/{owner}/{repo}/clear | 清空一个仓库
[**putReposOwnerRepoCollaboratorsUsername**](RepositoriesApi.md#putReposOwnerRepoCollaboratorsUsername) | **PUT** /repos/{owner}/{repo}/collaborators/{username} | 添加仓库成员或更新仓库成员权限
[**putReposOwnerRepoContentsPath**](RepositoriesApi.md#putReposOwnerRepoContentsPath) | **PUT** /repos/{owner}/{repo}/contents/{path} | 更新文件
[**putReposOwnerRepoKeysEnableId**](RepositoriesApi.md#putReposOwnerRepoKeysEnableId) | **PUT** /repos/{owner}/{repo}/keys/enable/{id} | 启用仓库公钥
[**putReposOwnerRepoPages**](RepositoriesApi.md#putReposOwnerRepoPages) | **PUT** /repos/{owner}/{repo}/pages | 上传设置 Pages SSL 证书和域名
[**putReposOwnerRepoPushConfig**](RepositoriesApi.md#putReposOwnerRepoPushConfig) | **PUT** /repos/{owner}/{repo}/push_config | 修改仓库推送规则设置
[**putReposOwnerRepoReviewer**](RepositoriesApi.md#putReposOwnerRepoReviewer) | **PUT** /repos/{owner}/{repo}/reviewer | 修改代码审查设置

<a name="deleteReposOwnerRepo"></a>
# **deleteReposOwnerRepo**
> deleteReposOwnerRepo(owner, repo)

删除一个仓库

删除一个仓库

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
try {
    apiInstance.deleteReposOwnerRepo(owner, repo);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#deleteReposOwnerRepo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteReposOwnerRepoBaiduStatisticKey"></a>
# **deleteReposOwnerRepoBaiduStatisticKey**
> deleteReposOwnerRepoBaiduStatisticKey(owner, repo)

删除仓库的百度统计 key

删除仓库的百度统计 key

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
try {
    apiInstance.deleteReposOwnerRepoBaiduStatisticKey(owner, repo);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#deleteReposOwnerRepoBaiduStatisticKey");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteReposOwnerRepoBranchesBranchProtection"></a>
# **deleteReposOwnerRepoBranchesBranchProtection**
> deleteReposOwnerRepoBranchesBranchProtection(owner, repo, branch)

取消保护分支的设置

取消保护分支的设置

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String branch = "branch_example"; // String | 分支名称
try {
    apiInstance.deleteReposOwnerRepoBranchesBranchProtection(owner, repo, branch);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#deleteReposOwnerRepoBranchesBranchProtection");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **branch** | **String**| 分支名称 |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteReposOwnerRepoBranchesWildcardSetting"></a>
# **deleteReposOwnerRepoBranchesWildcardSetting**
> ProtectionRule deleteReposOwnerRepoBranchesWildcardSetting(owner, repo, wildcard)

删除保护分支规则

删除保护分支规则

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String wildcard = "wildcard_example"; // String | 分支/通配符
try {
    ProtectionRule result = apiInstance.deleteReposOwnerRepoBranchesWildcardSetting(owner, repo, wildcard);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#deleteReposOwnerRepoBranchesWildcardSetting");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **wildcard** | **String**| 分支/通配符 |

### Return type

[**ProtectionRule**](ProtectionRule.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteReposOwnerRepoCollaboratorsUsername"></a>
# **deleteReposOwnerRepoCollaboratorsUsername**
> deleteReposOwnerRepoCollaboratorsUsername(owner, repo, username)

移除仓库成员

移除仓库成员

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String username = "username_example"; // String | 用户名(username/login)
try {
    apiInstance.deleteReposOwnerRepoCollaboratorsUsername(owner, repo, username);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#deleteReposOwnerRepoCollaboratorsUsername");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **username** | **String**| 用户名(username/login) |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteReposOwnerRepoCommentsId"></a>
# **deleteReposOwnerRepoCommentsId**
> deleteReposOwnerRepoCommentsId(owner, repo, id)

删除Commit评论

删除Commit评论

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 评论的ID
try {
    apiInstance.deleteReposOwnerRepoCommentsId(owner, repo, id);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#deleteReposOwnerRepoCommentsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| 评论的ID |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteReposOwnerRepoContentsPath"></a>
# **deleteReposOwnerRepoContentsPath**
> CommitContent deleteReposOwnerRepoContentsPath(owner, repo, path, body)

删除文件

删除文件

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String path = "path_example"; // String | 文件的路径
ContentsPathBody2 body = new ContentsPathBody2(); // ContentsPathBody2 | 
try {
    CommitContent result = apiInstance.deleteReposOwnerRepoContentsPath(owner, repo, path, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#deleteReposOwnerRepoContentsPath");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **path** | **String**| 文件的路径 |
 **body** | [**ContentsPathBody2**](ContentsPathBody2.md)|  | [optional]

### Return type

[**CommitContent**](CommitContent.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/json

<a name="deleteReposOwnerRepoKeysEnableId"></a>
# **deleteReposOwnerRepoKeysEnableId**
> deleteReposOwnerRepoKeysEnableId(owner, repo, id)

停用仓库公钥

停用仓库公钥

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 公钥 ID
try {
    apiInstance.deleteReposOwnerRepoKeysEnableId(owner, repo, id);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#deleteReposOwnerRepoKeysEnableId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| 公钥 ID |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteReposOwnerRepoKeysId"></a>
# **deleteReposOwnerRepoKeysId**
> deleteReposOwnerRepoKeysId(owner, repo, id)

删除一个仓库公钥

删除一个仓库公钥

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 公钥 ID
try {
    apiInstance.deleteReposOwnerRepoKeysId(owner, repo, id);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#deleteReposOwnerRepoKeysId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| 公钥 ID |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteReposOwnerRepoReleasesId"></a>
# **deleteReposOwnerRepoReleasesId**
> deleteReposOwnerRepoReleasesId(owner, repo, id)

删除仓库Release

删除仓库Release

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 
try {
    apiInstance.deleteReposOwnerRepoReleasesId(owner, repo, id);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#deleteReposOwnerRepoReleasesId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**|  |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteReposOwnerRepoReleasesReleaseIdAttachFilesAttachFileId"></a>
# **deleteReposOwnerRepoReleasesReleaseIdAttachFilesAttachFileId**
> deleteReposOwnerRepoReleasesReleaseIdAttachFilesAttachFileId(owner, repo, releaseId, attachFileId)

删除仓库下指定 Release 的指定附件

删除仓库下指定 Release 的指定附件

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer releaseId = 56; // Integer | 
Integer attachFileId = 56; // Integer | 
try {
    apiInstance.deleteReposOwnerRepoReleasesReleaseIdAttachFilesAttachFileId(owner, repo, releaseId, attachFileId);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#deleteReposOwnerRepoReleasesReleaseIdAttachFilesAttachFileId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **releaseId** | **Integer**|  |
 **attachFileId** | **Integer**|  |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getEnterprisesEnterpriseRepos"></a>
# **getEnterprisesEnterpriseRepos**
> List&lt;Project&gt; getEnterprisesEnterpriseRepos(enterprise, search, type, direct, page, perPage)

获取企业的所有仓库

获取企业的所有仓库

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String search = "search_example"; // String | 搜索字符串
String type = "all"; // String | 筛选仓库的类型，可以是 all, public, internal, private。默认: all
Boolean direct = true; // Boolean | 只获取直属仓库，默认: false
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Project> result = apiInstance.getEnterprisesEnterpriseRepos(enterprise, search, type, direct, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getEnterprisesEnterpriseRepos");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **search** | **String**| 搜索字符串 | [optional]
 **type** | **String**| 筛选仓库的类型，可以是 all, public, internal, private。默认: all | [optional] [default to all] [enum: all, public, internal, private]
 **direct** | **Boolean**| 只获取直属仓库，默认: false | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;Project&gt;**](Project.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getOrgsOrgRepos"></a>
# **getOrgsOrgRepos**
> List&lt;Project&gt; getOrgsOrgRepos(org, type, page, perPage)

获取一个组织的仓库

获取一个组织的仓库

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String org = "org_example"; // String | 组织的路径(path/login)
String type = "all"; // String | 筛选仓库的类型，可以是 all, public, private。默认: all
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Project> result = apiInstance.getOrgsOrgRepos(org, type, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getOrgsOrgRepos");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |
 **type** | **String**| 筛选仓库的类型，可以是 all, public, private。默认: all | [optional] [default to all] [enum: all, public, private]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;Project&gt;**](Project.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepo"></a>
# **getReposOwnerRepo**
> Project getReposOwnerRepo(owner, repo)

获取用户的某个仓库

获取用户的某个仓库

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
try {
    Project result = apiInstance.getReposOwnerRepo(owner, repo);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |

### Return type

[**Project**](Project.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoBaiduStatisticKey"></a>
# **getReposOwnerRepoBaiduStatisticKey**
> getReposOwnerRepoBaiduStatisticKey(owner, repo)

获取仓库的百度统计 key

获取仓库的百度统计 key

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
try {
    apiInstance.getReposOwnerRepoBaiduStatisticKey(owner, repo);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoBaiduStatisticKey");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getReposOwnerRepoBlamePath"></a>
# **getReposOwnerRepoBlamePath**
> List&lt;Blame&gt; getReposOwnerRepoBlamePath(owner, repo, path, ref)

Blame

Blame

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String path = "path_example"; // String | 文件的路径（1 MB 以内的文件文件）
String ref = "ref_example"; // String | 分支、tag 或 commit。默认: 仓库的默认分支（通常是 master）
try {
    List<Blame> result = apiInstance.getReposOwnerRepoBlamePath(owner, repo, path, ref);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoBlamePath");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **path** | **String**| 文件的路径（1 MB 以内的文件文件） |
 **ref** | **String**| 分支、tag 或 commit。默认: 仓库的默认分支（通常是 master） | [optional]

### Return type

[**List&lt;Blame&gt;**](Blame.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoBranches"></a>
# **getReposOwnerRepoBranches**
> List&lt;Branch&gt; getReposOwnerRepoBranches(owner, repo, sort, direction, page, perPage)

获取所有分支

获取所有分支

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String sort = "name"; // String | 排序字段
String direction = "asc"; // String | 排序方向
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Branch> result = apiInstance.getReposOwnerRepoBranches(owner, repo, sort, direction, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoBranches");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **sort** | **String**| 排序字段 | [optional] [default to name] [enum: name, updated]
 **direction** | **String**| 排序方向 | [optional] [default to asc] [enum: asc, desc]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;Branch&gt;**](Branch.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoBranchesBranch"></a>
# **getReposOwnerRepoBranchesBranch**
> CompleteBranch getReposOwnerRepoBranchesBranch(owner, repo, branch)

获取单个分支

获取单个分支

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String branch = "branch_example"; // String | 分支名称
try {
    CompleteBranch result = apiInstance.getReposOwnerRepoBranchesBranch(owner, repo, branch);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoBranchesBranch");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **branch** | **String**| 分支名称 |

### Return type

[**CompleteBranch**](CompleteBranch.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoCollaborators"></a>
# **getReposOwnerRepoCollaborators**
> List&lt;ProjectMember&gt; getReposOwnerRepoCollaborators(owner, repo, page, perPage)

获取仓库的所有成员

获取仓库的所有成员

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<ProjectMember> result = apiInstance.getReposOwnerRepoCollaborators(owner, repo, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoCollaborators");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;ProjectMember&gt;**](ProjectMember.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoCollaboratorsUsername"></a>
# **getReposOwnerRepoCollaboratorsUsername**
> getReposOwnerRepoCollaboratorsUsername(owner, repo, username)

判断用户是否为仓库成员

判断用户是否为仓库成员

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String username = "username_example"; // String | 用户名(username/login)
try {
    apiInstance.getReposOwnerRepoCollaboratorsUsername(owner, repo, username);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoCollaboratorsUsername");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **username** | **String**| 用户名(username/login) |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getReposOwnerRepoCollaboratorsUsernamePermission"></a>
# **getReposOwnerRepoCollaboratorsUsernamePermission**
> ProjectMemberPermission getReposOwnerRepoCollaboratorsUsernamePermission(owner, repo, username)

查看仓库成员的权限

查看仓库成员的权限

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String username = "username_example"; // String | 用户名(username/login)
try {
    ProjectMemberPermission result = apiInstance.getReposOwnerRepoCollaboratorsUsernamePermission(owner, repo, username);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoCollaboratorsUsernamePermission");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **username** | **String**| 用户名(username/login) |

### Return type

[**ProjectMemberPermission**](ProjectMemberPermission.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoComments"></a>
# **getReposOwnerRepoComments**
> List&lt;Note&gt; getReposOwnerRepoComments(owner, repo, page, perPage, order)

获取仓库的 Commit 评论

获取仓库的 Commit 评论

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
String order = "asc"; // String | 排序顺序: asc(default),desc
try {
    List<Note> result = apiInstance.getReposOwnerRepoComments(owner, repo, page, perPage, order);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoComments");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]
 **order** | **String**| 排序顺序: asc(default),desc | [optional] [default to asc]

### Return type

[**List&lt;Note&gt;**](Note.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoCommentsId"></a>
# **getReposOwnerRepoCommentsId**
> Note getReposOwnerRepoCommentsId(owner, repo, id)

获取仓库的某条Commit评论

获取仓库的某条Commit评论

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 评论的ID
try {
    Note result = apiInstance.getReposOwnerRepoCommentsId(owner, repo, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoCommentsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| 评论的ID |

### Return type

[**Note**](Note.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoCommits"></a>
# **getReposOwnerRepoCommits**
> List&lt;RepoCommit&gt; getReposOwnerRepoCommits(owner, repo, sha, path, author, since, until, page, perPage)

仓库的所有提交

仓库的所有提交

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String sha = "sha_example"; // String | 提交起始的SHA值或者分支名. 默认: 仓库的默认分支
String path = "path_example"; // String | 包含该文件的提交
String author = "author_example"; // String | 提交作者的邮箱或个人空间地址(username/login)
String since = "since_example"; // String | 提交的起始时间，时间格式为 ISO 8601
String until = "until_example"; // String | 提交的最后时间，时间格式为 ISO 8601
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<RepoCommit> result = apiInstance.getReposOwnerRepoCommits(owner, repo, sha, path, author, since, until, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoCommits");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **sha** | **String**| 提交起始的SHA值或者分支名. 默认: 仓库的默认分支 | [optional]
 **path** | **String**| 包含该文件的提交 | [optional]
 **author** | **String**| 提交作者的邮箱或个人空间地址(username/login) | [optional]
 **since** | **String**| 提交的起始时间，时间格式为 ISO 8601 | [optional]
 **until** | **String**| 提交的最后时间，时间格式为 ISO 8601 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;RepoCommit&gt;**](RepoCommit.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoCommitsRefComments"></a>
# **getReposOwnerRepoCommitsRefComments**
> List&lt;Note&gt; getReposOwnerRepoCommitsRefComments(owner, repo, ref, page, perPage)

获取单个Commit的评论

获取单个Commit的评论

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String ref = "ref_example"; // String | Commit的Reference
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Note> result = apiInstance.getReposOwnerRepoCommitsRefComments(owner, repo, ref, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoCommitsRefComments");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **ref** | **String**| Commit的Reference |
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;Note&gt;**](Note.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoCommitsSha"></a>
# **getReposOwnerRepoCommitsSha**
> RepoCommitWithFiles getReposOwnerRepoCommitsSha(owner, repo, sha)

仓库的某个提交

仓库的某个提交

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String sha = "sha_example"; // String | 提交的SHA值或者分支名
try {
    RepoCommitWithFiles result = apiInstance.getReposOwnerRepoCommitsSha(owner, repo, sha);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoCommitsSha");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **sha** | **String**| 提交的SHA值或者分支名 |

### Return type

[**RepoCommitWithFiles**](RepoCommitWithFiles.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoCompareBaseHead"></a>
# **getReposOwnerRepoCompareBaseHead**
> Compare getReposOwnerRepoCompareBaseHead(owner, repo, base, head, straight, suffix)

Commits 对比

Commits 对比  返回的 commits 数量限制在 100 以内

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String base = "base_example"; // String | 对比的起点。Commit SHA、分支名或标签名
String head = "head_example"; // String | 对比的终点。Commit SHA、分支名或标签名
Boolean straight = true; // Boolean | 是否直对比。默认 false
String suffix = "suffix_example"; // String | 按照文件后缀过滤文件，如 `.txt`。只影响 `files`
try {
    Compare result = apiInstance.getReposOwnerRepoCompareBaseHead(owner, repo, base, head, straight, suffix);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoCompareBaseHead");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **base** | **String**| 对比的起点。Commit SHA、分支名或标签名 |
 **head** | **String**| 对比的终点。Commit SHA、分支名或标签名 |
 **straight** | **Boolean**| 是否直对比。默认 false | [optional]
 **suffix** | **String**| 按照文件后缀过滤文件，如 &#x60;.txt&#x60;。只影响 &#x60;files&#x60; | [optional]

### Return type

[**Compare**](Compare.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoContentsPath"></a>
# **getReposOwnerRepoContentsPath**
> Object getReposOwnerRepoContentsPath(owner, repo, path, ref)

获取仓库具体路径下的内容

获取仓库具体路径下的内容

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String path = "path_example"; // String | 文件的路径
String ref = "ref_example"; // String | 分支、tag或commit。默认: 仓库的默认分支(通常是master)
try {
    Object result = apiInstance.getReposOwnerRepoContentsPath(owner, repo, path, ref);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoContentsPath");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **path** | **String**| 文件的路径 |
 **ref** | **String**| 分支、tag或commit。默认: 仓库的默认分支(通常是master) | [optional]

### Return type

**Object**

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoContributors"></a>
# **getReposOwnerRepoContributors**
> List&lt;Contributor&gt; getReposOwnerRepoContributors(owner, repo, type)

获取仓库贡献者

获取仓库贡献者

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String type = "committers"; // String | 贡献者类型
try {
    List<Contributor> result = apiInstance.getReposOwnerRepoContributors(owner, repo, type);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoContributors");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **type** | **String**| 贡献者类型 | [optional] [default to committers] [enum: authors, committers]

### Return type

[**List&lt;Contributor&gt;**](Contributor.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoForks"></a>
# **getReposOwnerRepoForks**
> List&lt;Project&gt; getReposOwnerRepoForks(owner, repo, sort, page, perPage)

查看仓库的Forks

查看仓库的Forks

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String sort = "newest"; // String | 排序方式: fork的时间(newest, oldest)，star的人数(stargazers)
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Project> result = apiInstance.getReposOwnerRepoForks(owner, repo, sort, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoForks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **sort** | **String**| 排序方式: fork的时间(newest, oldest)，star的人数(stargazers) | [optional] [default to newest] [enum: newest, oldest, stargazers]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;Project&gt;**](Project.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoKeys"></a>
# **getReposOwnerRepoKeys**
> List&lt;SSHKey&gt; getReposOwnerRepoKeys(owner, repo, page, perPage)

获取仓库已部署的公钥

获取仓库已部署的公钥

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<SSHKey> result = apiInstance.getReposOwnerRepoKeys(owner, repo, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoKeys");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;SSHKey&gt;**](SSHKey.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoKeysAvailable"></a>
# **getReposOwnerRepoKeysAvailable**
> List&lt;SSHKeyBasic&gt; getReposOwnerRepoKeysAvailable(owner, repo, page, perPage)

获取仓库可部署的公钥

获取仓库可部署的公钥

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<SSHKeyBasic> result = apiInstance.getReposOwnerRepoKeysAvailable(owner, repo, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoKeysAvailable");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;SSHKeyBasic&gt;**](SSHKeyBasic.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoKeysId"></a>
# **getReposOwnerRepoKeysId**
> SSHKey getReposOwnerRepoKeysId(owner, repo, id)

获取仓库的单个公钥

获取仓库的单个公钥

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 公钥 ID
try {
    SSHKey result = apiInstance.getReposOwnerRepoKeysId(owner, repo, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoKeysId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| 公钥 ID |

### Return type

[**SSHKey**](SSHKey.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoPages"></a>
# **getReposOwnerRepoPages**
> getReposOwnerRepoPages(owner, repo)

获取Pages信息

获取Pages信息

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
try {
    apiInstance.getReposOwnerRepoPages(owner, repo);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoPages");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getReposOwnerRepoPushConfig"></a>
# **getReposOwnerRepoPushConfig**
> ProjectPushConfig getReposOwnerRepoPushConfig(owner, repo)

获取仓库推送规则设置

获取仓库推送规则设置

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
try {
    ProjectPushConfig result = apiInstance.getReposOwnerRepoPushConfig(owner, repo);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoPushConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |

### Return type

[**ProjectPushConfig**](ProjectPushConfig.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoRawPath"></a>
# **getReposOwnerRepoRawPath**
> File getReposOwnerRepoRawPath(owner, repo, path, ref)

获取 raw 文件（100MB 以内）

获取 raw 文件（100MB 以内）

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String path = "path_example"; // String | 文件的路径
String ref = "ref_example"; // String | 分支、tag 或 commit。默认: 仓库的默认分支（通常是 master）
try {
    File result = apiInstance.getReposOwnerRepoRawPath(owner, repo, path, ref);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoRawPath");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **path** | **String**| 文件的路径 |
 **ref** | **String**| 分支、tag 或 commit。默认: 仓库的默认分支（通常是 master） | [optional]

### Return type

[**File**](File.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

<a name="getReposOwnerRepoReadme"></a>
# **getReposOwnerRepoReadme**
> Content getReposOwnerRepoReadme(owner, repo, ref)

获取仓库README

获取仓库README

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String ref = "ref_example"; // String | 分支、tag或commit。默认: 仓库的默认分支(通常是master)
try {
    Content result = apiInstance.getReposOwnerRepoReadme(owner, repo, ref);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoReadme");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **ref** | **String**| 分支、tag或commit。默认: 仓库的默认分支(通常是master) | [optional]

### Return type

[**Content**](Content.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoReleases"></a>
# **getReposOwnerRepoReleases**
> List&lt;Release&gt; getReposOwnerRepoReleases(owner, repo, page, perPage, direction)

获取仓库的所有Releases

获取仓库的所有Releases

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
String direction = "direction_example"; // String | 可选。升序/降序。不填为升序
try {
    List<Release> result = apiInstance.getReposOwnerRepoReleases(owner, repo, page, perPage, direction);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoReleases");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]
 **direction** | **String**| 可选。升序/降序。不填为升序 | [optional] [enum: asc, desc]

### Return type

[**List&lt;Release&gt;**](Release.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoReleasesId"></a>
# **getReposOwnerRepoReleasesId**
> Release getReposOwnerRepoReleasesId(owner, repo, id)

获取仓库的单个Releases

获取仓库的单个Releases

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 发行版本的ID
try {
    Release result = apiInstance.getReposOwnerRepoReleasesId(owner, repo, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoReleasesId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| 发行版本的ID |

### Return type

[**Release**](Release.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoReleasesLatest"></a>
# **getReposOwnerRepoReleasesLatest**
> Release getReposOwnerRepoReleasesLatest(owner, repo)

获取仓库的最后更新的Release

获取仓库的最后更新的Release

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
try {
    Release result = apiInstance.getReposOwnerRepoReleasesLatest(owner, repo);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoReleasesLatest");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |

### Return type

[**Release**](Release.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoReleasesReleaseIdAttachFiles"></a>
# **getReposOwnerRepoReleasesReleaseIdAttachFiles**
> List&lt;AttachFile&gt; getReposOwnerRepoReleasesReleaseIdAttachFiles(owner, repo, releaseId, page, perPage, direction)

获取仓库下的指定 Release 的所有附件

获取仓库下的指定 Release 的所有附件

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer releaseId = 56; // Integer | 发行版本的ID
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
String direction = "direction_example"; // String | 可选: 升序/降序，默认为升序
try {
    List<AttachFile> result = apiInstance.getReposOwnerRepoReleasesReleaseIdAttachFiles(owner, repo, releaseId, page, perPage, direction);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoReleasesReleaseIdAttachFiles");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **releaseId** | **Integer**| 发行版本的ID |
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]
 **direction** | **String**| 可选: 升序/降序，默认为升序 | [optional] [enum: asc, desc]

### Return type

[**List&lt;AttachFile&gt;**](AttachFile.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoReleasesReleaseIdAttachFilesAttachFileId"></a>
# **getReposOwnerRepoReleasesReleaseIdAttachFilesAttachFileId**
> AttachFile getReposOwnerRepoReleasesReleaseIdAttachFilesAttachFileId(owner, repo, releaseId, attachFileId)

获取仓库下指定 Release 的单个附件

获取仓库下指定 Release 的单个附件

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer releaseId = 56; // Integer | 发行版本的ID
Integer attachFileId = 56; // Integer | 发行版本下的附件ID
try {
    AttachFile result = apiInstance.getReposOwnerRepoReleasesReleaseIdAttachFilesAttachFileId(owner, repo, releaseId, attachFileId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoReleasesReleaseIdAttachFilesAttachFileId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **releaseId** | **Integer**| 发行版本的ID |
 **attachFileId** | **Integer**| 发行版本下的附件ID |

### Return type

[**AttachFile**](AttachFile.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoReleasesReleaseIdAttachFilesAttachFileIdDownload"></a>
# **getReposOwnerRepoReleasesReleaseIdAttachFilesAttachFileIdDownload**
> File getReposOwnerRepoReleasesReleaseIdAttachFilesAttachFileIdDownload(owner, repo, releaseId, attachFileId)

下载指定 Release 的单个附件

下载指定 Release 的单个附件

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer releaseId = 56; // Integer | 发行版本的ID
Integer attachFileId = 56; // Integer | 发行版本下的附件ID
try {
    File result = apiInstance.getReposOwnerRepoReleasesReleaseIdAttachFilesAttachFileIdDownload(owner, repo, releaseId, attachFileId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoReleasesReleaseIdAttachFilesAttachFileIdDownload");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **releaseId** | **Integer**| 发行版本的ID |
 **attachFileId** | **Integer**| 发行版本下的附件ID |

### Return type

[**File**](File.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

<a name="getReposOwnerRepoReleasesTagsTag"></a>
# **getReposOwnerRepoReleasesTagsTag**
> Release getReposOwnerRepoReleasesTagsTag(owner, repo, tag)

根据Tag名称获取仓库的Release

根据Tag名称获取仓库的Release

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String tag = "tag_example"; // String | Tag 名称
try {
    Release result = apiInstance.getReposOwnerRepoReleasesTagsTag(owner, repo, tag);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoReleasesTagsTag");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **tag** | **String**| Tag 名称 |

### Return type

[**Release**](Release.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoTags"></a>
# **getReposOwnerRepoTags**
> List&lt;Tag&gt; getReposOwnerRepoTags(owner, repo, sort, direction, page, perPage)

列出仓库所有的 tags

列出仓库所有的 tags

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String sort = "name"; // String | 排序字段
String direction = "asc"; // String | 排序方向
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Tag> result = apiInstance.getReposOwnerRepoTags(owner, repo, sort, direction, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoTags");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **sort** | **String**| 排序字段 | [optional] [default to name] [enum: name, updated]
 **direction** | **String**| 排序方向 | [optional] [default to asc] [enum: asc, desc]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;Tag&gt;**](Tag.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoTarball"></a>
# **getReposOwnerRepoTarball**
> File getReposOwnerRepoTarball(owner, repo, ref)

下载仓库 tar.gz

下载仓库 tar.gz

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String ref = "ref_example"; // String | 分支、tag或commit。默认: 仓库的默认分支(通常是master)
try {
    File result = apiInstance.getReposOwnerRepoTarball(owner, repo, ref);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoTarball");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **ref** | **String**| 分支、tag或commit。默认: 仓库的默认分支(通常是master) | [optional]

### Return type

[**File**](File.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/gzip

<a name="getReposOwnerRepoZipball"></a>
# **getReposOwnerRepoZipball**
> File getReposOwnerRepoZipball(owner, repo, ref)

下载仓库 zip

下载仓库 zip

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String ref = "ref_example"; // String | 分支、tag或commit。默认: 仓库的默认分支(通常是master)
try {
    File result = apiInstance.getReposOwnerRepoZipball(owner, repo, ref);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getReposOwnerRepoZipball");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **ref** | **String**| 分支、tag或commit。默认: 仓库的默认分支(通常是master) | [optional]

### Return type

[**File**](File.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/zip

<a name="getUserRepos"></a>
# **getUserRepos**
> List&lt;Project&gt; getUserRepos(visibility, affiliation, type, sort, direction, q, page, perPage)

列出授权用户的所有仓库

列出授权用户的所有仓库

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String visibility = "visibility_example"; // String | 公开(public)、私有(private)或者所有(all)，默认: 所有(all)
String affiliation = "affiliation_example"; // String | owner(授权用户拥有的仓库)、collaborator(授权用户为仓库成员)、organization_member(授权用户为仓库所在组织并有访问仓库权限)、enterprise_member(授权用户所在企业并有访问仓库权限)、admin(所有有权限的，包括所管理的组织中所有仓库、所管理的企业的所有仓库)。                    可以用逗号分隔符组合。如: owner, organization_member 或 owner, collaborator, organization_member
String type = "type_example"; // String | 筛选用户仓库: 其创建(owner)、个人(personal)、其为成员(member)、公开(public)、私有(private)，不能与 visibility 或 affiliation 参数一并使用，否则会报 422 错误
String sort = "full_name"; // String | 排序方式: 创建时间(created)，更新时间(updated)，最后推送时间(pushed)，仓库所属与名称(full_name)。默认: full_name
String direction = "direction_example"; // String | 如果sort参数为full_name，用升序(asc)。否则降序(desc)
String q = "q_example"; // String | 搜索关键字
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Project> result = apiInstance.getUserRepos(visibility, affiliation, type, sort, direction, q, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getUserRepos");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **visibility** | **String**| 公开(public)、私有(private)或者所有(all)，默认: 所有(all) | [optional] [enum: private, public, all]
 **affiliation** | **String**| owner(授权用户拥有的仓库)、collaborator(授权用户为仓库成员)、organization_member(授权用户为仓库所在组织并有访问仓库权限)、enterprise_member(授权用户所在企业并有访问仓库权限)、admin(所有有权限的，包括所管理的组织中所有仓库、所管理的企业的所有仓库)。                    可以用逗号分隔符组合。如: owner, organization_member 或 owner, collaborator, organization_member | [optional]
 **type** | **String**| 筛选用户仓库: 其创建(owner)、个人(personal)、其为成员(member)、公开(public)、私有(private)，不能与 visibility 或 affiliation 参数一并使用，否则会报 422 错误 | [optional] [enum: all, owner, personal, member, public, private]
 **sort** | **String**| 排序方式: 创建时间(created)，更新时间(updated)，最后推送时间(pushed)，仓库所属与名称(full_name)。默认: full_name | [optional] [default to full_name] [enum: created, updated, pushed, full_name]
 **direction** | **String**| 如果sort参数为full_name，用升序(asc)。否则降序(desc) | [optional] [enum: asc, desc]
 **q** | **String**| 搜索关键字 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;Project&gt;**](Project.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUsersUsernameRepos"></a>
# **getUsersUsernameRepos**
> List&lt;Project&gt; getUsersUsernameRepos(username, type, sort, direction, page, perPage)

获取某个用户的公开仓库

获取某个用户的公开仓库

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String username = "username_example"; // String | 用户名(username/login)
String type = "all"; // String | 用户创建的仓库(owner)，用户个人仓库(personal)，用户为仓库成员(member)，所有(all)。默认: 所有(all)
String sort = "full_name"; // String | 排序方式: 创建时间(created)，更新时间(updated)，最后推送时间(pushed)，仓库所属与名称(full_name)。默认: full_name
String direction = "direction_example"; // String | 如果sort参数为full_name，用升序(asc)。否则降序(desc)
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Project> result = apiInstance.getUsersUsernameRepos(username, type, sort, direction, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getUsersUsernameRepos");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **type** | **String**| 用户创建的仓库(owner)，用户个人仓库(personal)，用户为仓库成员(member)，所有(all)。默认: 所有(all) | [optional] [default to all] [enum: all, owner, personal, member]
 **sort** | **String**| 排序方式: 创建时间(created)，更新时间(updated)，最后推送时间(pushed)，仓库所属与名称(full_name)。默认: full_name | [optional] [default to full_name] [enum: created, updated, pushed, full_name]
 **direction** | **String**| 如果sort参数为full_name，用升序(asc)。否则降序(desc) | [optional] [enum: asc, desc]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;Project&gt;**](Project.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="patchReposOwnerRepo"></a>
# **patchReposOwnerRepo**
> Project patchReposOwnerRepo(owner, repo, body)

更新仓库设置

更新仓库设置

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
OwnerRepoBody body = new OwnerRepoBody(); // OwnerRepoBody | 
try {
    Project result = apiInstance.patchReposOwnerRepo(owner, repo, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#patchReposOwnerRepo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **body** | [**OwnerRepoBody**](OwnerRepoBody.md)|  | [optional]

### Return type

[**Project**](Project.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/json

<a name="patchReposOwnerRepoCommentsId"></a>
# **patchReposOwnerRepoCommentsId**
> Note patchReposOwnerRepoCommentsId(owner, repo, id, body)

更新Commit评论

更新Commit评论

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 评论的ID
CommentBody body = new CommentBody(); // CommentBody | 
try {
    Note result = apiInstance.patchReposOwnerRepoCommentsId(owner, repo, id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#patchReposOwnerRepoCommentsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| 评论的ID |
 **body** | [**CommentBody**](CommentBody.md)|  | [optional]

### Return type

[**Note**](Note.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/json

<a name="patchReposOwnerRepoReleasesId"></a>
# **patchReposOwnerRepoReleasesId**
> Release patchReposOwnerRepoReleasesId(owner, repo, id, body)

更新仓库Release

更新仓库Release

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 
ReleasesIdBody body = new ReleasesIdBody(); // ReleasesIdBody | 
try {
    Release result = apiInstance.patchReposOwnerRepoReleasesId(owner, repo, id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#patchReposOwnerRepoReleasesId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**|  |
 **body** | [**ReleasesIdBody**](ReleasesIdBody.md)|  | [optional]

### Return type

[**Release**](Release.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/json

<a name="postEnterprisesEnterpriseRepos"></a>
# **postEnterprisesEnterpriseRepos**
> Project postEnterprisesEnterpriseRepos(enterprise, body)

创建企业仓库

创建企业仓库

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
EnterpriseReposBody body = new EnterpriseReposBody(); // EnterpriseReposBody | 
try {
    Project result = apiInstance.postEnterprisesEnterpriseRepos(enterprise, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#postEnterprisesEnterpriseRepos");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **body** | [**EnterpriseReposBody**](EnterpriseReposBody.md)|  | [optional]

### Return type

[**Project**](Project.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postOrgsOrgRepos"></a>
# **postOrgsOrgRepos**
> Project postOrgsOrgRepos(org, body)

创建组织仓库

创建组织仓库

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String org = "org_example"; // String | 组织的路径(path/login)
OrgReposBody body = new OrgReposBody(); // OrgReposBody | 
try {
    Project result = apiInstance.postOrgsOrgRepos(org, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#postOrgsOrgRepos");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |
 **body** | [**OrgReposBody**](OrgReposBody.md)|  | [optional]

### Return type

[**Project**](Project.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postReposOwnerRepoBaiduStatisticKey"></a>
# **postReposOwnerRepoBaiduStatisticKey**
> postReposOwnerRepoBaiduStatisticKey(owner, repo, body)

设置/更新仓库的百度统计 key

设置/更新仓库的百度统计 key

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
RepoBaiduStatisticKeyBody body = new RepoBaiduStatisticKeyBody(); // RepoBaiduStatisticKeyBody | 
try {
    apiInstance.postReposOwnerRepoBaiduStatisticKey(owner, repo, body);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#postReposOwnerRepoBaiduStatisticKey");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **body** | [**RepoBaiduStatisticKeyBody**](RepoBaiduStatisticKeyBody.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="postReposOwnerRepoBranches"></a>
# **postReposOwnerRepoBranches**
> CompleteBranch postReposOwnerRepoBranches(owner, repo, body)

创建分支

创建分支

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
RepoBranchesBody body = new RepoBranchesBody(); // RepoBranchesBody | 
try {
    CompleteBranch result = apiInstance.postReposOwnerRepoBranches(owner, repo, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#postReposOwnerRepoBranches");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **body** | [**RepoBranchesBody**](RepoBranchesBody.md)|  | [optional]

### Return type

[**CompleteBranch**](CompleteBranch.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postReposOwnerRepoCommits"></a>
# **postReposOwnerRepoCommits**
> RepoCommitWithFiles postReposOwnerRepoCommits(owner, repo, body)

提交多个文件变更

提交多个文件变更

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
RepoCommitsBody body = new RepoCommitsBody(); // RepoCommitsBody | 
try {
    RepoCommitWithFiles result = apiInstance.postReposOwnerRepoCommits(owner, repo, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#postReposOwnerRepoCommits");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **body** | [**RepoCommitsBody**](RepoCommitsBody.md)|  | [optional]

### Return type

[**RepoCommitWithFiles**](RepoCommitWithFiles.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postReposOwnerRepoCommitsShaComments"></a>
# **postReposOwnerRepoCommitsShaComments**
> Note postReposOwnerRepoCommitsShaComments(owner, repo, sha, body)

创建Commit评论

创建Commit评论

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String sha = "sha_example"; // String | 评论的sha值
ShaCommentsBody body = new ShaCommentsBody(); // ShaCommentsBody | 
try {
    Note result = apiInstance.postReposOwnerRepoCommitsShaComments(owner, repo, sha, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#postReposOwnerRepoCommitsShaComments");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **sha** | **String**| 评论的sha值 |
 **body** | [**ShaCommentsBody**](ShaCommentsBody.md)|  | [optional]

### Return type

[**Note**](Note.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postReposOwnerRepoContentsPath"></a>
# **postReposOwnerRepoContentsPath**
> CommitContent postReposOwnerRepoContentsPath(owner, repo, path, body)

新建文件

新建文件

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String path = "path_example"; // String | 文件的路径
ContentsPathBody1 body = new ContentsPathBody1(); // ContentsPathBody1 | 
try {
    CommitContent result = apiInstance.postReposOwnerRepoContentsPath(owner, repo, path, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#postReposOwnerRepoContentsPath");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **path** | **String**| 文件的路径 |
 **body** | [**ContentsPathBody1**](ContentsPathBody1.md)|  | [optional]

### Return type

[**CommitContent**](CommitContent.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postReposOwnerRepoForks"></a>
# **postReposOwnerRepoForks**
> Project postReposOwnerRepoForks(owner, repo, body)

Fork一个仓库

Fork一个仓库

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
RepoForksBody body = new RepoForksBody(); // RepoForksBody | 
try {
    Project result = apiInstance.postReposOwnerRepoForks(owner, repo, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#postReposOwnerRepoForks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **body** | [**RepoForksBody**](RepoForksBody.md)|  | [optional]

### Return type

[**Project**](Project.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postReposOwnerRepoKeys"></a>
# **postReposOwnerRepoKeys**
> SSHKey postReposOwnerRepoKeys(owner, repo, body)

为仓库添加公钥

为仓库添加公钥

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
RepoKeysBody body = new RepoKeysBody(); // RepoKeysBody | 
try {
    SSHKey result = apiInstance.postReposOwnerRepoKeys(owner, repo, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#postReposOwnerRepoKeys");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **body** | [**RepoKeysBody**](RepoKeysBody.md)|  | [optional]

### Return type

[**SSHKey**](SSHKey.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postReposOwnerRepoOpen"></a>
# **postReposOwnerRepoOpen**
> postReposOwnerRepoOpen(owner, repo)

开通Gitee Go

开通Gitee Go

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库path
try {
    apiInstance.postReposOwnerRepoOpen(owner, repo);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#postReposOwnerRepoOpen");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库path |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="postReposOwnerRepoPagesBuilds"></a>
# **postReposOwnerRepoPagesBuilds**
> postReposOwnerRepoPagesBuilds(owner, repo)

请求建立Pages

请求建立Pages

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
try {
    apiInstance.postReposOwnerRepoPagesBuilds(owner, repo);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#postReposOwnerRepoPagesBuilds");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="postReposOwnerRepoReleases"></a>
# **postReposOwnerRepoReleases**
> Release postReposOwnerRepoReleases(owner, repo, body)

创建仓库Release

创建仓库Release

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
RepoReleasesBody body = new RepoReleasesBody(); // RepoReleasesBody | 
try {
    Release result = apiInstance.postReposOwnerRepoReleases(owner, repo, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#postReposOwnerRepoReleases");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **body** | [**RepoReleasesBody**](RepoReleasesBody.md)|  | [optional]

### Return type

[**Release**](Release.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postReposOwnerRepoReleasesReleaseIdAttachFiles"></a>
# **postReposOwnerRepoReleasesReleaseIdAttachFiles**
> AttachFile postReposOwnerRepoReleasesReleaseIdAttachFiles(owner, repo, releaseId, file)

上传附件到仓库指定 Release

上传附件到仓库指定 Release

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer releaseId = 56; // Integer | 发行版本的ID
File file = new File("file_example"); // File | 
try {
    AttachFile result = apiInstance.postReposOwnerRepoReleasesReleaseIdAttachFiles(owner, repo, releaseId, file);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#postReposOwnerRepoReleasesReleaseIdAttachFiles");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **releaseId** | **Integer**| 发行版本的ID |
 **file** | **File**|  | [optional]

### Return type

[**AttachFile**](AttachFile.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="postReposOwnerRepoTags"></a>
# **postReposOwnerRepoTags**
> Tag postReposOwnerRepoTags(owner, repo, body)

创建一个仓库的 Tag

创建一个仓库的 Tag

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
RepoTagsBody body = new RepoTagsBody(); // RepoTagsBody | 
try {
    Tag result = apiInstance.postReposOwnerRepoTags(owner, repo, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#postReposOwnerRepoTags");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **body** | [**RepoTagsBody**](RepoTagsBody.md)|  | [optional]

### Return type

[**Tag**](Tag.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postReposOwnerRepoTrafficData"></a>
# **postReposOwnerRepoTrafficData**
> ProjectTrafficData postReposOwnerRepoTrafficData(owner, repo, body)

获取最近30天的七日以内访问量

获取最近30天的七日以内访问量

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
RepoTrafficdataBody body = new RepoTrafficdataBody(); // RepoTrafficdataBody | 
try {
    ProjectTrafficData result = apiInstance.postReposOwnerRepoTrafficData(owner, repo, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#postReposOwnerRepoTrafficData");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **body** | [**RepoTrafficdataBody**](RepoTrafficdataBody.md)|  | [optional]

### Return type

[**ProjectTrafficData**](ProjectTrafficData.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postUserRepos"></a>
# **postUserRepos**
> Project postUserRepos(body)

创建一个仓库

创建一个仓库

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
UserReposBody body = new UserReposBody(); // UserReposBody | 
try {
    Project result = apiInstance.postUserRepos(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#postUserRepos");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UserReposBody**](UserReposBody.md)|  | [optional]

### Return type

[**Project**](Project.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putReposOwnerRepoBranchesBranchProtection"></a>
# **putReposOwnerRepoBranchesBranchProtection**
> CompleteBranch putReposOwnerRepoBranchesBranchProtection(owner, repo, branch)

设置分支保护

设置分支保护

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String branch = "branch_example"; // String | 分支名称
try {
    CompleteBranch result = apiInstance.putReposOwnerRepoBranchesBranchProtection(owner, repo, branch);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#putReposOwnerRepoBranchesBranchProtection");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **branch** | **String**| 分支名称 |

### Return type

[**CompleteBranch**](CompleteBranch.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="putReposOwnerRepoBranchesSettingNew"></a>
# **putReposOwnerRepoBranchesSettingNew**
> ProtectionRule putReposOwnerRepoBranchesSettingNew(owner, repo, body)

新建保护分支规则

新建保护分支规则

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
SettingNewBody body = new SettingNewBody(); // SettingNewBody | 
try {
    ProtectionRule result = apiInstance.putReposOwnerRepoBranchesSettingNew(owner, repo, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#putReposOwnerRepoBranchesSettingNew");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **body** | [**SettingNewBody**](SettingNewBody.md)|  | [optional]

### Return type

[**ProtectionRule**](ProtectionRule.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putReposOwnerRepoBranchesWildcardSetting"></a>
# **putReposOwnerRepoBranchesWildcardSetting**
> ProtectionRule putReposOwnerRepoBranchesWildcardSetting(owner, repo, wildcard, body)

更新保护分支规则

更新保护分支规则

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String wildcard = "wildcard_example"; // String | 分支/通配符
WildcardSettingBody body = new WildcardSettingBody(); // WildcardSettingBody | 
try {
    ProtectionRule result = apiInstance.putReposOwnerRepoBranchesWildcardSetting(owner, repo, wildcard, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#putReposOwnerRepoBranchesWildcardSetting");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **wildcard** | **String**| 分支/通配符 |
 **body** | [**WildcardSettingBody**](WildcardSettingBody.md)|  | [optional]

### Return type

[**ProtectionRule**](ProtectionRule.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putReposOwnerRepoClear"></a>
# **putReposOwnerRepoClear**
> putReposOwnerRepoClear(owner, repo)

清空一个仓库

清空一个仓库

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
try {
    apiInstance.putReposOwnerRepoClear(owner, repo);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#putReposOwnerRepoClear");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="putReposOwnerRepoCollaboratorsUsername"></a>
# **putReposOwnerRepoCollaboratorsUsername**
> ProjectMember putReposOwnerRepoCollaboratorsUsername(owner, repo, username, body)

添加仓库成员或更新仓库成员权限

添加仓库成员或更新仓库成员权限

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String username = "username_example"; // String | 用户名(username/login)
CollaboratorsUsernameBody body = new CollaboratorsUsernameBody(); // CollaboratorsUsernameBody | 
try {
    ProjectMember result = apiInstance.putReposOwnerRepoCollaboratorsUsername(owner, repo, username, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#putReposOwnerRepoCollaboratorsUsername");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **username** | **String**| 用户名(username/login) |
 **body** | [**CollaboratorsUsernameBody**](CollaboratorsUsernameBody.md)|  | [optional]

### Return type

[**ProjectMember**](ProjectMember.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putReposOwnerRepoContentsPath"></a>
# **putReposOwnerRepoContentsPath**
> CommitContent putReposOwnerRepoContentsPath(owner, repo, path, body)

更新文件

更新文件

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String path = "path_example"; // String | 文件的路径
ContentsPathBody body = new ContentsPathBody(); // ContentsPathBody | 
try {
    CommitContent result = apiInstance.putReposOwnerRepoContentsPath(owner, repo, path, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#putReposOwnerRepoContentsPath");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **path** | **String**| 文件的路径 |
 **body** | [**ContentsPathBody**](ContentsPathBody.md)|  | [optional]

### Return type

[**CommitContent**](CommitContent.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putReposOwnerRepoKeysEnableId"></a>
# **putReposOwnerRepoKeysEnableId**
> putReposOwnerRepoKeysEnableId(owner, repo, id)

启用仓库公钥

启用仓库公钥

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 公钥 ID
try {
    apiInstance.putReposOwnerRepoKeysEnableId(owner, repo, id);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#putReposOwnerRepoKeysEnableId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| 公钥 ID |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="putReposOwnerRepoPages"></a>
# **putReposOwnerRepoPages**
> putReposOwnerRepoPages(owner, repo, body)

上传设置 Pages SSL 证书和域名

上传设置 Pages SSL 证书和域名

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
RepoPagesBody body = new RepoPagesBody(); // RepoPagesBody | 
try {
    apiInstance.putReposOwnerRepoPages(owner, repo, body);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#putReposOwnerRepoPages");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **body** | [**RepoPagesBody**](RepoPagesBody.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="putReposOwnerRepoPushConfig"></a>
# **putReposOwnerRepoPushConfig**
> ProjectPushConfig putReposOwnerRepoPushConfig(owner, repo, body)

修改仓库推送规则设置

修改仓库推送规则设置

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
RepoPushConfigBody body = new RepoPushConfigBody(); // RepoPushConfigBody | 
try {
    ProjectPushConfig result = apiInstance.putReposOwnerRepoPushConfig(owner, repo, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#putReposOwnerRepoPushConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **body** | [**RepoPushConfigBody**](RepoPushConfigBody.md)|  | [optional]

### Return type

[**ProjectPushConfig**](ProjectPushConfig.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putReposOwnerRepoReviewer"></a>
# **putReposOwnerRepoReviewer**
> Project putReposOwnerRepoReviewer(owner, repo, body)

修改代码审查设置

修改代码审查设置

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.RepositoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
RepoReviewerBody body = new RepoReviewerBody(); // RepoReviewerBody | 
try {
    Project result = apiInstance.putReposOwnerRepoReviewer(owner, repo, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#putReposOwnerRepoReviewer");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **body** | [**RepoReviewerBody**](RepoReviewerBody.md)|  | [optional]

### Return type

[**Project**](Project.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

