# SSHKey

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**id** | **Integer** |  |  [optional]
**key** | **String** |  |  [optional]
**title** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
