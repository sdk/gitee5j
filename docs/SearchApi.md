# SearchApi

All URIs are relative to *https://gitee.com/api/v5*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getSearchIssues**](SearchApi.md#getSearchIssues) | **GET** /search/issues | 搜索 Issues
[**getSearchRepositories**](SearchApi.md#getSearchRepositories) | **GET** /search/repositories | 搜索仓库
[**getSearchUsers**](SearchApi.md#getSearchUsers) | **GET** /search/users | 搜索用户

<a name="getSearchIssues"></a>
# **getSearchIssues**
> List&lt;Issue&gt; getSearchIssues(q, page, perPage, repo, language, label, state, author, assignee, sort, order)

搜索 Issues

搜索 Issues

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.SearchApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

SearchApi apiInstance = new SearchApi();
String q = "q_example"; // String | 搜索关键字
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
String repo = "repo_example"; // String | 筛选指定仓库 (path, e.g. oschina/git-osc) 的 issues
String language = "language_example"; // String | 筛选指定语言的 issues
String label = "label_example"; // String | 筛选指定标签的 issues
String state = "state_example"; // String | 筛选指定状态的 issues, open(开启)、closed(完成)、rejected(拒绝)
String author = "author_example"; // String | 筛选指定创建者 (username/login) 的 issues
String assignee = "assignee_example"; // String | 筛选指定负责人 (username/login) 的 issues
String sort = "sort_example"; // String | 排序字段，created_at(创建时间)、last_push_at(更新时间)、notes_count(评论数)，默认为最佳匹配
String order = "desc"; // String | 排序顺序: desc(default)、asc
try {
    List<Issue> result = apiInstance.getSearchIssues(q, page, perPage, repo, language, label, state, author, assignee, sort, order);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SearchApi#getSearchIssues");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **String**| 搜索关键字 |
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]
 **repo** | **String**| 筛选指定仓库 (path, e.g. oschina/git-osc) 的 issues | [optional]
 **language** | **String**| 筛选指定语言的 issues | [optional] [enum: ruby, Java, JavaScript, HTML, PHP, CSS, QML, C, Dart, Go, FORTRAN, AutoHotkey, PowerShell, Common Lisp, D, VHDL, Scheme, Objective-C, Shell, Haskell, XSLT, M, C++, Julia, Elixir, DOT, TypeScript, Puppet, C#, Racket, OCaml, Pascal, Haxe, LiveScript, Android, Standard ML, Coq, Awk, eC, Scilab, Prolog, Smalltalk, Vala, Ada, ColdFusion, Slash, Nemerle, Swift, Eiffel, Docker, NodeJS, Perl, Rust, Lua, CoffeeScript, Scala, Matlab, Assembly, Groovy, Clojure, Visual Basic, VimL, ASP, Erlang, Arduino, Verilog, Delphi, Emacs Lisp, TeX/LaTeX, ActionScript, R, WeChat, Crystal, Kotlin, SQL, Lisp, XML, C/C++, HTML/CSS, E language, assembly language, 其他, test, GVPgranm, moofy]
 **label** | **String**| 筛选指定标签的 issues | [optional]
 **state** | **String**| 筛选指定状态的 issues, open(开启)、closed(完成)、rejected(拒绝) | [optional] [enum: open, progressing, closed, rejected]
 **author** | **String**| 筛选指定创建者 (username/login) 的 issues | [optional]
 **assignee** | **String**| 筛选指定负责人 (username/login) 的 issues | [optional]
 **sort** | **String**| 排序字段，created_at(创建时间)、last_push_at(更新时间)、notes_count(评论数)，默认为最佳匹配 | [optional] [enum: created_at, updated_at, notes_count]
 **order** | **String**| 排序顺序: desc(default)、asc | [optional] [default to desc] [enum: asc, desc]

### Return type

[**List&lt;Issue&gt;**](Issue.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getSearchRepositories"></a>
# **getSearchRepositories**
> List&lt;Project&gt; getSearchRepositories(q, page, perPage, owner, fork, language, sort, order)

搜索仓库

搜索仓库

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.SearchApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

SearchApi apiInstance = new SearchApi();
String q = "q_example"; // String | 搜索关键字
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
String owner = "owner_example"; // String | 筛选指定空间地址(企业、组织或个人的地址 path) 的仓库
Boolean fork = true; // Boolean | 是否搜索含 fork 的仓库，默认：否
String language = "language_example"; // String | 筛选指定语言的仓库
String sort = "sort_example"; // String | 排序字段，last_push_at(更新时间)、stars_count(收藏数)、forks_count(Fork 数)、watches_count(关注数)，默认为最佳匹配
String order = "desc"; // String | 排序顺序: desc(default)、asc
try {
    List<Project> result = apiInstance.getSearchRepositories(q, page, perPage, owner, fork, language, sort, order);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SearchApi#getSearchRepositories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **String**| 搜索关键字 |
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]
 **owner** | **String**| 筛选指定空间地址(企业、组织或个人的地址 path) 的仓库 | [optional]
 **fork** | **Boolean**| 是否搜索含 fork 的仓库，默认：否 | [optional]
 **language** | **String**| 筛选指定语言的仓库 | [optional] [enum: ruby, Java, JavaScript, HTML, PHP, CSS, QML, C, Dart, Go, FORTRAN, AutoHotkey, PowerShell, Common Lisp, D, VHDL, Scheme, Objective-C, Shell, Haskell, XSLT, M, C++, Julia, Elixir, DOT, TypeScript, Puppet, C#, Racket, OCaml, Pascal, Haxe, LiveScript, Android, Standard ML, Coq, Awk, eC, Scilab, Prolog, Smalltalk, Vala, Ada, ColdFusion, Slash, Nemerle, Swift, Eiffel, Docker, NodeJS, Perl, Rust, Lua, CoffeeScript, Scala, Matlab, Assembly, Groovy, Clojure, Visual Basic, VimL, ASP, Erlang, Arduino, Verilog, Delphi, Emacs Lisp, TeX/LaTeX, ActionScript, R, WeChat, Crystal, Kotlin, SQL, Lisp, XML, C/C++, HTML/CSS, E language, assembly language, 其他, test, GVPgranm, moofy]
 **sort** | **String**| 排序字段，last_push_at(更新时间)、stars_count(收藏数)、forks_count(Fork 数)、watches_count(关注数)，默认为最佳匹配 | [optional] [enum: last_push_at, stars_count, forks_count, watches_count]
 **order** | **String**| 排序顺序: desc(default)、asc | [optional] [default to desc] [enum: asc, desc]

### Return type

[**List&lt;Project&gt;**](Project.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getSearchUsers"></a>
# **getSearchUsers**
> List&lt;User&gt; getSearchUsers(q, page, perPage, sort, order)

搜索用户

搜索用户

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.SearchApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

SearchApi apiInstance = new SearchApi();
String q = "q_example"; // String | 搜索关键字
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
String sort = "sort_example"; // String | 排序字段，joined_at(注册时间)，默认为最佳匹配
String order = "desc"; // String | 排序顺序: desc(default)、asc
try {
    List<User> result = apiInstance.getSearchUsers(q, page, perPage, sort, order);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SearchApi#getSearchUsers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **String**| 搜索关键字 |
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]
 **sort** | **String**| 排序字段，joined_at(注册时间)，默认为最佳匹配 | [optional] [enum: joined_at]
 **order** | **String**| 排序顺序: desc(default)、asc | [optional] [default to desc] [enum: asc, desc]

### Return type

[**List&lt;User&gt;**](User.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

