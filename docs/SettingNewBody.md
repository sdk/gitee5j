# SettingNewBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**escapseProtectBranchList** | **List&lt;String&gt;** | 不受规则影响的常规分支列表，以英文逗号分隔，形如：[&#x27;a&#x27;, &#x27;b&#x27;] |  [optional]
**merger** | **String** | 可合并 Pull Request 成员。developer：仓库管理员和开发者；admin：仓库管理员；none：禁止任何人合并; 用户：个人的地址 path（多个用户用 &#x27;;&#x27; 隔开） | 
**mode** | [**ModeEnum**](#ModeEnum) | 模式。standard: 标准模式, review: 评审模式 | 
**pusher** | **String** | 可推送代码成员。developer：仓库管理员和开发者；admin：仓库管理员；none：禁止任何人合并; 用户：个人的地址 path（多个用户用 &#x27;;&#x27; 隔开） | 
**wildcard** | **String** | 分支/通配符 | 

<a name="ModeEnum"></a>
## Enum: ModeEnum
Name | Value
---- | -----
STANDARD | &quot;standard&quot;
REVIEW | &quot;review&quot;
