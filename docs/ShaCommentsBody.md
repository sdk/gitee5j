# ShaCommentsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**body** | **String** | 评论的内容 | 
**path** | **String** | 文件的相对路径 |  [optional]
**position** | **Integer** | Diff的相对行数 |  [optional]
