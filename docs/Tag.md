# Tag

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**commit** | [**TagCommit**](TagCommit.md) |  |  [optional]
**message** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**tagger** | [**GitUser**](GitUser.md) |  |  [optional]
