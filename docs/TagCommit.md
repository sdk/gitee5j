# TagCommit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**sha** | **String** |  |  [optional]
