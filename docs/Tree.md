# Tree

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sha** | **String** |  |  [optional]
**tree** | [**List&lt;TreeEntry&gt;**](TreeEntry.md) |  |  [optional]
**truncated** | **Boolean** |  |  [optional]
**url** | **String** |  |  [optional]
