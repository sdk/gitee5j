# TreeEntry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mode** | **String** |  |  [optional]
**path** | **String** |  |  [optional]
**sha** | **String** |  |  [optional]
**type** | **String** |  |  [optional]
