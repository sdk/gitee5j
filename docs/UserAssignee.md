# UserAssignee

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accept** | **Boolean** | 是否审查通过 |  [optional]
**assignee** | **Boolean** | 是否默认指派审查 |  [optional]
**avatarUrl** | **String** |  |  [optional]
**codeOwner** | **Boolean** | 是否CodeOwner指派审查 |  [optional]
**eventsUrl** | **String** |  |  [optional]
**followersUrl** | **String** |  |  [optional]
**followingUrl** | **String** |  |  [optional]
**gistsUrl** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**id** | **Integer** |  |  [optional]
**login** | **String** |  |  [optional]
**memberRole** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**organizationsUrl** | **String** |  |  [optional]
**receivedEventsUrl** | **String** |  |  [optional]
**remark** | **String** | 企业备注名 |  [optional]
**reposUrl** | **String** |  |  [optional]
**starredUrl** | **String** |  |  [optional]
**subscriptionsUrl** | **String** |  |  [optional]
**type** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
