# UserBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bio** | **String** | 自我介绍 |  [optional]
**blog** | **String** | 微博链接 |  [optional]
**name** | **String** | 昵称 |  [optional]
**weibo** | **String** | 博客站点 |  [optional]
