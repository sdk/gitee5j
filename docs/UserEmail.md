# UserEmail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** |  |  [optional]
**scope** | **List&lt;String&gt;** |  |  [optional]
**state** | **String** |  |  [optional]
