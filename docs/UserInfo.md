# UserInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**avatarUrl** | **String** |  |  [optional]
**bio** | **String** |  |  [optional]
**blog** | **String** |  |  [optional]
**company** | **String** |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**email** | **String** |  |  [optional]
**eventsUrl** | **String** |  |  [optional]
**followers** | **Integer** |  |  [optional]
**followersUrl** | **String** |  |  [optional]
**following** | **Integer** |  |  [optional]
**followingUrl** | **String** |  |  [optional]
**gistsUrl** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**id** | **Integer** |  |  [optional]
**linkedin** | **String** |  |  [optional]
**login** | **String** |  |  [optional]
**memberRole** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**organizationsUrl** | **String** |  |  [optional]
**profession** | **String** |  |  [optional]
**publicGists** | **Integer** |  |  [optional]
**publicRepos** | **Integer** |  |  [optional]
**qq** | **String** |  |  [optional]
**receivedEventsUrl** | **String** |  |  [optional]
**remark** | **String** | 企业备注名 |  [optional]
**reposUrl** | **String** |  |  [optional]
**stared** | **Integer** |  |  [optional]
**starredUrl** | **String** |  |  [optional]
**subscriptionsUrl** | **String** |  |  [optional]
**type** | **String** |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**url** | **String** |  |  [optional]
**watched** | **Integer** |  |  [optional]
**wechat** | **String** |  |  [optional]
**weibo** | **String** |  |  [optional]
