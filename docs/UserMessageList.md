# UserMessageList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**list** | [**List&lt;UserMessage&gt;**](UserMessage.md) | 私信列表 |  [optional]
**totalCount** | **Integer** |  |  [optional]
