# UserMini

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**avatarUrl** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**id** | **Integer** |  |  [optional]
**login** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**remark** | **String** | 企业备注名 |  [optional]
**url** | **String** |  |  [optional]
