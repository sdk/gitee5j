# UserNotification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**actor** | [**UserBasic**](UserBasic.md) |  |  [optional]
**content** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**id** | **Integer** |  |  [optional]
**mute** | **Boolean** |  |  [optional]
**namespaces** | [**List&lt;UserNotificationNamespace&gt;**](UserNotificationNamespace.md) | 通知次级关联对象 |  [optional]
**repository** | [**ProjectBasic**](ProjectBasic.md) |  |  [optional]
**subject** | [**UserNotificationSubject**](UserNotificationSubject.md) |  |  [optional]
**type** | **String** |  |  [optional]
**unread** | **Boolean** |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**url** | **String** |  |  [optional]
