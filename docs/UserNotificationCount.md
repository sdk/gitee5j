# UserNotificationCount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**messageCount** | **Integer** | 私信数量 |  [optional]
**notificationCount** | **Integer** | 通知数量 |  [optional]
**totalCount** | **Integer** | 通知总数 |  [optional]
