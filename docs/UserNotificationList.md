# UserNotificationList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**list** | [**List&lt;UserNotification&gt;**](UserNotification.md) | 通知列表 |  [optional]
**totalCount** | **Integer** |  |  [optional]
