# UserNotificationNamespace

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**htmlUrl** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**type** | **String** |  |  [optional]
