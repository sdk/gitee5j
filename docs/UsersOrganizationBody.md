# UsersOrganizationBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **String** | 组织描述 |  [optional]
**name** | **String** | 组织名称 | 
**org** | **String** | 组织的路径(path/login) | 
