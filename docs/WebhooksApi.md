# WebhooksApi

All URIs are relative to *https://gitee.com/api/v5*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteReposOwnerRepoHooksId**](WebhooksApi.md#deleteReposOwnerRepoHooksId) | **DELETE** /repos/{owner}/{repo}/hooks/{id} | 删除一个仓库WebHook
[**getReposOwnerRepoHooks**](WebhooksApi.md#getReposOwnerRepoHooks) | **GET** /repos/{owner}/{repo}/hooks | 列出仓库的WebHooks
[**getReposOwnerRepoHooksId**](WebhooksApi.md#getReposOwnerRepoHooksId) | **GET** /repos/{owner}/{repo}/hooks/{id} | 获取仓库单个WebHook
[**patchReposOwnerRepoHooksId**](WebhooksApi.md#patchReposOwnerRepoHooksId) | **PATCH** /repos/{owner}/{repo}/hooks/{id} | 更新一个仓库WebHook
[**postReposOwnerRepoHooks**](WebhooksApi.md#postReposOwnerRepoHooks) | **POST** /repos/{owner}/{repo}/hooks | 创建一个仓库WebHook
[**postReposOwnerRepoHooksIdTests**](WebhooksApi.md#postReposOwnerRepoHooksIdTests) | **POST** /repos/{owner}/{repo}/hooks/{id}/tests | 测试WebHook是否发送成功

<a name="deleteReposOwnerRepoHooksId"></a>
# **deleteReposOwnerRepoHooksId**
> deleteReposOwnerRepoHooksId(owner, repo, id)

删除一个仓库WebHook

删除一个仓库WebHook

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.WebhooksApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

WebhooksApi apiInstance = new WebhooksApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | Webhook的ID
try {
    apiInstance.deleteReposOwnerRepoHooksId(owner, repo, id);
} catch (ApiException e) {
    System.err.println("Exception when calling WebhooksApi#deleteReposOwnerRepoHooksId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| Webhook的ID |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getReposOwnerRepoHooks"></a>
# **getReposOwnerRepoHooks**
> List&lt;Hook&gt; getReposOwnerRepoHooks(owner, repo, page, perPage)

列出仓库的WebHooks

列出仓库的WebHooks

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.WebhooksApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

WebhooksApi apiInstance = new WebhooksApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Hook> result = apiInstance.getReposOwnerRepoHooks(owner, repo, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WebhooksApi#getReposOwnerRepoHooks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20] [enum: 1, 100]

### Return type

[**List&lt;Hook&gt;**](Hook.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReposOwnerRepoHooksId"></a>
# **getReposOwnerRepoHooksId**
> Hook getReposOwnerRepoHooksId(owner, repo, id)

获取仓库单个WebHook

获取仓库单个WebHook

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.WebhooksApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

WebhooksApi apiInstance = new WebhooksApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | Webhook的ID
try {
    Hook result = apiInstance.getReposOwnerRepoHooksId(owner, repo, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WebhooksApi#getReposOwnerRepoHooksId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| Webhook的ID |

### Return type

[**Hook**](Hook.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="patchReposOwnerRepoHooksId"></a>
# **patchReposOwnerRepoHooksId**
> Hook patchReposOwnerRepoHooksId(owner, repo, id, body)

更新一个仓库WebHook

更新一个仓库WebHook

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.WebhooksApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

WebhooksApi apiInstance = new WebhooksApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | Webhook的ID
HooksIdBody body = new HooksIdBody(); // HooksIdBody | 
try {
    Hook result = apiInstance.patchReposOwnerRepoHooksId(owner, repo, id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WebhooksApi#patchReposOwnerRepoHooksId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| Webhook的ID |
 **body** | [**HooksIdBody**](HooksIdBody.md)|  | [optional]

### Return type

[**Hook**](Hook.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: */*
 - **Accept**: application/json

<a name="postReposOwnerRepoHooks"></a>
# **postReposOwnerRepoHooks**
> Hook postReposOwnerRepoHooks(owner, repo, body)

创建一个仓库WebHook

创建一个仓库WebHook

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.WebhooksApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

WebhooksApi apiInstance = new WebhooksApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
RepoHooksBody body = new RepoHooksBody(); // RepoHooksBody | 
try {
    Hook result = apiInstance.postReposOwnerRepoHooks(owner, repo, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WebhooksApi#postReposOwnerRepoHooks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **body** | [**RepoHooksBody**](RepoHooksBody.md)|  | [optional]

### Return type

[**Hook**](Hook.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postReposOwnerRepoHooksIdTests"></a>
# **postReposOwnerRepoHooksIdTests**
> postReposOwnerRepoHooksIdTests(owner, repo, id)

测试WebHook是否发送成功

测试WebHook是否发送成功

### Example
```java
// Import classes:
//import com.gitee.sdk.gitee5j.ApiClient;
//import com.gitee.sdk.gitee5j.ApiException;
//import com.gitee.sdk.gitee5j.Configuration;
//import com.gitee.sdk.gitee5j.auth.*;
//import com.gitee.sdk.gitee5j.api.WebhooksApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

WebhooksApi apiInstance = new WebhooksApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | Webhook的ID
try {
    apiInstance.postReposOwnerRepoHooksIdTests(owner, repo, id);
} catch (ApiException e) {
    System.err.println("Exception when calling WebhooksApi#postReposOwnerRepoHooksIdTests");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| Webhook的ID |

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

