# WeekReport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content** | **String** |  |  [optional]
**contentHtml** | **String** |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) | 创建时间 |  [optional]
**id** | **Integer** |  |  [optional]
**month** | **Integer** |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) | 更新时间 |  [optional]
**user** | [**UserMini**](UserMini.md) |  |  [optional]
**weekBegin** | [**LocalDate**](LocalDate.md) |  |  [optional]
**weekEnd** | [**LocalDate**](LocalDate.md) |  |  [optional]
**weekIndex** | **Integer** |  |  [optional]
**year** | **Integer** |  |  [optional]
