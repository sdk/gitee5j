## 格式化 json

```bash
wget https://repo1.maven.org/maven2/io/swagger/codegen/v3/swagger-codegen-cli/3.0.46/swagger-codegen-cli-3.0.46.jar

curl https://gitee.com/api/v5/swagger_doc.json | jq --sort-keys > api_v5.json

sed 's/V5\|\/v5//g' api_v5.json > api.json

./remove_access_token.rb api.json

./form_data_as_body.rb api.json

./format.sh api.json
```

调整 api.json 文件。

## 生成代码

仓库根目录执行：

```bash
./scripts/generate.sh
```

## 发布

```bash
mvn clean verify gpg:sign install:install deploy:deploy -Dmaven.test.skip=true
```

## 测试

### 初始化

- 需要存在用户 git

用户以及仓库初始化：

- 创建用户 gitee5j
- 创建仓库 gitee5j/example，公开
- 仓库 gitee5j/example 下新建 issue
- 用户 git 添加 issue 评论
- 用户 git 添加 issue 评论并 @gitee5j
- 仓库 gitee5j/example 下新建 PR 1，合并状态
- 仓库 gitee5j/example 下新建 PR 2，打开状态，关联 Issue

组织以及仓库初始化：

- 创建组织 gitee5j-org
- 创建仓库 gitee5j-org/example

- 创建组织 gitee5j-org2

企业以及仓库初始化：

- 创建企业 gitee5j-ent
- 创建仓库 gitee5j-ent/example
- 仓库 gitee5j-ent/example 下新建 issue
- 仓库 gitee5j-ent/example 下新建 PR 1，合并状态
- 仓库 gitee5j-ent/example 下新建 PR 2，打开状态，关联 Issue

### 环境变量

* `TEST_GITEE5J_ACCESS_TOKEN`
* `TEST_GITEE5J_BASE_PATH`

    ``````
