#!/bin/bash

# wget https://repo1.maven.org/maven2/io/swagger/codegen/v3/swagger-codegen-cli/3.0.46/swagger-codegen-cli-3.0.46.jar
# curl https://gitee.com/api/v5/swagger_doc.json | jq --sort-keys > api_v5.json
# sed 's/V5\|\/v5//g' api_v5.json > api.json
# ./remove_access_token.rb api.json
# ./form_data_as_body.rb api.json
# ./format.sh api.json
cd scripts
java -jar swagger-codegen-cli.jar generate \
    --lang java \
    --output ../../gitee5j \
    --input-spec api.json \
    --api-package com.gitee.sdk.gitee5j.api \
    --model-package com.gitee.sdk.gitee5j.model \
    --group-id com.gitee.sdk \
    --artifact-id gitee5j \
    --artifact-version 1.1-SNAPSHOT \
    --git-user-id "oschina" \
    --git-repo-id "gitee5j" \
    --http-user-agent "gitee5j" \
    -DhideGenerationTimestamp=true,java8=true,library=okhttp4-gson
rm -rf \
	../.swagger-codegen-ignore \
	../.swagger-codegen/ \
	../.travis.yml \
	../build.gradle \
	../build.sbt \
	../git_push.sh \
	../gradle.properties \
	../gradle/ \
	../gradlew \
	../gradlew.bat \
	../settings.gradle \
	../src/main/AndroidManifest.xml

git checkout ../.gitignore
