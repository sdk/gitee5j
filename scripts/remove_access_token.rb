#!/usr/bin/env ruby
require 'json'

data = File.read('api.json')
json = JSON.parse(data)
json['paths'].each do |_, path|
  path.each do |_, method|
    method['security'] = [{"OAuth2": []}]
    # only for pages
    # method['security'] = [{"OAuth2": [], "Bearer": []}]
    next unless parameters = method['parameters']
    parameters.each_with_index do |parameter, i|
      if parameter['name'] == 'access_token'
        parameters[i] = nil
        parameters.compact!
        break
      end
    end
  end
end

File.open('api.json', 'w') do |f|
  f.puts JSON.pretty_generate(json)
end

