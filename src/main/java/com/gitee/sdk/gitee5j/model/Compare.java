/*
 * Gitee Open API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.4.83
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package com.gitee.sdk.gitee5j.model;

import java.util.Objects;
import java.util.Arrays;
import com.gitee.sdk.gitee5j.model.DiffFile;
import com.gitee.sdk.gitee5j.model.RepoCommit;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
/**
 * Commits 对比  返回的 commits 数量限制在 100 以内
 */
@Schema(description = "Commits 对比  返回的 commits 数量限制在 100 以内")


public class Compare {
  @SerializedName("base_commit")
  private RepoCommit baseCommit = null;

  @SerializedName("commits")
  private List<RepoCommit> commits = null;

  @SerializedName("files")
  private List<DiffFile> files = null;

  @SerializedName("merge_base_commit")
  private RepoCommit mergeBaseCommit = null;

  @SerializedName("truncated")
  private Boolean truncated = null;

  public Compare baseCommit(RepoCommit baseCommit) {
    this.baseCommit = baseCommit;
    return this;
  }

   /**
   * Get baseCommit
   * @return baseCommit
  **/
  @Schema(description = "")
  public RepoCommit getBaseCommit() {
    return baseCommit;
  }

  public void setBaseCommit(RepoCommit baseCommit) {
    this.baseCommit = baseCommit;
  }

  public Compare commits(List<RepoCommit> commits) {
    this.commits = commits;
    return this;
  }

  public Compare addCommitsItem(RepoCommit commitsItem) {
    if (this.commits == null) {
      this.commits = new ArrayList<>();
    }
    this.commits.add(commitsItem);
    return this;
  }

   /**
   * commits 数量限制在 100 以内
   * @return commits
  **/
  @Schema(description = "commits 数量限制在 100 以内")
  public List<RepoCommit> getCommits() {
    return commits;
  }

  public void setCommits(List<RepoCommit> commits) {
    this.commits = commits;
  }

  public Compare files(List<DiffFile> files) {
    this.files = files;
    return this;
  }

  public Compare addFilesItem(DiffFile filesItem) {
    if (this.files == null) {
      this.files = new ArrayList<>();
    }
    this.files.add(filesItem);
    return this;
  }

   /**
   * 文件列表
   * @return files
  **/
  @Schema(description = "文件列表")
  public List<DiffFile> getFiles() {
    return files;
  }

  public void setFiles(List<DiffFile> files) {
    this.files = files;
  }

  public Compare mergeBaseCommit(RepoCommit mergeBaseCommit) {
    this.mergeBaseCommit = mergeBaseCommit;
    return this;
  }

   /**
   * Get mergeBaseCommit
   * @return mergeBaseCommit
  **/
  @Schema(description = "")
  public RepoCommit getMergeBaseCommit() {
    return mergeBaseCommit;
  }

  public void setMergeBaseCommit(RepoCommit mergeBaseCommit) {
    this.mergeBaseCommit = mergeBaseCommit;
  }

  public Compare truncated(Boolean truncated) {
    this.truncated = truncated;
    return this;
  }

   /**
   * 文件列表是否被截断
   * @return truncated
  **/
  @Schema(description = "文件列表是否被截断")
  public Boolean isTruncated() {
    return truncated;
  }

  public void setTruncated(Boolean truncated) {
    this.truncated = truncated;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Compare compare = (Compare) o;
    return Objects.equals(this.baseCommit, compare.baseCommit) &&
        Objects.equals(this.commits, compare.commits) &&
        Objects.equals(this.files, compare.files) &&
        Objects.equals(this.mergeBaseCommit, compare.mergeBaseCommit) &&
        Objects.equals(this.truncated, compare.truncated);
  }

  @Override
  public int hashCode() {
    return Objects.hash(baseCommit, commits, files, mergeBaseCommit, truncated);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Compare {\n");
    
    sb.append("    baseCommit: ").append(toIndentedString(baseCommit)).append("\n");
    sb.append("    commits: ").append(toIndentedString(commits)).append("\n");
    sb.append("    files: ").append(toIndentedString(files)).append("\n");
    sb.append("    mergeBaseCommit: ").append(toIndentedString(mergeBaseCommit)).append("\n");
    sb.append("    truncated: ").append(toIndentedString(truncated)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
