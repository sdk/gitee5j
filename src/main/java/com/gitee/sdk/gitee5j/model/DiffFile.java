/*
 * Gitee Open API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.4.83
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package com.gitee.sdk.gitee5j.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
/**
 * DiffFile
 */



public class DiffFile {
  @SerializedName("additions")
  private Integer additions = null;

  @SerializedName("blob_url")
  private String blobUrl = null;

  @SerializedName("changes")
  private Integer changes = null;

  @SerializedName("content_url")
  private String contentUrl = null;

  @SerializedName("deletions")
  private Integer deletions = null;

  @SerializedName("filename")
  private String filename = null;

  @SerializedName("patch")
  private String patch = null;

  @SerializedName("raw_url")
  private String rawUrl = null;

  @SerializedName("sha")
  private String sha = null;

  @SerializedName("status")
  private String status = null;

  @SerializedName("truncated")
  private Boolean truncated = null;

  public DiffFile additions(Integer additions) {
    this.additions = additions;
    return this;
  }

   /**
   * 新增行数
   * @return additions
  **/
  @Schema(description = "新增行数")
  public Integer getAdditions() {
    return additions;
  }

  public void setAdditions(Integer additions) {
    this.additions = additions;
  }

  public DiffFile blobUrl(String blobUrl) {
    this.blobUrl = blobUrl;
    return this;
  }

   /**
   * blob 链接
   * @return blobUrl
  **/
  @Schema(description = "blob 链接")
  public String getBlobUrl() {
    return blobUrl;
  }

  public void setBlobUrl(String blobUrl) {
    this.blobUrl = blobUrl;
  }

  public DiffFile changes(Integer changes) {
    this.changes = changes;
    return this;
  }

   /**
   * 变更行数
   * @return changes
  **/
  @Schema(description = "变更行数")
  public Integer getChanges() {
    return changes;
  }

  public void setChanges(Integer changes) {
    this.changes = changes;
  }

  public DiffFile contentUrl(String contentUrl) {
    this.contentUrl = contentUrl;
    return this;
  }

   /**
   * content 链接
   * @return contentUrl
  **/
  @Schema(description = "content 链接")
  public String getContentUrl() {
    return contentUrl;
  }

  public void setContentUrl(String contentUrl) {
    this.contentUrl = contentUrl;
  }

  public DiffFile deletions(Integer deletions) {
    this.deletions = deletions;
    return this;
  }

   /**
   * 删除行数
   * @return deletions
  **/
  @Schema(description = "删除行数")
  public Integer getDeletions() {
    return deletions;
  }

  public void setDeletions(Integer deletions) {
    this.deletions = deletions;
  }

  public DiffFile filename(String filename) {
    this.filename = filename;
    return this;
  }

   /**
   * 文件路径
   * @return filename
  **/
  @Schema(description = "文件路径")
  public String getFilename() {
    return filename;
  }

  public void setFilename(String filename) {
    this.filename = filename;
  }

  public DiffFile patch(String patch) {
    this.patch = patch;
    return this;
  }

   /**
   * patch
   * @return patch
  **/
  @Schema(description = "patch")
  public String getPatch() {
    return patch;
  }

  public void setPatch(String patch) {
    this.patch = patch;
  }

  public DiffFile rawUrl(String rawUrl) {
    this.rawUrl = rawUrl;
    return this;
  }

   /**
   * raw 链接
   * @return rawUrl
  **/
  @Schema(description = "raw 链接")
  public String getRawUrl() {
    return rawUrl;
  }

  public void setRawUrl(String rawUrl) {
    this.rawUrl = rawUrl;
  }

  public DiffFile sha(String sha) {
    this.sha = sha;
    return this;
  }

   /**
   * Get sha
   * @return sha
  **/
  @Schema(description = "")
  public String getSha() {
    return sha;
  }

  public void setSha(String sha) {
    this.sha = sha;
  }

  public DiffFile status(String status) {
    this.status = status;
    return this;
  }

   /**
   * 文件状态
   * @return status
  **/
  @Schema(description = "文件状态")
  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public DiffFile truncated(Boolean truncated) {
    this.truncated = truncated;
    return this;
  }

   /**
   * patch 内容是否被截断
   * @return truncated
  **/
  @Schema(description = "patch 内容是否被截断")
  public Boolean isTruncated() {
    return truncated;
  }

  public void setTruncated(Boolean truncated) {
    this.truncated = truncated;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DiffFile diffFile = (DiffFile) o;
    return Objects.equals(this.additions, diffFile.additions) &&
        Objects.equals(this.blobUrl, diffFile.blobUrl) &&
        Objects.equals(this.changes, diffFile.changes) &&
        Objects.equals(this.contentUrl, diffFile.contentUrl) &&
        Objects.equals(this.deletions, diffFile.deletions) &&
        Objects.equals(this.filename, diffFile.filename) &&
        Objects.equals(this.patch, diffFile.patch) &&
        Objects.equals(this.rawUrl, diffFile.rawUrl) &&
        Objects.equals(this.sha, diffFile.sha) &&
        Objects.equals(this.status, diffFile.status) &&
        Objects.equals(this.truncated, diffFile.truncated);
  }

  @Override
  public int hashCode() {
    return Objects.hash(additions, blobUrl, changes, contentUrl, deletions, filename, patch, rawUrl, sha, status, truncated);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DiffFile {\n");
    
    sb.append("    additions: ").append(toIndentedString(additions)).append("\n");
    sb.append("    blobUrl: ").append(toIndentedString(blobUrl)).append("\n");
    sb.append("    changes: ").append(toIndentedString(changes)).append("\n");
    sb.append("    contentUrl: ").append(toIndentedString(contentUrl)).append("\n");
    sb.append("    deletions: ").append(toIndentedString(deletions)).append("\n");
    sb.append("    filename: ").append(toIndentedString(filename)).append("\n");
    sb.append("    patch: ").append(toIndentedString(patch)).append("\n");
    sb.append("    rawUrl: ").append(toIndentedString(rawUrl)).append("\n");
    sb.append("    sha: ").append(toIndentedString(sha)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    truncated: ").append(toIndentedString(truncated)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
