/*
 * Gitee Open API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.4.83
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package com.gitee.sdk.gitee5j.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
/**
 * EnterpriseMembersBody
 */



public class EnterpriseMembersBody {
  @SerializedName("email")
  private String email = null;

  @SerializedName("name")
  private String name = null;

  /**
   * 企业角色：member &#x3D;&gt; 普通成员, outsourced &#x3D;&gt; 外包成员, admin &#x3D;&gt; 管理员
   */
  @JsonAdapter(RoleEnum.Adapter.class)
  public enum RoleEnum {
    @SerializedName("admin")
    ADMIN("admin"),
    @SerializedName("member")
    MEMBER("member"),
    @SerializedName("outsourced")
    OUTSOURCED("outsourced");

    private String value;

    RoleEnum(String value) {
      this.value = value;
    }
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }
    public static RoleEnum fromValue(String input) {
      for (RoleEnum b : RoleEnum.values()) {
        if (b.value.equals(input)) {
          return b;
        }
      }
      return null;
    }
    public static class Adapter extends TypeAdapter<RoleEnum> {
      @Override
      public void write(final JsonWriter jsonWriter, final RoleEnum enumeration) throws IOException {
        jsonWriter.value(String.valueOf(enumeration.getValue()));
      }

      @Override
      public RoleEnum read(final JsonReader jsonReader) throws IOException {
        Object value = jsonReader.nextString();
        return RoleEnum.fromValue((String)(value));
      }
    }
  }  @SerializedName("role")
  private RoleEnum role = RoleEnum.MEMBER;

  @SerializedName("username")
  private String username = null;

  public EnterpriseMembersBody email(String email) {
    this.email = email;
    return this;
  }

   /**
   * 要添加邮箱地址，若该邮箱未注册则自动创建帐号。username,email至少填写一个
   * @return email
  **/
  @Schema(description = "要添加邮箱地址，若该邮箱未注册则自动创建帐号。username,email至少填写一个")
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public EnterpriseMembersBody name(String name) {
    this.name = name;
    return this;
  }

   /**
   * 企业成员真实姓名（备注）
   * @return name
  **/
  @Schema(description = "企业成员真实姓名（备注）")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public EnterpriseMembersBody role(RoleEnum role) {
    this.role = role;
    return this;
  }

   /**
   * 企业角色：member &#x3D;&gt; 普通成员, outsourced &#x3D;&gt; 外包成员, admin &#x3D;&gt; 管理员
   * @return role
  **/
  @Schema(description = "企业角色：member => 普通成员, outsourced => 外包成员, admin => 管理员")
  public RoleEnum getRole() {
    return role;
  }

  public void setRole(RoleEnum role) {
    this.role = role;
  }

  public EnterpriseMembersBody username(String username) {
    this.username = username;
    return this;
  }

   /**
   * 需要邀请的用户名(username/login)，username,email至少填写一个
   * @return username
  **/
  @Schema(description = "需要邀请的用户名(username/login)，username,email至少填写一个")
  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EnterpriseMembersBody enterpriseMembersBody = (EnterpriseMembersBody) o;
    return Objects.equals(this.email, enterpriseMembersBody.email) &&
        Objects.equals(this.name, enterpriseMembersBody.name) &&
        Objects.equals(this.role, enterpriseMembersBody.role) &&
        Objects.equals(this.username, enterpriseMembersBody.username);
  }

  @Override
  public int hashCode() {
    return Objects.hash(email, name, role, username);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class EnterpriseMembersBody {\n");
    
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    role: ").append(toIndentedString(role)).append("\n");
    sb.append("    username: ").append(toIndentedString(username)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
