/*
 * Gitee Open API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.4.83
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package com.gitee.sdk.gitee5j.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
/**
 * EnterpriseReposBody
 */



public class EnterpriseReposBody {
  @SerializedName("auto_init")
  private Boolean autoInit = null;

  @SerializedName("can_comment")
  private Boolean canComment = true;

  @SerializedName("description")
  private String description = null;

  /**
   * Git Ignore模版
   */
  @JsonAdapter(GitignoreTemplateEnum.Adapter.class)
  public enum GitignoreTemplateEnum {
    @SerializedName("Actionscript")
    ACTIONSCRIPT("Actionscript"),
    @SerializedName("Ada")
    ADA("Ada"),
    @SerializedName("Agda")
    AGDA("Agda"),
    @SerializedName("Android")
    ANDROID("Android"),
    @SerializedName("Anjuta")
    ANJUTA("Anjuta"),
    @SerializedName("Ansible")
    ANSIBLE("Ansible"),
    @SerializedName("AppEngine")
    APPENGINE("AppEngine"),
    @SerializedName("AppceleratorTitanium")
    APPCELERATORTITANIUM("AppceleratorTitanium"),
    @SerializedName("ArchLinuxPackages")
    ARCHLINUXPACKAGES("ArchLinuxPackages"),
    @SerializedName("Archives")
    ARCHIVES("Archives"),
    @SerializedName("Autotools")
    AUTOTOOLS("Autotools"),
    @SerializedName("Backup")
    BACKUP("Backup"),
    @SerializedName("Bazaar")
    BAZAAR("Bazaar"),
    @SerializedName("BricxCC")
    BRICXCC("BricxCC"),
    @SerializedName("C")
    C("C"),
    @SerializedName("C++")
    C_("C++"),
    @SerializedName("CFWheels")
    CFWHEELS("CFWheels"),
    @SerializedName("CMake")
    CMAKE("CMake"),
    @SerializedName("CUDA")
    CUDA("CUDA"),
    @SerializedName("CVS")
    CVS("CVS"),
    @SerializedName("CakePHP")
    CAKEPHP("CakePHP"),
    @SerializedName("Calabash")
    CALABASH("Calabash"),
    @SerializedName("ChefCookbook")
    CHEFCOOKBOOK("ChefCookbook"),
    @SerializedName("Clojure")
    CLOJURE("Clojure"),
    @SerializedName("Cloud9")
    CLOUD9("Cloud9"),
    @SerializedName("CodeIgniter")
    CODEIGNITER("CodeIgniter"),
    @SerializedName("CodeKit")
    CODEKIT("CodeKit"),
    @SerializedName("CommonLisp")
    COMMONLISP("CommonLisp"),
    @SerializedName("Composer")
    COMPOSER("Composer"),
    @SerializedName("Concrete5")
    CONCRETE5("Concrete5"),
    @SerializedName("Coq")
    COQ("Coq"),
    @SerializedName("CraftCMS")
    CRAFTCMS("CraftCMS"),
    @SerializedName("D")
    D("D"),
    @SerializedName("DM")
    DM("DM"),
    @SerializedName("Dart")
    DART("Dart"),
    @SerializedName("DartEditor")
    DARTEDITOR("DartEditor"),
    @SerializedName("Delphi")
    DELPHI("Delphi"),
    @SerializedName("Diff")
    DIFF("Diff"),
    @SerializedName("Dreamweaver")
    DREAMWEAVER("Dreamweaver"),
    @SerializedName("Dropbox")
    DROPBOX("Dropbox"),
    @SerializedName("Drupal")
    DRUPAL("Drupal"),
    @SerializedName("EPiServer")
    EPISERVER("EPiServer"),
    @SerializedName("Eagle")
    EAGLE("Eagle"),
    @SerializedName("Eclipse")
    ECLIPSE("Eclipse"),
    @SerializedName("EiffelStudio")
    EIFFELSTUDIO("EiffelStudio"),
    @SerializedName("Elisp")
    ELISP("Elisp"),
    @SerializedName("Elixir")
    ELIXIR("Elixir"),
    @SerializedName("Elm")
    ELM("Elm"),
    @SerializedName("Emacs")
    EMACS("Emacs"),
    @SerializedName("Ensime")
    ENSIME("Ensime"),
    @SerializedName("Erlang")
    ERLANG("Erlang"),
    @SerializedName("Espresso")
    ESPRESSO("Espresso"),
    @SerializedName("ExpressionEngine")
    EXPRESSIONENGINE("ExpressionEngine"),
    @SerializedName("ExtJs")
    EXTJS("ExtJs"),
    @SerializedName("Fancy")
    FANCY("Fancy"),
    @SerializedName("Finale")
    FINALE("Finale"),
    @SerializedName("FlexBuilder")
    FLEXBUILDER("FlexBuilder"),
    @SerializedName("Flutter")
    FLUTTER("Flutter"),
    @SerializedName("ForceDotCom")
    FORCEDOTCOM("ForceDotCom"),
    @SerializedName("Fortran")
    FORTRAN("Fortran"),
    @SerializedName("FuelPHP")
    FUELPHP("FuelPHP"),
    @SerializedName("GPG")
    GPG("GPG"),
    @SerializedName("GWT")
    GWT("GWT"),
    @SerializedName("Gcov")
    GCOV("Gcov"),
    @SerializedName("GitBook")
    GITBOOK("GitBook"),
    @SerializedName("Go")
    GO("Go"),
    @SerializedName("Godot")
    GODOT("Godot"),
    @SerializedName("Gradle")
    GRADLE("Gradle"),
    @SerializedName("Grails")
    GRAILS("Grails"),
    @SerializedName("Haskell")
    HASKELL("Haskell"),
    @SerializedName("IGORPro")
    IGORPRO("IGORPro"),
    @SerializedName("Idris")
    IDRIS("Idris"),
    @SerializedName("Images")
    IMAGES("Images"),
    @SerializedName("JBoss")
    JBOSS("JBoss"),
    @SerializedName("JDeveloper")
    JDEVELOPER("JDeveloper"),
    @SerializedName("JENKINS_HOME")
    JENKINS_HOME("JENKINS_HOME"),
    @SerializedName("JEnv")
    JENV("JEnv"),
    @SerializedName("Java")
    JAVA("Java"),
    @SerializedName("Jekyll")
    JEKYLL("Jekyll"),
    @SerializedName("JetBrains")
    JETBRAINS("JetBrains"),
    @SerializedName("Joomla")
    JOOMLA("Joomla"),
    @SerializedName("Julia")
    JULIA("Julia"),
    @SerializedName("KDevelop4")
    KDEVELOP4("KDevelop4"),
    @SerializedName("Kate")
    KATE("Kate"),
    @SerializedName("KiCad")
    KICAD("KiCad"),
    @SerializedName("Kohana")
    KOHANA("Kohana"),
    @SerializedName("Kotlin")
    KOTLIN("Kotlin"),
    @SerializedName("LabVIEW")
    LABVIEW("LabVIEW"),
    @SerializedName("Laravel")
    LARAVEL("Laravel"),
    @SerializedName("Lazarus")
    LAZARUS("Lazarus"),
    @SerializedName("Leiningen")
    LEININGEN("Leiningen"),
    @SerializedName("LemonStand")
    LEMONSTAND("LemonStand"),
    @SerializedName("LibreOffice")
    LIBREOFFICE("LibreOffice"),
    @SerializedName("Lilypond")
    LILYPOND("Lilypond"),
    @SerializedName("Linux")
    LINUX("Linux"),
    @SerializedName("Lithium")
    LITHIUM("Lithium"),
    @SerializedName("Lua")
    LUA("Lua"),
    @SerializedName("LyX")
    LYX("LyX"),
    @SerializedName("MATLAB")
    MATLAB("MATLAB"),
    @SerializedName("Magento")
    MAGENTO("Magento"),
    @SerializedName("Maven")
    MAVEN("Maven"),
    @SerializedName("Mercurial")
    MERCURIAL("Mercurial"),
    @SerializedName("Mercury")
    MERCURY("Mercury"),
    @SerializedName("MetaProgrammingSystem")
    METAPROGRAMMINGSYSTEM("MetaProgrammingSystem"),
    @SerializedName("Metals")
    METALS("Metals"),
    @SerializedName("MicrosoftOffice")
    MICROSOFTOFFICE("MicrosoftOffice"),
    @SerializedName("MiniProgram")
    MINIPROGRAM("MiniProgram"),
    @SerializedName("ModelSim")
    MODELSIM("ModelSim"),
    @SerializedName("Momentics")
    MOMENTICS("Momentics"),
    @SerializedName("MonoDevelop")
    MONODEVELOP("MonoDevelop"),
    @SerializedName("Nanoc")
    NANOC("Nanoc"),
    @SerializedName("NetBeans")
    NETBEANS("NetBeans"),
    @SerializedName("Nim")
    NIM("Nim"),
    @SerializedName("Ninja")
    NINJA("Ninja"),
    @SerializedName("Node")
    NODE("Node"),
    @SerializedName("NotepadPP")
    NOTEPADPP("NotepadPP"),
    @SerializedName("OCaml")
    OCAML("OCaml"),
    @SerializedName("Objective-C")
    OBJECTIVE_C("Objective-C"),
    @SerializedName("Octave")
    OCTAVE("Octave"),
    @SerializedName("Opa")
    OPA("Opa"),
    @SerializedName("OpenCart")
    OPENCART("OpenCart"),
    @SerializedName("OracleForms")
    ORACLEFORMS("OracleForms"),
    @SerializedName("Otto")
    OTTO("Otto"),
    @SerializedName("PSoCCreator")
    PSOCCREATOR("PSoCCreator"),
    @SerializedName("Packer")
    PACKER("Packer"),
    @SerializedName("Patch")
    PATCH("Patch"),
    @SerializedName("Perl")
    PERL("Perl"),
    @SerializedName("Phalcon")
    PHALCON("Phalcon"),
    @SerializedName("PlayFramework")
    PLAYFRAMEWORK("PlayFramework"),
    @SerializedName("Plone")
    PLONE("Plone"),
    @SerializedName("Prestashop")
    PRESTASHOP("Prestashop"),
    @SerializedName("Processing")
    PROCESSING("Processing"),
    @SerializedName("PuTTY")
    PUTTY("PuTTY"),
    @SerializedName("PureScript")
    PURESCRIPT("PureScript"),
    @SerializedName("Python")
    PYTHON("Python"),
    @SerializedName("Qooxdoo")
    QOOXDOO("Qooxdoo"),
    @SerializedName("Qt")
    QT("Qt"),
    @SerializedName("R")
    R("R"),
    @SerializedName("ROS")
    ROS("ROS"),
    @SerializedName("Rails")
    RAILS("Rails"),
    @SerializedName("Raku")
    RAKU("Raku"),
    @SerializedName("Redcar")
    REDCAR("Redcar"),
    @SerializedName("Redis")
    REDIS("Redis"),
    @SerializedName("RhodesRhomobile")
    RHODESRHOMOBILE("RhodesRhomobile"),
    @SerializedName("Ruby")
    RUBY("Ruby"),
    @SerializedName("Rust")
    RUST("Rust"),
    @SerializedName("SBT")
    SBT("SBT"),
    @SerializedName("SCons")
    SCONS("SCons"),
    @SerializedName("SVN")
    SVN("SVN"),
    @SerializedName("Sass")
    SASS("Sass"),
    @SerializedName("Scala")
    SCALA("Scala"),
    @SerializedName("Scheme")
    SCHEME("Scheme"),
    @SerializedName("Scrivener")
    SCRIVENER("Scrivener"),
    @SerializedName("Sdcc")
    SDCC("Sdcc"),
    @SerializedName("SeamGen")
    SEAMGEN("SeamGen"),
    @SerializedName("SketchUp")
    SKETCHUP("SketchUp"),
    @SerializedName("SlickEdit")
    SLICKEDIT("SlickEdit"),
    @SerializedName("Smalltalk")
    SMALLTALK("Smalltalk"),
    @SerializedName("Stata")
    STATA("Stata"),
    @SerializedName("Stella")
    STELLA("Stella"),
    @SerializedName("SublimeText")
    SUBLIMETEXT("SublimeText"),
    @SerializedName("SugarCRM")
    SUGARCRM("SugarCRM"),
    @SerializedName("Swift")
    SWIFT("Swift"),
    @SerializedName("Symfony")
    SYMFONY("Symfony"),
    @SerializedName("SymphonyCMS")
    SYMPHONYCMS("SymphonyCMS"),
    @SerializedName("SynopsysVCS")
    SYNOPSYSVCS("SynopsysVCS"),
    @SerializedName("Tags")
    TAGS("Tags"),
    @SerializedName("TeX")
    TEX("TeX"),
    @SerializedName("Terraform")
    TERRAFORM("Terraform"),
    @SerializedName("TextMate")
    TEXTMATE("TextMate"),
    @SerializedName("Textpattern")
    TEXTPATTERN("Textpattern"),
    @SerializedName("TortoiseGit")
    TORTOISEGIT("TortoiseGit"),
    @SerializedName("TurboGears2")
    TURBOGEARS2("TurboGears2"),
    @SerializedName("Typo3")
    TYPO3("Typo3"),
    @SerializedName("Umbraco")
    UMBRACO("Umbraco"),
    @SerializedName("Unity")
    UNITY("Unity"),
    @SerializedName("UnrealEngine")
    UNREALENGINE("UnrealEngine"),
    @SerializedName("VVVV")
    VVVV("VVVV"),
    @SerializedName("Vagrant")
    VAGRANT("Vagrant"),
    @SerializedName("Vim")
    VIM("Vim"),
    @SerializedName("VirtualEnv")
    VIRTUALENV("VirtualEnv"),
    @SerializedName("Virtuoso")
    VIRTUOSO("Virtuoso"),
    @SerializedName("VisualStudio")
    VISUALSTUDIO("VisualStudio"),
    @SerializedName("VisualStudioCode")
    VISUALSTUDIOCODE("VisualStudioCode"),
    @SerializedName("Waf")
    WAF("Waf"),
    @SerializedName("WebMethods")
    WEBMETHODS("WebMethods"),
    @SerializedName("Windows")
    WINDOWS("Windows"),
    @SerializedName("WordPress")
    WORDPRESS("WordPress"),
    @SerializedName("Xcode")
    XCODE("Xcode"),
    @SerializedName("XilinxISE")
    XILINXISE("XilinxISE"),
    @SerializedName("Xojo")
    XOJO("Xojo"),
    @SerializedName("Yeoman")
    YEOMAN("Yeoman"),
    @SerializedName("Yii")
    YII("Yii"),
    @SerializedName("ZendFramework")
    ZENDFRAMEWORK("ZendFramework"),
    @SerializedName("Zephir")
    ZEPHIR("Zephir"),
    @SerializedName("macOS")
    MACOS("macOS");

    private String value;

    GitignoreTemplateEnum(String value) {
      this.value = value;
    }
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }
    public static GitignoreTemplateEnum fromValue(String input) {
      for (GitignoreTemplateEnum b : GitignoreTemplateEnum.values()) {
        if (b.value.equals(input)) {
          return b;
        }
      }
      return null;
    }
    public static class Adapter extends TypeAdapter<GitignoreTemplateEnum> {
      @Override
      public void write(final JsonWriter jsonWriter, final GitignoreTemplateEnum enumeration) throws IOException {
        jsonWriter.value(String.valueOf(enumeration.getValue()));
      }

      @Override
      public GitignoreTemplateEnum read(final JsonReader jsonReader) throws IOException {
        Object value = jsonReader.nextString();
        return GitignoreTemplateEnum.fromValue((String)(value));
      }
    }
  }  @SerializedName("gitignore_template")
  private GitignoreTemplateEnum gitignoreTemplate = null;

  @SerializedName("has_issues")
  private Boolean hasIssues = true;

  @SerializedName("has_wiki")
  private Boolean hasWiki = true;

  @SerializedName("homepage")
  private String homepage = null;

  /**
   * License模版
   */
  @JsonAdapter(LicenseTemplateEnum.Adapter.class)
  public enum LicenseTemplateEnum {
    @SerializedName("MulanPSL-2.0")
    MULANPSL_2_0("MulanPSL-2.0"),
    @SerializedName("0BSD")
    _0BSD("0BSD"),
    @SerializedName("AFL-3.0")
    AFL_3_0("AFL-3.0"),
    @SerializedName("AGPL-3.0")
    AGPL_3_0("AGPL-3.0"),
    @SerializedName("Apache-2.0")
    APACHE_2_0("Apache-2.0"),
    @SerializedName("Artistic-2.0")
    ARTISTIC_2_0("Artistic-2.0"),
    @SerializedName("BSD-2-Clause")
    BSD_2_CLAUSE("BSD-2-Clause"),
    @SerializedName("BSD-3-Clause")
    BSD_3_CLAUSE("BSD-3-Clause"),
    @SerializedName("BSD-3-Clause-Clear")
    BSD_3_CLAUSE_CLEAR("BSD-3-Clause-Clear"),
    @SerializedName("BSL-1.0")
    BSL_1_0("BSL-1.0"),
    @SerializedName("CC-BY-4.0")
    CC_BY_4_0("CC-BY-4.0"),
    @SerializedName("CC-BY-SA-4.0")
    CC_BY_SA_4_0("CC-BY-SA-4.0"),
    @SerializedName("CC0-1.0")
    CC0_1_0("CC0-1.0"),
    @SerializedName("ECL-2.0")
    ECL_2_0("ECL-2.0"),
    @SerializedName("EPL-1.0")
    EPL_1_0("EPL-1.0"),
    @SerializedName("EPL-2.0")
    EPL_2_0("EPL-2.0"),
    @SerializedName("EUPL-1.1")
    EUPL_1_1("EUPL-1.1"),
    @SerializedName("EUPL-1.2")
    EUPL_1_2("EUPL-1.2"),
    @SerializedName("GPL-2.0")
    GPL_2_0("GPL-2.0"),
    @SerializedName("GPL-3.0")
    GPL_3_0("GPL-3.0"),
    @SerializedName("ISC")
    ISC("ISC"),
    @SerializedName("LGPL-2.1")
    LGPL_2_1("LGPL-2.1"),
    @SerializedName("LGPL-3.0")
    LGPL_3_0("LGPL-3.0"),
    @SerializedName("LPPL-1.3c")
    LPPL_1_3C("LPPL-1.3c"),
    @SerializedName("MIT")
    MIT("MIT"),
    @SerializedName("MPL-2.0")
    MPL_2_0("MPL-2.0"),
    @SerializedName("MS-PL")
    MS_PL("MS-PL"),
    @SerializedName("MS-RL")
    MS_RL("MS-RL"),
    @SerializedName("MulanPSL-1.0")
    MULANPSL_1_0("MulanPSL-1.0"),
    @SerializedName("MulanPubL-1.0")
    MULANPUBL_1_0("MulanPubL-1.0"),
    @SerializedName("MulanPubL-2.0")
    MULANPUBL_2_0("MulanPubL-2.0"),
    @SerializedName("NCSA")
    NCSA("NCSA"),
    @SerializedName("OFL-1.1")
    OFL_1_1("OFL-1.1"),
    @SerializedName("OSL-3.0")
    OSL_3_0("OSL-3.0"),
    @SerializedName("PostgreSQL")
    POSTGRESQL("PostgreSQL"),
    @SerializedName("UPL-1.0")
    UPL_1_0("UPL-1.0"),
    @SerializedName("Unlicense")
    UNLICENSE("Unlicense"),
    @SerializedName("WTFPL")
    WTFPL("WTFPL"),
    @SerializedName("Zlib")
    ZLIB("Zlib");

    private String value;

    LicenseTemplateEnum(String value) {
      this.value = value;
    }
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }
    public static LicenseTemplateEnum fromValue(String input) {
      for (LicenseTemplateEnum b : LicenseTemplateEnum.values()) {
        if (b.value.equals(input)) {
          return b;
        }
      }
      return null;
    }
    public static class Adapter extends TypeAdapter<LicenseTemplateEnum> {
      @Override
      public void write(final JsonWriter jsonWriter, final LicenseTemplateEnum enumeration) throws IOException {
        jsonWriter.value(String.valueOf(enumeration.getValue()));
      }

      @Override
      public LicenseTemplateEnum read(final JsonReader jsonReader) throws IOException {
        Object value = jsonReader.nextString();
        return LicenseTemplateEnum.fromValue((String)(value));
      }
    }
  }  @SerializedName("license_template")
  private LicenseTemplateEnum licenseTemplate = null;

  @SerializedName("members")
  private String members = null;

  @SerializedName("name")
  private String name = null;

  @SerializedName("outsourced")
  private Boolean outsourced = null;

  @SerializedName("path")
  private String path = null;

  /**
   * 仓库开源类型。0(私有), 1(外部开源), 2(内部开源)。默认: 0
   */
  @JsonAdapter(PrivateEnum.Adapter.class)
  public enum PrivateEnum {
    @SerializedName("0")
    NUMBER_0(0),
    @SerializedName("1")
    NUMBER_1(1),
    @SerializedName("2")
    NUMBER_2(2);

    private Integer value;

    PrivateEnum(Integer value) {
      this.value = value;
    }
    public Integer getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }
    public static PrivateEnum fromValue(Integer input) {
      for (PrivateEnum b : PrivateEnum.values()) {
        if (b.value.equals(input)) {
          return b;
        }
      }
      return null;
    }
    public static class Adapter extends TypeAdapter<PrivateEnum> {
      @Override
      public void write(final JsonWriter jsonWriter, final PrivateEnum enumeration) throws IOException {
        jsonWriter.value(String.valueOf(enumeration.getValue()));
      }

      @Override
      public PrivateEnum read(final JsonReader jsonReader) throws IOException {
        Object value = jsonReader.nextInt();
        return PrivateEnum.fromValue((Integer)(value));
      }
    }
  }  @SerializedName("private")
  private PrivateEnum _private = PrivateEnum.NUMBER_0;

  @SerializedName("project_creator")
  private String projectCreator = null;

  public EnterpriseReposBody autoInit(Boolean autoInit) {
    this.autoInit = autoInit;
    return this;
  }

   /**
   * 值为true时则会用README初始化仓库。默认: 不初始化(false)
   * @return autoInit
  **/
  @Schema(description = "值为true时则会用README初始化仓库。默认: 不初始化(false)")
  public Boolean isAutoInit() {
    return autoInit;
  }

  public void setAutoInit(Boolean autoInit) {
    this.autoInit = autoInit;
  }

  public EnterpriseReposBody canComment(Boolean canComment) {
    this.canComment = canComment;
    return this;
  }

   /**
   * 允许用户对仓库进行评论。默认： 允许(true)
   * @return canComment
  **/
  @Schema(description = "允许用户对仓库进行评论。默认： 允许(true)")
  public Boolean isCanComment() {
    return canComment;
  }

  public void setCanComment(Boolean canComment) {
    this.canComment = canComment;
  }

  public EnterpriseReposBody description(String description) {
    this.description = description;
    return this;
  }

   /**
   * 仓库描述
   * @return description
  **/
  @Schema(description = "仓库描述")
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public EnterpriseReposBody gitignoreTemplate(GitignoreTemplateEnum gitignoreTemplate) {
    this.gitignoreTemplate = gitignoreTemplate;
    return this;
  }

   /**
   * Git Ignore模版
   * @return gitignoreTemplate
  **/
  @Schema(description = "Git Ignore模版")
  public GitignoreTemplateEnum getGitignoreTemplate() {
    return gitignoreTemplate;
  }

  public void setGitignoreTemplate(GitignoreTemplateEnum gitignoreTemplate) {
    this.gitignoreTemplate = gitignoreTemplate;
  }

  public EnterpriseReposBody hasIssues(Boolean hasIssues) {
    this.hasIssues = hasIssues;
    return this;
  }

   /**
   * 允许提Issue与否。默认: 允许(true)
   * @return hasIssues
  **/
  @Schema(description = "允许提Issue与否。默认: 允许(true)")
  public Boolean isHasIssues() {
    return hasIssues;
  }

  public void setHasIssues(Boolean hasIssues) {
    this.hasIssues = hasIssues;
  }

  public EnterpriseReposBody hasWiki(Boolean hasWiki) {
    this.hasWiki = hasWiki;
    return this;
  }

   /**
   * 提供Wiki与否。默认: 提供(true)
   * @return hasWiki
  **/
  @Schema(description = "提供Wiki与否。默认: 提供(true)")
  public Boolean isHasWiki() {
    return hasWiki;
  }

  public void setHasWiki(Boolean hasWiki) {
    this.hasWiki = hasWiki;
  }

  public EnterpriseReposBody homepage(String homepage) {
    this.homepage = homepage;
    return this;
  }

   /**
   * 主页(eg: https://gitee.com)
   * @return homepage
  **/
  @Schema(description = "主页(eg: https://gitee.com)")
  public String getHomepage() {
    return homepage;
  }

  public void setHomepage(String homepage) {
    this.homepage = homepage;
  }

  public EnterpriseReposBody licenseTemplate(LicenseTemplateEnum licenseTemplate) {
    this.licenseTemplate = licenseTemplate;
    return this;
  }

   /**
   * License模版
   * @return licenseTemplate
  **/
  @Schema(description = "License模版")
  public LicenseTemplateEnum getLicenseTemplate() {
    return licenseTemplate;
  }

  public void setLicenseTemplate(LicenseTemplateEnum licenseTemplate) {
    this.licenseTemplate = licenseTemplate;
  }

  public EnterpriseReposBody members(String members) {
    this.members = members;
    return this;
  }

   /**
   * 用逗号分开的仓库成员。如: member1,member2
   * @return members
  **/
  @Schema(description = "用逗号分开的仓库成员。如: member1,member2")
  public String getMembers() {
    return members;
  }

  public void setMembers(String members) {
    this.members = members;
  }

  public EnterpriseReposBody name(String name) {
    this.name = name;
    return this;
  }

   /**
   * 仓库名称
   * @return name
  **/
  @Schema(required = true, description = "仓库名称")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public EnterpriseReposBody outsourced(Boolean outsourced) {
    this.outsourced = outsourced;
    return this;
  }

   /**
   * 值为true值为外包仓库, false值为内部仓库。默认: 内部仓库(false)
   * @return outsourced
  **/
  @Schema(description = "值为true值为外包仓库, false值为内部仓库。默认: 内部仓库(false)")
  public Boolean isOutsourced() {
    return outsourced;
  }

  public void setOutsourced(Boolean outsourced) {
    this.outsourced = outsourced;
  }

  public EnterpriseReposBody path(String path) {
    this.path = path;
    return this;
  }

   /**
   * 仓库路径
   * @return path
  **/
  @Schema(description = "仓库路径")
  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public EnterpriseReposBody _private(PrivateEnum _private) {
    this._private = _private;
    return this;
  }

   /**
   * 仓库开源类型。0(私有), 1(外部开源), 2(内部开源)。默认: 0
   * @return _private
  **/
  @Schema(description = "仓库开源类型。0(私有), 1(外部开源), 2(内部开源)。默认: 0")
  public PrivateEnum getPrivate() {
    return _private;
  }

  public void setPrivate(PrivateEnum _private) {
    this._private = _private;
  }

  public EnterpriseReposBody projectCreator(String projectCreator) {
    this.projectCreator = projectCreator;
    return this;
  }

   /**
   * 负责人的username
   * @return projectCreator
  **/
  @Schema(description = "负责人的username")
  public String getProjectCreator() {
    return projectCreator;
  }

  public void setProjectCreator(String projectCreator) {
    this.projectCreator = projectCreator;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EnterpriseReposBody enterpriseReposBody = (EnterpriseReposBody) o;
    return Objects.equals(this.autoInit, enterpriseReposBody.autoInit) &&
        Objects.equals(this.canComment, enterpriseReposBody.canComment) &&
        Objects.equals(this.description, enterpriseReposBody.description) &&
        Objects.equals(this.gitignoreTemplate, enterpriseReposBody.gitignoreTemplate) &&
        Objects.equals(this.hasIssues, enterpriseReposBody.hasIssues) &&
        Objects.equals(this.hasWiki, enterpriseReposBody.hasWiki) &&
        Objects.equals(this.homepage, enterpriseReposBody.homepage) &&
        Objects.equals(this.licenseTemplate, enterpriseReposBody.licenseTemplate) &&
        Objects.equals(this.members, enterpriseReposBody.members) &&
        Objects.equals(this.name, enterpriseReposBody.name) &&
        Objects.equals(this.outsourced, enterpriseReposBody.outsourced) &&
        Objects.equals(this.path, enterpriseReposBody.path) &&
        Objects.equals(this._private, enterpriseReposBody._private) &&
        Objects.equals(this.projectCreator, enterpriseReposBody.projectCreator);
  }

  @Override
  public int hashCode() {
    return Objects.hash(autoInit, canComment, description, gitignoreTemplate, hasIssues, hasWiki, homepage, licenseTemplate, members, name, outsourced, path, _private, projectCreator);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class EnterpriseReposBody {\n");
    
    sb.append("    autoInit: ").append(toIndentedString(autoInit)).append("\n");
    sb.append("    canComment: ").append(toIndentedString(canComment)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    gitignoreTemplate: ").append(toIndentedString(gitignoreTemplate)).append("\n");
    sb.append("    hasIssues: ").append(toIndentedString(hasIssues)).append("\n");
    sb.append("    hasWiki: ").append(toIndentedString(hasWiki)).append("\n");
    sb.append("    homepage: ").append(toIndentedString(homepage)).append("\n");
    sb.append("    licenseTemplate: ").append(toIndentedString(licenseTemplate)).append("\n");
    sb.append("    members: ").append(toIndentedString(members)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    outsourced: ").append(toIndentedString(outsourced)).append("\n");
    sb.append("    path: ").append(toIndentedString(path)).append("\n");
    sb.append("    _private: ").append(toIndentedString(_private)).append("\n");
    sb.append("    projectCreator: ").append(toIndentedString(projectCreator)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
