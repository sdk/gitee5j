/*
 * Gitee Open API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.4.83
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package com.gitee.sdk.gitee5j.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
import java.time.OffsetDateTime;
/**
 * IssueState
 */



public class IssueState {
  @SerializedName("color")
  private String color = null;

  @SerializedName("command")
  private String command = null;

  @SerializedName("created_at")
  private OffsetDateTime createdAt = null;

  @SerializedName("icon")
  private String icon = null;

  @SerializedName("id")
  private Integer id = null;

  @SerializedName("serial")
  private Integer serial = null;

  @SerializedName("title")
  private String title = null;

  @SerializedName("updated_at")
  private OffsetDateTime updatedAt = null;

  public IssueState color(String color) {
    this.color = color;
    return this;
  }

   /**
   * 任务状态的颜色
   * @return color
  **/
  @Schema(description = "任务状态的颜色")
  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public IssueState command(String command) {
    this.command = command;
    return this;
  }

   /**
   * 任务状态的 指令
   * @return command
  **/
  @Schema(description = "任务状态的 指令")
  public String getCommand() {
    return command;
  }

  public void setCommand(String command) {
    this.command = command;
  }

  public IssueState createdAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
   * 任务状态创建时间
   * @return createdAt
  **/
  @Schema(description = "任务状态创建时间")
  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
  }

  public IssueState icon(String icon) {
    this.icon = icon;
    return this;
  }

   /**
   * 任务状态的 Icon
   * @return icon
  **/
  @Schema(description = "任务状态的 Icon")
  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }

  public IssueState id(Integer id) {
    this.id = id;
    return this;
  }

   /**
   * 任务状态 ID
   * @return id
  **/
  @Schema(description = "任务状态 ID")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public IssueState serial(Integer serial) {
    this.serial = serial;
    return this;
  }

   /**
   * 任务状态的 排序
   * @return serial
  **/
  @Schema(description = "任务状态的 排序")
  public Integer getSerial() {
    return serial;
  }

  public void setSerial(Integer serial) {
    this.serial = serial;
  }

  public IssueState title(String title) {
    this.title = title;
    return this;
  }

   /**
   * 任务状态的名称
   * @return title
  **/
  @Schema(description = "任务状态的名称")
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public IssueState updatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
    return this;
  }

   /**
   * 任务状态更新时间
   * @return updatedAt
  **/
  @Schema(description = "任务状态更新时间")
  public OffsetDateTime getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    IssueState issueState = (IssueState) o;
    return Objects.equals(this.color, issueState.color) &&
        Objects.equals(this.command, issueState.command) &&
        Objects.equals(this.createdAt, issueState.createdAt) &&
        Objects.equals(this.icon, issueState.icon) &&
        Objects.equals(this.id, issueState.id) &&
        Objects.equals(this.serial, issueState.serial) &&
        Objects.equals(this.title, issueState.title) &&
        Objects.equals(this.updatedAt, issueState.updatedAt);
  }

  @Override
  public int hashCode() {
    return Objects.hash(color, command, createdAt, icon, id, serial, title, updatedAt);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class IssueState {\n");
    
    sb.append("    color: ").append(toIndentedString(color)).append("\n");
    sb.append("    command: ").append(toIndentedString(command)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    icon: ").append(toIndentedString(icon)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    serial: ").append(toIndentedString(serial)).append("\n");
    sb.append("    title: ").append(toIndentedString(title)).append("\n");
    sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
