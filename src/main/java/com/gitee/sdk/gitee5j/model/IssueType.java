/*
 * Gitee Open API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.4.83
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package com.gitee.sdk.gitee5j.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
import java.time.OffsetDateTime;
/**
 * IssueType
 */



public class IssueType {
  @SerializedName("color")
  private String color = null;

  @SerializedName("created_at")
  private OffsetDateTime createdAt = null;

  @SerializedName("id")
  private Integer id = null;

  @SerializedName("ident")
  private String ident = null;

  @SerializedName("is_system")
  private Boolean isSystem = null;

  @SerializedName("template")
  private String template = null;

  @SerializedName("title")
  private String title = null;

  @SerializedName("updated_at")
  private OffsetDateTime updatedAt = null;

  public IssueType color(String color) {
    this.color = color;
    return this;
  }

   /**
   * 颜色
   * @return color
  **/
  @Schema(description = "颜色")
  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public IssueType createdAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
   * 任务类型创建时间
   * @return createdAt
  **/
  @Schema(description = "任务类型创建时间")
  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
  }

  public IssueType id(Integer id) {
    this.id = id;
    return this;
  }

   /**
   * 任务类型 ID
   * @return id
  **/
  @Schema(description = "任务类型 ID")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public IssueType ident(String ident) {
    this.ident = ident;
    return this;
  }

   /**
   * 唯一标识符
   * @return ident
  **/
  @Schema(description = "唯一标识符")
  public String getIdent() {
    return ident;
  }

  public void setIdent(String ident) {
    this.ident = ident;
  }

  public IssueType isSystem(Boolean isSystem) {
    this.isSystem = isSystem;
    return this;
  }

   /**
   * 是否系统默认类型
   * @return isSystem
  **/
  @Schema(description = "是否系统默认类型")
  public Boolean isIsSystem() {
    return isSystem;
  }

  public void setIsSystem(Boolean isSystem) {
    this.isSystem = isSystem;
  }

  public IssueType template(String template) {
    this.template = template;
    return this;
  }

   /**
   * 任务类型模板
   * @return template
  **/
  @Schema(description = "任务类型模板")
  public String getTemplate() {
    return template;
  }

  public void setTemplate(String template) {
    this.template = template;
  }

  public IssueType title(String title) {
    this.title = title;
    return this;
  }

   /**
   * 任务类型的名称
   * @return title
  **/
  @Schema(description = "任务类型的名称")
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public IssueType updatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
    return this;
  }

   /**
   * 任务类型更新时间
   * @return updatedAt
  **/
  @Schema(description = "任务类型更新时间")
  public OffsetDateTime getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    IssueType issueType = (IssueType) o;
    return Objects.equals(this.color, issueType.color) &&
        Objects.equals(this.createdAt, issueType.createdAt) &&
        Objects.equals(this.id, issueType.id) &&
        Objects.equals(this.ident, issueType.ident) &&
        Objects.equals(this.isSystem, issueType.isSystem) &&
        Objects.equals(this.template, issueType.template) &&
        Objects.equals(this.title, issueType.title) &&
        Objects.equals(this.updatedAt, issueType.updatedAt);
  }

  @Override
  public int hashCode() {
    return Objects.hash(color, createdAt, id, ident, isSystem, template, title, updatedAt);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class IssueType {\n");
    
    sb.append("    color: ").append(toIndentedString(color)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    ident: ").append(toIndentedString(ident)).append("\n");
    sb.append("    isSystem: ").append(toIndentedString(isSystem)).append("\n");
    sb.append("    template: ").append(toIndentedString(template)).append("\n");
    sb.append("    title: ").append(toIndentedString(title)).append("\n");
    sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
