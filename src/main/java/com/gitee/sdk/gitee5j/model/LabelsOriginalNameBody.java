/*
 * Gitee Open API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.4.83
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package com.gitee.sdk.gitee5j.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
/**
 * LabelsOriginalNameBody
 */



public class LabelsOriginalNameBody {
  @SerializedName("color")
  private String color = null;

  @SerializedName("name")
  private String name = null;

  public LabelsOriginalNameBody color(String color) {
    this.color = color;
    return this;
  }

   /**
   * 标签新颜色
   * @return color
  **/
  @Schema(description = "标签新颜色")
  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public LabelsOriginalNameBody name(String name) {
    this.name = name;
    return this;
  }

   /**
   * 标签新名称
   * @return name
  **/
  @Schema(description = "标签新名称")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LabelsOriginalNameBody labelsOriginalNameBody = (LabelsOriginalNameBody) o;
    return Objects.equals(this.color, labelsOriginalNameBody.color) &&
        Objects.equals(this.name, labelsOriginalNameBody.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(color, name);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LabelsOriginalNameBody {\n");
    
    sb.append("    color: ").append(toIndentedString(color)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
