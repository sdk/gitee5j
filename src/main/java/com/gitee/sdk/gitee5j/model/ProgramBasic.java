/*
 * Gitee Open API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.4.83
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package com.gitee.sdk.gitee5j.model;

import java.util.Objects;
import java.util.Arrays;
import com.gitee.sdk.gitee5j.model.UserBasic;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
/**
 * ProgramBasic
 */



public class ProgramBasic {
  @SerializedName("assignee")
  private UserBasic assignee = null;

  @SerializedName("author")
  private UserBasic author = null;

  @SerializedName("description")
  private String description = null;

  @SerializedName("id")
  private Integer id = null;

  @SerializedName("name")
  private String name = null;

  public ProgramBasic assignee(UserBasic assignee) {
    this.assignee = assignee;
    return this;
  }

   /**
   * Get assignee
   * @return assignee
  **/
  @Schema(description = "")
  public UserBasic getAssignee() {
    return assignee;
  }

  public void setAssignee(UserBasic assignee) {
    this.assignee = assignee;
  }

  public ProgramBasic author(UserBasic author) {
    this.author = author;
    return this;
  }

   /**
   * Get author
   * @return author
  **/
  @Schema(description = "")
  public UserBasic getAuthor() {
    return author;
  }

  public void setAuthor(UserBasic author) {
    this.author = author;
  }

  public ProgramBasic description(String description) {
    this.description = description;
    return this;
  }

   /**
   * 项目描述
   * @return description
  **/
  @Schema(description = "项目描述")
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public ProgramBasic id(Integer id) {
    this.id = id;
    return this;
  }

   /**
   * 项目id
   * @return id
  **/
  @Schema(description = "项目id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public ProgramBasic name(String name) {
    this.name = name;
    return this;
  }

   /**
   * 项目名称
   * @return name
  **/
  @Schema(description = "项目名称")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProgramBasic programBasic = (ProgramBasic) o;
    return Objects.equals(this.assignee, programBasic.assignee) &&
        Objects.equals(this.author, programBasic.author) &&
        Objects.equals(this.description, programBasic.description) &&
        Objects.equals(this.id, programBasic.id) &&
        Objects.equals(this.name, programBasic.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(assignee, author, description, id, name);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ProgramBasic {\n");
    
    sb.append("    assignee: ").append(toIndentedString(assignee)).append("\n");
    sb.append("    author: ").append(toIndentedString(author)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
