/*
 * Gitee Open API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.4.83
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package com.gitee.sdk.gitee5j.model;

import java.util.Objects;
import java.util.Arrays;
import com.gitee.sdk.gitee5j.model.ProjectBasic;
import com.gitee.sdk.gitee5j.model.UserBasic;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
/**
 * PullRequestBranch
 */



public class PullRequestBranch {
  @SerializedName("label")
  private String label = null;

  @SerializedName("ref")
  private String ref = null;

  @SerializedName("repo")
  private ProjectBasic repo = null;

  @SerializedName("sha")
  private String sha = null;

  @SerializedName("user")
  private UserBasic user = null;

  public PullRequestBranch label(String label) {
    this.label = label;
    return this;
  }

   /**
   * Get label
   * @return label
  **/
  @Schema(description = "")
  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public PullRequestBranch ref(String ref) {
    this.ref = ref;
    return this;
  }

   /**
   * Get ref
   * @return ref
  **/
  @Schema(description = "")
  public String getRef() {
    return ref;
  }

  public void setRef(String ref) {
    this.ref = ref;
  }

  public PullRequestBranch repo(ProjectBasic repo) {
    this.repo = repo;
    return this;
  }

   /**
   * Get repo
   * @return repo
  **/
  @Schema(description = "")
  public ProjectBasic getRepo() {
    return repo;
  }

  public void setRepo(ProjectBasic repo) {
    this.repo = repo;
  }

  public PullRequestBranch sha(String sha) {
    this.sha = sha;
    return this;
  }

   /**
   * Get sha
   * @return sha
  **/
  @Schema(description = "")
  public String getSha() {
    return sha;
  }

  public void setSha(String sha) {
    this.sha = sha;
  }

  public PullRequestBranch user(UserBasic user) {
    this.user = user;
    return this;
  }

   /**
   * Get user
   * @return user
  **/
  @Schema(description = "")
  public UserBasic getUser() {
    return user;
  }

  public void setUser(UserBasic user) {
    this.user = user;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PullRequestBranch pullRequestBranch = (PullRequestBranch) o;
    return Objects.equals(this.label, pullRequestBranch.label) &&
        Objects.equals(this.ref, pullRequestBranch.ref) &&
        Objects.equals(this.repo, pullRequestBranch.repo) &&
        Objects.equals(this.sha, pullRequestBranch.sha) &&
        Objects.equals(this.user, pullRequestBranch.user);
  }

  @Override
  public int hashCode() {
    return Objects.hash(label, ref, repo, sha, user);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PullRequestBranch {\n");
    
    sb.append("    label: ").append(toIndentedString(label)).append("\n");
    sb.append("    ref: ").append(toIndentedString(ref)).append("\n");
    sb.append("    repo: ").append(toIndentedString(repo)).append("\n");
    sb.append("    sha: ").append(toIndentedString(sha)).append("\n");
    sb.append("    user: ").append(toIndentedString(user)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
