/*
 * Gitee Open API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.4.83
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package com.gitee.sdk.gitee5j.model;

import java.util.Objects;
import java.util.Arrays;
import com.gitee.sdk.gitee5j.model.PullRequestLinks;
import com.gitee.sdk.gitee5j.model.UserBasic;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
import java.time.OffsetDateTime;
/**
 * 编辑评论
 */
@Schema(description = "编辑评论")


public class PullRequestComments {
  @SerializedName("_links")
  private PullRequestLinks _links = null;

  @SerializedName("body")
  private String body = null;

  @SerializedName("comment_type")
  private String commentType = null;

  @SerializedName("commit_id")
  private String commitId = null;

  @SerializedName("created_at")
  private OffsetDateTime createdAt = null;

  @SerializedName("html_url")
  private String htmlUrl = null;

  @SerializedName("id")
  private Integer id = null;

  @SerializedName("in_reply_to_id")
  private Integer inReplyToId = null;

  @SerializedName("new_line")
  private String newLine = null;

  @SerializedName("original_commit_id")
  private String originalCommitId = null;

  @SerializedName("original_position")
  private String originalPosition = null;

  @SerializedName("path")
  private String path = null;

  @SerializedName("position")
  private String position = null;

  @SerializedName("pull_request_url")
  private String pullRequestUrl = null;

  @SerializedName("updated_at")
  private OffsetDateTime updatedAt = null;

  @SerializedName("url")
  private String url = null;

  @SerializedName("user")
  private UserBasic user = null;

  public PullRequestComments _links(PullRequestLinks _links) {
    this._links = _links;
    return this;
  }

   /**
   * Get _links
   * @return _links
  **/
  @Schema(description = "")
  public PullRequestLinks getLinks() {
    return _links;
  }

  public void setLinks(PullRequestLinks _links) {
    this._links = _links;
  }

  public PullRequestComments body(String body) {
    this.body = body;
    return this;
  }

   /**
   * Get body
   * @return body
  **/
  @Schema(description = "")
  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public PullRequestComments commentType(String commentType) {
    this.commentType = commentType;
    return this;
  }

   /**
   * Get commentType
   * @return commentType
  **/
  @Schema(description = "")
  public String getCommentType() {
    return commentType;
  }

  public void setCommentType(String commentType) {
    this.commentType = commentType;
  }

  public PullRequestComments commitId(String commitId) {
    this.commitId = commitId;
    return this;
  }

   /**
   * Get commitId
   * @return commitId
  **/
  @Schema(description = "")
  public String getCommitId() {
    return commitId;
  }

  public void setCommitId(String commitId) {
    this.commitId = commitId;
  }

  public PullRequestComments createdAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
   * Get createdAt
   * @return createdAt
  **/
  @Schema(description = "")
  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
  }

  public PullRequestComments htmlUrl(String htmlUrl) {
    this.htmlUrl = htmlUrl;
    return this;
  }

   /**
   * Get htmlUrl
   * @return htmlUrl
  **/
  @Schema(description = "")
  public String getHtmlUrl() {
    return htmlUrl;
  }

  public void setHtmlUrl(String htmlUrl) {
    this.htmlUrl = htmlUrl;
  }

  public PullRequestComments id(Integer id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @Schema(description = "")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public PullRequestComments inReplyToId(Integer inReplyToId) {
    this.inReplyToId = inReplyToId;
    return this;
  }

   /**
   * Get inReplyToId
   * @return inReplyToId
  **/
  @Schema(description = "")
  public Integer getInReplyToId() {
    return inReplyToId;
  }

  public void setInReplyToId(Integer inReplyToId) {
    this.inReplyToId = inReplyToId;
  }

  public PullRequestComments newLine(String newLine) {
    this.newLine = newLine;
    return this;
  }

   /**
   * Get newLine
   * @return newLine
  **/
  @Schema(description = "")
  public String getNewLine() {
    return newLine;
  }

  public void setNewLine(String newLine) {
    this.newLine = newLine;
  }

  public PullRequestComments originalCommitId(String originalCommitId) {
    this.originalCommitId = originalCommitId;
    return this;
  }

   /**
   * Get originalCommitId
   * @return originalCommitId
  **/
  @Schema(description = "")
  public String getOriginalCommitId() {
    return originalCommitId;
  }

  public void setOriginalCommitId(String originalCommitId) {
    this.originalCommitId = originalCommitId;
  }

  public PullRequestComments originalPosition(String originalPosition) {
    this.originalPosition = originalPosition;
    return this;
  }

   /**
   * Get originalPosition
   * @return originalPosition
  **/
  @Schema(description = "")
  public String getOriginalPosition() {
    return originalPosition;
  }

  public void setOriginalPosition(String originalPosition) {
    this.originalPosition = originalPosition;
  }

  public PullRequestComments path(String path) {
    this.path = path;
    return this;
  }

   /**
   * Get path
   * @return path
  **/
  @Schema(description = "")
  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public PullRequestComments position(String position) {
    this.position = position;
    return this;
  }

   /**
   * Get position
   * @return position
  **/
  @Schema(description = "")
  public String getPosition() {
    return position;
  }

  public void setPosition(String position) {
    this.position = position;
  }

  public PullRequestComments pullRequestUrl(String pullRequestUrl) {
    this.pullRequestUrl = pullRequestUrl;
    return this;
  }

   /**
   * Get pullRequestUrl
   * @return pullRequestUrl
  **/
  @Schema(description = "")
  public String getPullRequestUrl() {
    return pullRequestUrl;
  }

  public void setPullRequestUrl(String pullRequestUrl) {
    this.pullRequestUrl = pullRequestUrl;
  }

  public PullRequestComments updatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
    return this;
  }

   /**
   * Get updatedAt
   * @return updatedAt
  **/
  @Schema(description = "")
  public OffsetDateTime getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
  }

  public PullRequestComments url(String url) {
    this.url = url;
    return this;
  }

   /**
   * Get url
   * @return url
  **/
  @Schema(description = "")
  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public PullRequestComments user(UserBasic user) {
    this.user = user;
    return this;
  }

   /**
   * Get user
   * @return user
  **/
  @Schema(description = "")
  public UserBasic getUser() {
    return user;
  }

  public void setUser(UserBasic user) {
    this.user = user;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PullRequestComments pullRequestComments = (PullRequestComments) o;
    return Objects.equals(this._links, pullRequestComments._links) &&
        Objects.equals(this.body, pullRequestComments.body) &&
        Objects.equals(this.commentType, pullRequestComments.commentType) &&
        Objects.equals(this.commitId, pullRequestComments.commitId) &&
        Objects.equals(this.createdAt, pullRequestComments.createdAt) &&
        Objects.equals(this.htmlUrl, pullRequestComments.htmlUrl) &&
        Objects.equals(this.id, pullRequestComments.id) &&
        Objects.equals(this.inReplyToId, pullRequestComments.inReplyToId) &&
        Objects.equals(this.newLine, pullRequestComments.newLine) &&
        Objects.equals(this.originalCommitId, pullRequestComments.originalCommitId) &&
        Objects.equals(this.originalPosition, pullRequestComments.originalPosition) &&
        Objects.equals(this.path, pullRequestComments.path) &&
        Objects.equals(this.position, pullRequestComments.position) &&
        Objects.equals(this.pullRequestUrl, pullRequestComments.pullRequestUrl) &&
        Objects.equals(this.updatedAt, pullRequestComments.updatedAt) &&
        Objects.equals(this.url, pullRequestComments.url) &&
        Objects.equals(this.user, pullRequestComments.user);
  }

  @Override
  public int hashCode() {
    return Objects.hash(_links, body, commentType, commitId, createdAt, htmlUrl, id, inReplyToId, newLine, originalCommitId, originalPosition, path, position, pullRequestUrl, updatedAt, url, user);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PullRequestComments {\n");
    
    sb.append("    _links: ").append(toIndentedString(_links)).append("\n");
    sb.append("    body: ").append(toIndentedString(body)).append("\n");
    sb.append("    commentType: ").append(toIndentedString(commentType)).append("\n");
    sb.append("    commitId: ").append(toIndentedString(commitId)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    htmlUrl: ").append(toIndentedString(htmlUrl)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    inReplyToId: ").append(toIndentedString(inReplyToId)).append("\n");
    sb.append("    newLine: ").append(toIndentedString(newLine)).append("\n");
    sb.append("    originalCommitId: ").append(toIndentedString(originalCommitId)).append("\n");
    sb.append("    originalPosition: ").append(toIndentedString(originalPosition)).append("\n");
    sb.append("    path: ").append(toIndentedString(path)).append("\n");
    sb.append("    position: ").append(toIndentedString(position)).append("\n");
    sb.append("    pullRequestUrl: ").append(toIndentedString(pullRequestUrl)).append("\n");
    sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
    sb.append("    url: ").append(toIndentedString(url)).append("\n");
    sb.append("    user: ").append(toIndentedString(user)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
