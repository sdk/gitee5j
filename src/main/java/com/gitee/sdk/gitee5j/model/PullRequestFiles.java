/*
 * Gitee Open API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.4.83
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package com.gitee.sdk.gitee5j.model;

import java.util.Objects;
import java.util.Arrays;
import com.gitee.sdk.gitee5j.model.Patch;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
/**
 * Pull Request Commit文件列表。最多显示300条diff
 */
@Schema(description = "Pull Request Commit文件列表。最多显示300条diff")


public class PullRequestFiles {
  @SerializedName("additions")
  private Integer additions = null;

  @SerializedName("blob_url")
  private String blobUrl = null;

  @SerializedName("deletions")
  private Integer deletions = null;

  @SerializedName("filename")
  private String filename = null;

  @SerializedName("patch")
  private Patch patch = null;

  @SerializedName("raw_url")
  private String rawUrl = null;

  @SerializedName("sha")
  private String sha = null;

  @SerializedName("status")
  private String status = null;

  public PullRequestFiles additions(Integer additions) {
    this.additions = additions;
    return this;
  }

   /**
   * Get additions
   * @return additions
  **/
  @Schema(description = "")
  public Integer getAdditions() {
    return additions;
  }

  public void setAdditions(Integer additions) {
    this.additions = additions;
  }

  public PullRequestFiles blobUrl(String blobUrl) {
    this.blobUrl = blobUrl;
    return this;
  }

   /**
   * Get blobUrl
   * @return blobUrl
  **/
  @Schema(description = "")
  public String getBlobUrl() {
    return blobUrl;
  }

  public void setBlobUrl(String blobUrl) {
    this.blobUrl = blobUrl;
  }

  public PullRequestFiles deletions(Integer deletions) {
    this.deletions = deletions;
    return this;
  }

   /**
   * Get deletions
   * @return deletions
  **/
  @Schema(description = "")
  public Integer getDeletions() {
    return deletions;
  }

  public void setDeletions(Integer deletions) {
    this.deletions = deletions;
  }

  public PullRequestFiles filename(String filename) {
    this.filename = filename;
    return this;
  }

   /**
   * Get filename
   * @return filename
  **/
  @Schema(description = "")
  public String getFilename() {
    return filename;
  }

  public void setFilename(String filename) {
    this.filename = filename;
  }

  public PullRequestFiles patch(Patch patch) {
    this.patch = patch;
    return this;
  }

   /**
   * Get patch
   * @return patch
  **/
  @Schema(description = "")
  public Patch getPatch() {
    return patch;
  }

  public void setPatch(Patch patch) {
    this.patch = patch;
  }

  public PullRequestFiles rawUrl(String rawUrl) {
    this.rawUrl = rawUrl;
    return this;
  }

   /**
   * Get rawUrl
   * @return rawUrl
  **/
  @Schema(description = "")
  public String getRawUrl() {
    return rawUrl;
  }

  public void setRawUrl(String rawUrl) {
    this.rawUrl = rawUrl;
  }

  public PullRequestFiles sha(String sha) {
    this.sha = sha;
    return this;
  }

   /**
   * Get sha
   * @return sha
  **/
  @Schema(description = "")
  public String getSha() {
    return sha;
  }

  public void setSha(String sha) {
    this.sha = sha;
  }

  public PullRequestFiles status(String status) {
    this.status = status;
    return this;
  }

   /**
   * Get status
   * @return status
  **/
  @Schema(description = "")
  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PullRequestFiles pullRequestFiles = (PullRequestFiles) o;
    return Objects.equals(this.additions, pullRequestFiles.additions) &&
        Objects.equals(this.blobUrl, pullRequestFiles.blobUrl) &&
        Objects.equals(this.deletions, pullRequestFiles.deletions) &&
        Objects.equals(this.filename, pullRequestFiles.filename) &&
        Objects.equals(this.patch, pullRequestFiles.patch) &&
        Objects.equals(this.rawUrl, pullRequestFiles.rawUrl) &&
        Objects.equals(this.sha, pullRequestFiles.sha) &&
        Objects.equals(this.status, pullRequestFiles.status);
  }

  @Override
  public int hashCode() {
    return Objects.hash(additions, blobUrl, deletions, filename, patch, rawUrl, sha, status);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PullRequestFiles {\n");
    
    sb.append("    additions: ").append(toIndentedString(additions)).append("\n");
    sb.append("    blobUrl: ").append(toIndentedString(blobUrl)).append("\n");
    sb.append("    deletions: ").append(toIndentedString(deletions)).append("\n");
    sb.append("    filename: ").append(toIndentedString(filename)).append("\n");
    sb.append("    patch: ").append(toIndentedString(patch)).append("\n");
    sb.append("    rawUrl: ").append(toIndentedString(rawUrl)).append("\n");
    sb.append("    sha: ").append(toIndentedString(sha)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
