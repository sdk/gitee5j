/*
 * Gitee Open API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.4.83
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package com.gitee.sdk.gitee5j.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
/**
 * RepoPushConfigBody
 */



public class RepoPushConfigBody {
  @SerializedName("author_email_suffix")
  private String authorEmailSuffix = null;

  @SerializedName("commit_message_regex")
  private String commitMessageRegex = null;

  @SerializedName("except_manager")
  private Boolean exceptManager = null;

  @SerializedName("max_file_size")
  private Integer maxFileSize = null;

  @SerializedName("restrict_author_email_suffix")
  private Boolean restrictAuthorEmailSuffix = null;

  @SerializedName("restrict_commit_message")
  private Boolean restrictCommitMessage = null;

  @SerializedName("restrict_file_size")
  private Boolean restrictFileSize = null;

  @SerializedName("restrict_push_own_commit")
  private Boolean restrictPushOwnCommit = null;

  public RepoPushConfigBody authorEmailSuffix(String authorEmailSuffix) {
    this.authorEmailSuffix = authorEmailSuffix;
    return this;
  }

   /**
   * 指定邮箱域名的后缀
   * @return authorEmailSuffix
  **/
  @Schema(description = "指定邮箱域名的后缀")
  public String getAuthorEmailSuffix() {
    return authorEmailSuffix;
  }

  public void setAuthorEmailSuffix(String authorEmailSuffix) {
    this.authorEmailSuffix = authorEmailSuffix;
  }

  public RepoPushConfigBody commitMessageRegex(String commitMessageRegex) {
    this.commitMessageRegex = commitMessageRegex;
    return this;
  }

   /**
   * 用于验证提交信息的正则表达式
   * @return commitMessageRegex
  **/
  @Schema(description = "用于验证提交信息的正则表达式")
  public String getCommitMessageRegex() {
    return commitMessageRegex;
  }

  public void setCommitMessageRegex(String commitMessageRegex) {
    this.commitMessageRegex = commitMessageRegex;
  }

  public RepoPushConfigBody exceptManager(Boolean exceptManager) {
    this.exceptManager = exceptManager;
    return this;
  }

   /**
   * 仓库管理员不受上述规则限制
   * @return exceptManager
  **/
  @Schema(description = "仓库管理员不受上述规则限制")
  public Boolean isExceptManager() {
    return exceptManager;
  }

  public void setExceptManager(Boolean exceptManager) {
    this.exceptManager = exceptManager;
  }

  public RepoPushConfigBody maxFileSize(Integer maxFileSize) {
    this.maxFileSize = maxFileSize;
    return this;
  }

   /**
   * 限制单文件大小（MB）
   * @return maxFileSize
  **/
  @Schema(description = "限制单文件大小（MB）")
  public Integer getMaxFileSize() {
    return maxFileSize;
  }

  public void setMaxFileSize(Integer maxFileSize) {
    this.maxFileSize = maxFileSize;
  }

  public RepoPushConfigBody restrictAuthorEmailSuffix(Boolean restrictAuthorEmailSuffix) {
    this.restrictAuthorEmailSuffix = restrictAuthorEmailSuffix;
    return this;
  }

   /**
   * 启用只允许指定邮箱域名后缀的提交
   * @return restrictAuthorEmailSuffix
  **/
  @Schema(description = "启用只允许指定邮箱域名后缀的提交")
  public Boolean isRestrictAuthorEmailSuffix() {
    return restrictAuthorEmailSuffix;
  }

  public void setRestrictAuthorEmailSuffix(Boolean restrictAuthorEmailSuffix) {
    this.restrictAuthorEmailSuffix = restrictAuthorEmailSuffix;
  }

  public RepoPushConfigBody restrictCommitMessage(Boolean restrictCommitMessage) {
    this.restrictCommitMessage = restrictCommitMessage;
    return this;
  }

   /**
   * 启用提交信息正则表达式校验
   * @return restrictCommitMessage
  **/
  @Schema(description = "启用提交信息正则表达式校验")
  public Boolean isRestrictCommitMessage() {
    return restrictCommitMessage;
  }

  public void setRestrictCommitMessage(Boolean restrictCommitMessage) {
    this.restrictCommitMessage = restrictCommitMessage;
  }

  public RepoPushConfigBody restrictFileSize(Boolean restrictFileSize) {
    this.restrictFileSize = restrictFileSize;
    return this;
  }

   /**
   * 启用限制单文件大小
   * @return restrictFileSize
  **/
  @Schema(description = "启用限制单文件大小")
  public Boolean isRestrictFileSize() {
    return restrictFileSize;
  }

  public void setRestrictFileSize(Boolean restrictFileSize) {
    this.restrictFileSize = restrictFileSize;
  }

  public RepoPushConfigBody restrictPushOwnCommit(Boolean restrictPushOwnCommit) {
    this.restrictPushOwnCommit = restrictPushOwnCommit;
    return this;
  }

   /**
   * 启用只能推送自己的提交（所推送提交中的邮箱必须与推送者所设置的提交邮箱一致）
   * @return restrictPushOwnCommit
  **/
  @Schema(description = "启用只能推送自己的提交（所推送提交中的邮箱必须与推送者所设置的提交邮箱一致）")
  public Boolean isRestrictPushOwnCommit() {
    return restrictPushOwnCommit;
  }

  public void setRestrictPushOwnCommit(Boolean restrictPushOwnCommit) {
    this.restrictPushOwnCommit = restrictPushOwnCommit;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RepoPushConfigBody repoPushConfigBody = (RepoPushConfigBody) o;
    return Objects.equals(this.authorEmailSuffix, repoPushConfigBody.authorEmailSuffix) &&
        Objects.equals(this.commitMessageRegex, repoPushConfigBody.commitMessageRegex) &&
        Objects.equals(this.exceptManager, repoPushConfigBody.exceptManager) &&
        Objects.equals(this.maxFileSize, repoPushConfigBody.maxFileSize) &&
        Objects.equals(this.restrictAuthorEmailSuffix, repoPushConfigBody.restrictAuthorEmailSuffix) &&
        Objects.equals(this.restrictCommitMessage, repoPushConfigBody.restrictCommitMessage) &&
        Objects.equals(this.restrictFileSize, repoPushConfigBody.restrictFileSize) &&
        Objects.equals(this.restrictPushOwnCommit, repoPushConfigBody.restrictPushOwnCommit);
  }

  @Override
  public int hashCode() {
    return Objects.hash(authorEmailSuffix, commitMessageRegex, exceptManager, maxFileSize, restrictAuthorEmailSuffix, restrictCommitMessage, restrictFileSize, restrictPushOwnCommit);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RepoPushConfigBody {\n");
    
    sb.append("    authorEmailSuffix: ").append(toIndentedString(authorEmailSuffix)).append("\n");
    sb.append("    commitMessageRegex: ").append(toIndentedString(commitMessageRegex)).append("\n");
    sb.append("    exceptManager: ").append(toIndentedString(exceptManager)).append("\n");
    sb.append("    maxFileSize: ").append(toIndentedString(maxFileSize)).append("\n");
    sb.append("    restrictAuthorEmailSuffix: ").append(toIndentedString(restrictAuthorEmailSuffix)).append("\n");
    sb.append("    restrictCommitMessage: ").append(toIndentedString(restrictCommitMessage)).append("\n");
    sb.append("    restrictFileSize: ").append(toIndentedString(restrictFileSize)).append("\n");
    sb.append("    restrictPushOwnCommit: ").append(toIndentedString(restrictPushOwnCommit)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
