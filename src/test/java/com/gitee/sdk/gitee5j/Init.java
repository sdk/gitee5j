package com.gitee.sdk.gitee5j;

public class Init {
    public static final String USER = "gitee5j";
    public static final String USER_REPO = "example";
    public static final String USER_BRANCH = "master";
    public static final String USER_TAG = "v1.0.0";
    public static final String FILE_PATH = "hello.md";
    public static final String BLOB_SHA1 = "980a0d5f19a64b4b30a87d4206aade58726b60e3";
    public static final String BLOB_DATA1 = "Hello World!\n"; // SGVsbG8gV29ybGQK
    public static final String BLOB_SHA2 = "bca4516ca04a77c653929c44ee4c8dde51b9cd38";
    public static final String BLOB_DATA2 = "Hello Java!\n"; // SGVsbG8gSmF2YQo=
    public static final Integer EVENT_ID = 200_0000;
    public static final Integer PR_CLOSE = 1;
    public static final Integer PR_OPEN = 2;
    public static final String ISSUE_OPEN = "I1H";

    public static final Integer RELEASE_ID = 53;
    public static final String USER_REPO_BASE = "base";
    public static final String USER_REPO_HEAD = "head";

    public static final String USER_2 = "likui";
    public static final String USER_3 = "git";

    public static final String ORG = "gitee5j-org";
    public static final String ORG_REPO = "example";
    public static final String ORG2 = "gitee5j-org2";

    public static final String ENT = "gitee5j-ent";
    public static final String ENT_REPO = "example";
    public static final String ENT_ISSUE_OPEN = "I1G";

    private static final String BASE_PATH = "TEST_GITEE5J_BASE_PATH";
    private static final String ACCESS_TOKEN = "TEST_GITEE5J_ACCESS_TOKEN";

    public static void initAll() {
        String basePath = System.getenv(BASE_PATH);
        if (basePath == null) basePath =  "https://gitee.test/api/v5";

        String accessToken = System.getenv(ACCESS_TOKEN);
        if (accessToken == null) accessToken = "0a45cf0ab26c6d85ee5c3bd19fb4e39b";

        ApiClient apiClient = Configuration.getDefaultApiClient();
        if (basePath.equals(apiClient.getBasePath())) {
            throw new RuntimeException("is production");
        }
        apiClient.setBasePath(basePath);
        apiClient.setAccessToken(accessToken);
    }
}
