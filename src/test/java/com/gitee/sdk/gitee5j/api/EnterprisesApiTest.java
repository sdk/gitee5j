/*
 * Gitee Open API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.4.77
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package com.gitee.sdk.gitee5j.api;

import com.gitee.sdk.gitee5j.ApiException;
import com.gitee.sdk.gitee5j.Init;
import com.gitee.sdk.gitee5j.model.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


/**
 * API tests for EnterprisesApi
 */
public class EnterprisesApiTest {

    @BeforeAll
    static void initAll() {
        Init.initAll();
    }

    private final EnterprisesApi api = new EnterprisesApi();

    /**
     * 移除企业成员
     *
     * 移除企业成员
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void deleteEnterprisesEnterpriseMembersUsernameTest() throws Exception {
        String enterprise = Init.ENT;
        EnterpriseMembersBody membersBody = new EnterpriseMembersBody();
        membersBody.setUsername(Init.USER_3);
        membersBody.setName("Gitee");
        api.postEnterprisesEnterpriseMembers(enterprise, membersBody);

        String username = membersBody.getUsername();
        api.deleteEnterprisesEnterpriseMembersUsername(enterprise, username);
    }
    /**
     * 删除周报某个评论
     *
     * 删除周报某个评论
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void deleteEnterprisesEnterpriseWeekReportsReportIdCommentsIdTest() throws Exception {
        String enterprise = Init.ENT;
        Integer reportId = weekReport(enterprise, Init.USER).getId();
        Note note = api.postEnterprisesEnterpriseWeekReportsIdComment(enterprise, reportId, new IdCommentBody().body("Hello World"));

        Integer id = note.getId();
        api.deleteEnterprisesEnterpriseWeekReportsReportIdCommentsId(enterprise, reportId, id);
    }
    /**
     * 企业 Pull Request 列表
     *
     * 企业 Pull Request 列表
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void getEnterpriseEnterprisePullRequestsTest() throws Exception {
        String enterprise = Init.ENT;
        String issueNumber = Init.ENT_REPO;
        String repo = null;
        Integer programId = null;
        String state = null;
        String head = null;
        String base = null;
        String sort = null;
        String since = null;
        String direction = null;
        Integer milestoneNumber = null;
        String labels = null;
        Integer page = 1;
        Integer perPage = 20;
        List<PullRequest> response = api.getEnterpriseEnterprisePullRequests(enterprise, issueNumber, repo, programId, state, head, base, sort, since, direction, milestoneNumber, labels, page, perPage);
        assertFalse(response.isEmpty());
    }
    /**
     * 获取一个企业
     *
     * 获取一个企业
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void getEnterprisesEnterpriseTest() throws Exception {
        String enterprise = Init.ENT;
        EnterpriseBasic response = api.getEnterprisesEnterprise(enterprise);
        assertNotNull(response.getId());
        assertEquals(enterprise, response.getPath());
    }
    /**
     * 列出企业的所有成员
     *
     * 列出企业的所有成员
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void getEnterprisesEnterpriseMembersTest() throws Exception {
        String enterprise = Init.ENT;
        String role = null;
        Integer page = 1;
        Integer perPage = 20;
        List<EnterpriseMember> response = api.getEnterprisesEnterpriseMembers(enterprise, role, page, perPage);
        assertFalse(response.isEmpty());
    }
    /**
     * 获取企业成员信息(通过用户名/邮箱)
     *
     * 获取企业成员信息(通过用户名/邮箱)
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void getEnterprisesEnterpriseMembersSearchTest() throws Exception {
        String enterprise = Init.ENT;
        String queryType = "username";
        String queryValue = Init.USER;
        EnterpriseMember member = api.getEnterprisesEnterpriseMembersSearch(enterprise, queryType, queryValue);
        assertEquals(queryValue, member.getUser().getLogin());
    }
    /**
     * 获取企业的一个成员
     *
     * 获取企业的一个成员
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void getEnterprisesEnterpriseMembersUsernameTest() throws Exception {
        String enterprise = Init.ENT;
        String username = Init.USER;
        EnterpriseMember response = api.getEnterprisesEnterpriseMembersUsername(enterprise, username);
        assertEquals(username, response.getUser().getLogin());
    }
    /**
     * 个人周报列表
     *
     * 个人周报列表
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void getEnterprisesEnterpriseUsersUsernameWeekReportsTest() throws Exception {
        String enterprise = Init.ENT;
        String username = Init.USER;
        weekReport(enterprise, username);

        Integer page = 1;
        Integer perPage = 20;
        List<WeekReport> response = api.getEnterprisesEnterpriseUsersUsernameWeekReports(enterprise, username, page, perPage);
        assertFalse(response.isEmpty());
    }
    /**
     * 企业成员周报列表
     *
     * 企业成员周报列表
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void getEnterprisesEnterpriseWeekReportsTest() throws Exception {
        String enterprise = Init.ENT;
        EnterpriseWeekReportBody body = reportBody();
        List<WeekReport> reports = api.getEnterprisesEnterpriseWeekReports(enterprise, 1, 20, Init.USER, body.getYear(), body.getWeekIndex(), body.getDate());
        if (reports.isEmpty()) {
            api.postEnterprisesEnterpriseWeekReport(enterprise, body);
        }

        Integer page = 1;
        Integer perPage = 20;
        String username = Init.USER;
        Integer year = body.getYear();
        Integer weekIndex = body.getWeekIndex();
        String date = body.getDate();
        List<WeekReport> response = api.getEnterprisesEnterpriseWeekReports(enterprise, page, perPage, username, year, weekIndex, date);
        assertFalse(response.isEmpty());
    }
    /**
     * 周报详情
     *
     * 周报详情
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void getEnterprisesEnterpriseWeekReportsIdTest() throws Exception {
        String enterprise = Init.ENT;
        Integer id = weekReport(enterprise, Init.USER).getId();
        WeekReport response = api.getEnterprisesEnterpriseWeekReportsId(enterprise, id);
        assertEquals(id, response.getId());
    }
    /**
     * 某个周报评论列表
     *
     * 某个周报评论列表
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void getEnterprisesEnterpriseWeekReportsIdCommentsTest() throws Exception {
        String enterprise = Init.ENT;
        Integer id = weekReport(enterprise, Init.USER).getId();
        IdCommentBody body = new IdCommentBody().body("Hello Gitee");
        api.postEnterprisesEnterpriseWeekReportsIdComment(enterprise, id, body);

        Integer page = 1;
        Integer perPage = 20;
        List<Note> response = api.getEnterprisesEnterpriseWeekReportsIdComments(enterprise, id, page, perPage);
        assertFalse(response.isEmpty());
    }
    /**
     * 列出授权用户所属的企业
     *
     * 列出授权用户所属的企业
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void getUserEnterprisesTest() throws Exception {
        Integer page = 1;
        Integer perPage = 20;
        Boolean admin = true;
        List<EnterpriseBasic> response = api.getUserEnterprises(page, perPage, admin);
        assertFalse(response.isEmpty());
    }
    /**
     * 编辑周报
     *
     * 编辑周报
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void patchEnterprisesEnterpriseWeekReportIdTest() throws Exception {
        String enterprise = Init.ENT;
        Integer id = weekReport(enterprise, Init.USER).getId();
        WeekReportIdBody body = new WeekReportIdBody().content("Hello World");
        WeekReport response = api.patchEnterprisesEnterpriseWeekReportId(enterprise, id, body);
        assertEquals(body.getContent(), response.getContent());
    }
    /**
     * 添加或邀请企业成员
     *
     * 添加或邀请企业成员
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void postEnterprisesEnterpriseMembersTest() throws Exception {
        String enterprise = Init.ENT;
        EnterpriseMembersBody body = new EnterpriseMembersBody();
        body.setUsername(Init.USER_3);
        body.setName("Gitee");
        api.postEnterprisesEnterpriseMembers(enterprise, body);
        api.deleteEnterprisesEnterpriseMembersUsername(enterprise, body.getUsername());
    }
    /**
     * 新建周报
     *
     * 新建周报
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void postEnterprisesEnterpriseWeekReportTest() throws Exception {
        String enterprise = Init.ENT;
        EnterpriseWeekReportBody body = reportBody();
        List<WeekReport> reports = api.getEnterprisesEnterpriseWeekReports(enterprise, 1, 20, Init.USER, body.getYear(), body.getWeekIndex(), body.getDate());
        if (reports.isEmpty()) {
            WeekReport response = api.postEnterprisesEnterpriseWeekReport(enterprise, body);
            assertEquals(body.getWeekIndex(), response.getWeekIndex());
            assertEquals(body.getContent(), response.getContent());
        }
    }
    /**
     * 评论周报
     *
     * 评论周报
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void postEnterprisesEnterpriseWeekReportsIdCommentTest() throws Exception {
        String enterprise = Init.ENT;
        Integer id = weekReport(enterprise, Init.USER).getId();
        IdCommentBody body = new IdCommentBody().body("Hello Gitee!");
        Note response = api.postEnterprisesEnterpriseWeekReportsIdComment(enterprise, id, body);
        assertEquals(body.getBody(), response.getBody());

        api.deleteEnterprisesEnterpriseWeekReportsReportIdCommentsId(enterprise, id, response.getId());
    }
    /**
     * 修改企业成员权限或备注
     *
     * 修改企业成员权限或备注
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void putEnterprisesEnterpriseMembersUsernameTest() throws Exception {
        String enterprise = Init.ENT;
        EnterpriseMembersBody membersBody = new EnterpriseMembersBody();
        membersBody.setUsername(Init.USER_3);
        membersBody.setName("Gitee");
        api.postEnterprisesEnterpriseMembers(enterprise, membersBody);

        String username = membersBody.getUsername();
        MembersUsernameBody body = new MembersUsernameBody();
        body.setName("Gitee5j");
        EnterpriseMember response = api.putEnterprisesEnterpriseMembersUsername(enterprise, username, body);
        assertEquals(username, response.getUser().getLogin());
        assertEquals(body.getName(), response.getRemark());

        api.deleteEnterprisesEnterpriseMembersUsername(enterprise, membersBody.getUsername());
    }

    private EnterpriseWeekReportBody reportBody() {
        LocalDate date = LocalDate.now();

        EnterpriseWeekReportBody reportBody = new EnterpriseWeekReportBody();
        reportBody.setContent("Hello Gitee");
        reportBody.setDate(date.toString());
        reportBody.setWeekIndex(date.get(WeekFields.ISO.weekOfWeekBasedYear()));
        reportBody.setYear(date.getYear());
        return reportBody;
    }

    private WeekReport weekReport(String enterprise, String username) throws ApiException {
        EnterpriseWeekReportBody body = reportBody();
        List<WeekReport> reports = api.getEnterprisesEnterpriseWeekReports(enterprise, 1, 20, username, body.getYear(), body.getWeekIndex(), body.getDate());
        WeekReport report;
        if (reports.isEmpty()) {
            report = api.postEnterprisesEnterpriseWeekReport(enterprise, body);
        } else {
            report = reports.get(0);
        }
        return report;
    }
}
