/*
 * Gitee Open API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.4.77
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package com.gitee.sdk.gitee5j.api;

import com.gitee.sdk.gitee5j.Init;
import com.gitee.sdk.gitee5j.model.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


/**
 * API tests for GistsApi
 */
public class GistsApiTest {

    @BeforeAll
    static void initAll() {
        Init.initAll();
    }

    private final GistsApi api = new GistsApi();

    /**
     * 删除代码片段的评论
     *
     * 删除代码片段的评论
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void deleteGistsGistIdCommentsIdTest() throws Exception {
        List<Code> codes = api.getGists(null, 1, 20);

        String gistId = codes.get(0).getId();
        GistIdCommentsBody body = new GistIdCommentsBody();
        body.setBody("Hello Gitee5j");
        CodeComment codeComment = api.postGistsGistIdComments(gistId, body);

        Integer id = codeComment.getId();
        api.deleteGistsGistIdCommentsId(gistId, id);
    }
    /**
     * 删除指定代码片段
     *
     * 删除指定代码片段
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Disabled
    public void deleteGistsIdTest() throws Exception {
        String id = null;
        api.deleteGistsId(id);

        // TODO: test validations
    }
    /**
     * 取消Star代码片段
     *
     * 取消Star代码片段
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void deleteGistsIdStarTest() throws Exception {
        List<Code> codes = api.getGists(null, 1, 20);


        String id = codes.get(0).getId();
        api.putGistsIdStar(id);

        api.deleteGistsIdStar(id);
    }
    /**
     * 获取代码片段
     *
     * 获取代码片段
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void getGistsTest() throws Exception {
        String since = null;
        Integer page = 1;
        Integer perPage = 20;
        List<Code> response = api.getGists(since, page, perPage);
        assertFalse(response.isEmpty());
    }
    /**
     * 获取代码片段的评论
     *
     * 获取代码片段的评论
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void getGistsGistIdCommentsTest() throws Exception {
        List<Code> codes = api.getGists(null, 1, 20);

        String gistId = codes.get(0).getId();
        Integer page = 1;
        Integer perPage = 20;
        List<CodeComment> response = api.getGistsGistIdComments(gistId, page, perPage);
        assertFalse(response.isEmpty());
    }
    /**
     * 获取单条代码片段的评论
     *
     * 获取单条代码片段的评论
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void getGistsGistIdCommentsIdTest() throws Exception {
        List<Code> codes = api.getGists(null, 1, 20);

        String gistId = codes.get(0).getId();
        GistIdCommentsBody body = new GistIdCommentsBody();
        body.setBody("Hello Gitee5j");
        CodeComment codeComment = api.postGistsGistIdComments(gistId, body);

        Integer id = codeComment.getId();
        CodeComment response = api.getGistsGistIdCommentsId(gistId, id);
        assertEquals(codeComment.getId(), response.getId());
        assertEquals(codeComment.getBody(), response.getBody());
    }
    /**
     * 获取单条代码片段
     *
     * 获取单条代码片段
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void getGistsIdTest() throws Exception {
        List<Code> codes = api.getGists(null, 1, 20);

        String id = codes.get(0).getId();
        CodeForksHistory response = api.getGistsId(id);
        assertEquals(id, response.getId());
    }
    /**
     * 获取代码片段的commit
     *
     * 获取代码片段的commit
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void getGistsIdCommitsTest() throws Exception {
        List<Code> codes = api.getGists(null, 1, 20);

        String id = codes.get(0).getId();
        CodeForksHistory response = api.getGistsIdCommits(id);
        assertNotNull(response.getId());
    }
    /**
     * 获取 Fork 了指定代码片段的列表
     *
     * 获取 Fork 了指定代码片段的列表
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void getGistsIdForksTest() throws Exception {
        List<Code> codes = api.getGists(null, 1, 20);

        String id = codes.get(0).getId();
        Integer page = 1;
        Integer perPage = 20;
        CodeForks response = api.getGistsIdForks(id, page, perPage);

        // TODO: test validations
    }
    /**
     * 判断代码片段是否已Star
     *
     * 判断代码片段是否已Star
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void getGistsIdStarTest() throws Exception {
        List<Code> codes = api.getGists(null, 1, 20);

        String id = codes.get(0).getId();
        api.getGistsIdStar(id);
    }
    /**
     * 获取用户Star的代码片段
     *
     * 获取用户Star的代码片段
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void getGistsStarredTest() throws Exception {
        List<Code> codes = api.getGists(null, 1, 20);
        String gistId = codes.get(0).getId();
        api.putGistsIdStar(gistId);

        String since = null;
        Integer page = 1;
        Integer perPage = 20;
        List<Code> response = api.getGistsStarred(since, page, perPage);
        assertFalse(response.isEmpty());
    }
    /**
     * 修改代码片段的评论
     *
     * 修改代码片段的评论
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void patchGistsGistIdCommentsIdTest() throws Exception {
        List<Code> codes = api.getGists(null, 1, 20);

        String gistId = codes.get(0).getId();
        GistIdCommentsBody commentsBody = new GistIdCommentsBody();
        commentsBody.setBody("Hello Gitee5j");
        CodeComment codeComment = api.postGistsGistIdComments(gistId, commentsBody);

        Integer id = codeComment.getId();
        CommentBody body = new CommentBody();
        body.setBody("Hello Gitee5j update");
        CodeComment response = api.patchGistsGistIdCommentsId(gistId, id, body);
        assertEquals(body.getBody(), response.getBody());
    }
    /**
     * 修改代码片段
     *
     * 修改代码片段
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void patchGistsIdTest() throws Exception {
        String id = null;
        GistsIdBody body = null;
        CodeForksHistory response = api.patchGistsId(id, body);

        // TODO: test validations
    }
    /**
     * 创建代码片段
     *
     * 创建代码片段
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void postGistsTest() throws Exception {
        GistsBody body = new GistsBody();
        List<CodeForksHistory> response = api.postGists(body);

        // TODO: test validations
    }
    /**
     * 增加代码片段的评论
     *
     * 增加代码片段的评论
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void postGistsGistIdCommentsTest() throws Exception {
        List<Code> codes = api.getGists(null, 1, 20);

        String gistId = codes.get(0).getId();
        GistIdCommentsBody body = new GistIdCommentsBody();
        body.setBody("Hello Gitee5j");
        CodeComment response = api.postGistsGistIdComments(gistId, body);
        assertEquals(body.getBody(), response.getBody());
    }
    /**
     * Fork代码片段
     *
     * Fork代码片段
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Disabled
    public void postGistsIdForksTest() throws Exception {
        String id = null;
        api.postGistsIdForks(id);

        // TODO: test validations
    }
    /**
     * Star代码片段
     *
     * Star代码片段
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void putGistsIdStarTest() throws Exception {
        List<Code> codes = api.getGists(null, 1, 20);

        String id = codes.get(0).getId();
        api.putGistsIdStar(id);
    }
}
