/*
 * Gitee Open API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.4.77
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package com.gitee.sdk.gitee5j.api;

import com.gitee.sdk.gitee5j.Init;
import com.gitee.sdk.gitee5j.model.MarkdownBody;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;


/**
 * API tests for MiscellaneousApi
 */
public class MiscellaneousApiTest {

    @BeforeAll
    static void initAll() {
        Init.initAll();
    }

    private final MiscellaneousApi api = new MiscellaneousApi();

    /**
     * 列出可使用的 Emoji
     *
     * 列出可使用的 Emoji
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void getEmojisTest() throws Exception {
        api.getEmojis();

        // TODO: test validations
    }
    /**
     * 列出可使用的 .gitignore 模板
     *
     * 列出可使用的 .gitignore 模板
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void getGitignoreTemplatesTest() throws Exception {
        api.getGitignoreTemplates();

        // TODO: test validations
    }
    /**
     * 获取一个 .gitignore 模板
     *
     * 获取一个 .gitignore 模板
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void getGitignoreTemplatesNameTest() throws Exception {
        String name = "Java";
        api.getGitignoreTemplatesName(name);

        // TODO: test validations
    }
    /**
     * 获取一个 .gitignore 模板原始文件
     *
     * 获取一个 .gitignore 模板原始文件
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void getGitignoreTemplatesNameRawTest() throws Exception {
        String name = "Java";
        api.getGitignoreTemplatesNameRaw(name);

        // TODO: test validations
    }
    /**
     * 列出可使用的开源许可协议
     *
     * 列出可使用的开源许可协议
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void getLicensesTest() throws Exception {
        api.getLicenses();

        // TODO: test validations
    }
    /**
     * 获取一个开源许可协议
     *
     * 获取一个开源许可协议
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void getLicensesLicenseTest() throws Exception {
        String license = "MIT";
        api.getLicensesLicense(license);

        // TODO: test validations
    }
    /**
     * 获取一个开源许可协议原始文件
     *
     * 获取一个开源许可协议原始文件
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void getLicensesLicenseRawTest() throws Exception {
        String license = "MIT";
        api.getLicensesLicenseRaw(license);

        // TODO: test validations
    }
    /**
     * 获取一个仓库使用的开源许可协议
     *
     * 获取一个仓库使用的开源许可协议
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void getReposOwnerRepoLicenseTest() throws Exception {
        String owner = Init.USER;
        String repo = Init.USER_REPO;
        api.getReposOwnerRepoLicense(owner, repo);

        // TODO: test validations
    }
    /**
     * 渲染 Markdown 文本
     *
     * 渲染 Markdown 文本
     *
     * @throws Exception
     *          if the Api call fails
     */
    @Test
    public void postMarkdownTest() throws Exception {
        MarkdownBody body = new MarkdownBody();
        body.setText("# Title");
        api.postMarkdown(body);

        // TODO: test validations
    }
}
